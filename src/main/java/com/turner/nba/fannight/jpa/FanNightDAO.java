package com.turner.nba.fannight.jpa;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.turner.nba.fannight.jpa.Poll;
import com.turner.nba.fannight.jpa.PollValue;
import com.turner.nba.fannight.jpa.Team;
import com.turner.nba.fannight.jpa.Week;
import com.turner.nba.fannight.util.MessageVote;
import com.turner.loki.util.PersistenceManager;

public class FanNightDAO {
	Logger logger = Logger.getLogger(this.getClass());
	public String classReference = this.getClass().toString();

	public static final String NBAFANNIGHT = "nba_fan_night";
	private static FanNightDAO instance; 
	
	private FanNightDAO() {}
	
	public static FanNightDAO getInstance() {
		if(instance == null) {
			instance = new FanNightDAO();
		}
		
		return instance;
	}
	
	public List<Poll> getPolls() throws Exception {
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		List<Poll> polls = (List<Poll>) em.createQuery("SELECT P FROM Poll P").getResultList();
		
		return polls == null?new ArrayList<Poll>():polls;
	}
	
	public boolean updatePoll(String pollID, String tweetID){
		boolean hasError = false;
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		em.getTransaction().begin();
		try{
			Poll poll = em.find(Poll.class, pollID);
		
			if(poll == null){	//the poll has to exist!
				logger.warn("Poll with current id["+pollID+"] does not exist. Cannot update it!");
				return false;
			}
		
			logger.info("updating latest poll ["+poll.getId()+"] with vote id "+tweetID);
			
			poll.setPreviousTwitterVoteId(tweetID);
			poll.setPreviousTwitterVoteDate(new Date());
			
			em.persist(poll);
		}catch(Exception e){
			hasError = true;
			logger.warn("Error while updating poll["+pollID+"]: "+e.getMessage());
		}finally{
			if(hasError)
				em.getTransaction().rollback();
			else
				em.getTransaction().commit();
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return !hasError;
	}
	
	/***********************************************************************************************
	 * this method only updates the poll if a poll with the matching id already exists in the DB
	 * 
	 * @param poll
	 * @return
	 ***********************************************************************************************/
	public boolean updatePoll(Poll poll){
		boolean hasError = false;
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		try{
			Poll p = em.find(Poll.class, poll.getId());
		
			if(p == null){	//the poll has to exist!
				logger.warn("Poll with current id["+poll.getId()+"] does not exist. Cannot update it!");
				return false;
			}
		
			logger.info("updating latest poll ["+poll.getId()+"] with vote id "+poll.getPreviousTwitterVoteId());
			
			poll.setLastCogixPollDate(new Date());
			
			em.persist(poll);
		}catch(Exception e){
			hasError = true;
			logger.warn("Error while updating poll["+poll.getId()+"]: "+e.getMessage(), e);
		}finally{
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return !hasError;
	}
	
	/************************************************************************************************
	 * this method only insert a poll if a poll with the matching id doesn't already exist in the DB
	 * 
	 * @param poll
	 * @return
	 * @throws Exception 
	 ***********************************************************************************************/
	public boolean insertPoll(Poll poll) throws Exception{
		boolean noError = true;
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		em.getTransaction().begin();
		try{
			List<Poll> polls = (List<Poll>) em.createQuery("SELECT P FROM Poll P ORDER BY P.week").getResultList();
			Poll latestPoll = polls.isEmpty() ? null : (Poll) polls.get(polls.size() - 1);;
			logger.info("Found "+polls.size()+" polls");
			
			Poll p = em.find(Poll.class, poll.getId());
		
			if(p != null){	//the poll has to NOT exist!
				logger.warn("Poll with current id["+poll.getId()+"] already exists. Cannot re-insert it!");
				return false;
			}
		
			logger.info("inserting new poll ["+poll.getId()+"]");
			Date laDate = new Date();
			poll.setLastCogixPollDate(laDate);
			poll.setPreviousTwitterVoteDate(laDate);
			
			//force the new poll to start pulling tweets from where the previous poll stopped
			poll.setPreviousTwitterVoteId(latestPoll == null ? "0" : latestPoll.getPreviousTwitterVoteId());
			
			em.persist(poll);
		}catch(Exception e){
			noError = false;
			logger.warn("Error while inserting poll["+poll.getId()+"]: "+e.getMessage(), e);
		}finally{
			if(noError)
				em.getTransaction().commit();
			else
				em.getTransaction().rollback();
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return noError;
	}
	
	/*************************************************************************************************
	 * cleans out the VotingCounts table !
	 * 
	 * @return
	 * @throws Exception
	 *************************************************************************************************/
	public boolean deleteVotingCounts() throws Exception {
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		EntityTransaction trans = em.getTransaction();
		boolean rollback = false;
		
		try{
			trans.begin();
			Query q = em.createQuery("DELETE FROM VotingCounts");
			q.executeUpdate();
		}catch(Exception e){
			rollback = true;
			logger.error("Deleting the Voting Counts table failed: "+e.getMessage(), e);
			throw new Exception("Deleting the Voting Counts table failed: "+e.getMessage(), e);
		}finally{
			if(rollback)
				trans.rollback();
			else
				trans.commit();
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return true;
	}
	/*************************************************************************************************
	 * returns a single row of the VotingCounts table !
	 * 
	 * @return
	 * @throws Exception
	 *************************************************************************************************/
	public VotingCounts getVotingCounts( MessageVote vote ) throws Exception {
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		
		em.getTransaction().begin();
        VotingCountsId id = new VotingCountsId();
        id.setSource( vote.getSource() );
        id.setText( vote.getText() );
		VotingCounts vc = null;
		try {
			vc = em.find( VotingCounts.class, id );
		
			if(vc  == null) {	//the row does not exist create it 
                 vc = new VotingCounts();
                 vc.setCounter( 0 );
                 vc.setSource( vote.getSource() );
                 vc.setText( vote.getText() ); 
                
			}
		} catch(Exception e) {
			
			String msg = String.format("%s [%s] [%s] ","Get the Voting Counts table failed: ",id.getSource(),id.getText() ) + e.getMessage();
			logger.error( msg, e);
			throw new Exception( msg, e );
		} finally {
			
			em.getTransaction().rollback();
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		return vc;
	}
	/*************************************************************************************************
	 * updates a single row of the VotingCounts table !
	 * 
	 * @return
	 * @throws Exception
	 *************************************************************************************************/
	public void updateVotingCounts( VotingCounts vote ) throws Exception {
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		
		em.getTransaction().begin();
		boolean noError = true;
	    VotingCountsId id = new VotingCountsId();
	    id.setSource( vote.getSource() );
	    id.setText( vote.getText() );
	    Date now = new Date();
		
		try {
			VotingCounts vc = em.find( VotingCounts.class, id );
			
			if(vc  == null) {	//the row does not exist create it 
				 vote.setLastUpdate(  now );
				 em.persist( vote );
			} else {
				vc.setCounter( vote.getCounter() );
				vc.setLastUpdate( now );
				em.persist( vc );
			}
		    logger.info(String.format("%s [%s] [%s] %d","Updating Voting Counts with Count ",id.getSource(), id.getText(), vote.getCounter() ));
		     
	    } catch(Exception e) {	
	       noError = false;
	       String msg = String.format("Error while updating VotingCount [%s] [%s] ", id.getSource(), id.getText() );
		   logger.warn( msg + e.getMessage(), e);
	    } finally {
			if(noError)
				em.getTransaction().commit();
			else
				em.getTransaction().rollback();
		   PersistenceManager.closeEntityManager(NBAFANNIGHT);
	    }
	
		
	}
	/*************************************************************************************************
	 * cleans out the database!
	 * 
	 * @return
	 * @throws Exception
	 *************************************************************************************************/
	public boolean deletePolls() throws Exception {
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		EntityTransaction trans = em.getTransaction();
		boolean rollback = false;
		
		try{
			trans.begin();
			Query q = em.createQuery("DELETE FROM PollValue");
			q.executeUpdate();
			q = em.createQuery("DELETE FROM Poll");
			q.executeUpdate();
		}catch(Exception e){
			rollback = true;
			logger.error("Deleting the database failed: "+e.getMessage(), e);
			throw new Exception("Deleting database failed: "+e.getMessage(), e);
		}finally{
			if(rollback)
				trans.rollback();
			else
				trans.commit();
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return true;
	}
	
	/*************************************************************************************************
	 * cleans out the database and then inserts a new poll in it's place.
	 * 
	 * @param poll
	 * @return
	 * @throws Exception
	 *************************************************************************************************/
	public boolean reloadPoll(Poll poll) throws Exception {
		logger.info("saving poll object to database");
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		EntityTransaction trans = em.getTransaction();
		boolean rollback = false;
		
		try{
			trans.begin();
			Query q = em.createQuery("DELETE FROM PollValue");
			q.executeUpdate();
			q = em.createQuery("DELETE FROM Poll");
			q.executeUpdate();
			
			for(PollValue value:poll.getValues())
				value.setPoll(poll);
			
			em.persist(poll);
		}catch(Exception e){
			rollback = true;
			logger.error("Reloading the database failed: "+e.getMessage(), e);
			throw new Exception("Reloading database failed: "+e.getMessage(), e);
		}finally{
			if(rollback)
				trans.rollback();
			else
				trans.commit();
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return true;
	}
	
	public Poll getCurrentWeekPoll() throws Exception {
		return getCurrentWeek().getPoll();
	}
	
	/*public Poll getLatestCachedPoll() throws Exception{
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		Poll poll = null;
		
		try{
			List<Poll> polls = (List<Poll>) em.createQuery("SELECT P FROM Poll P").getResultList();
			logger.info("Found "+polls.size()+" polls");
			
			poll = (Poll) polls.get(polls.size() - 1);
		}catch(Exception e){
			logger.warn("Exception while retrieving latest poll: "+e.getMessage());
		}finally{
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return poll;
	}*/
	
	public Poll getSampleCogixPoll(){
		logger.info("retrieving sample cogix poll for testing purposes only");
		
		Poll poll = null;
		Week week = null;
		
		try{
			week = getCurrentWeek();
		}catch(Exception e){}
		
		if(week == null){
			logger.info("creating new sample cogix poll");
			poll = new Poll();
			poll.setId(System.currentTimeMillis()+"");
			poll.setLastCogixPollDate(new Date());
			
			List<PollValue> values = new ArrayList<PollValue>();
			for(int i = 0;i < 3;i++){
				PollValue pv = new PollValue();
				pv.setPoll(poll);
				values.add(pv);
			}
			
			values.get(0).setId("ORLvsCHI".toLowerCase());
			values.get(0).setValue("ORLvsCHI".toLowerCase());
			
			values.get(1).setId("TORvsOKC".toLowerCase());
			values.get(1).setValue("TORvsOKC".toLowerCase());
			
			values.get(2).setId("DETvsDEN".toLowerCase());
			values.get(2).setValue("DETvsDEN".toLowerCase());
			
			poll.setValues(values);
			
			week = new Week();
			week.setId(1L);
			week.setPoll(poll);
			week.setWeekYear(2012);
			
			poll.setWeek(week);
		}else{
			poll = week.getPoll();
			logger.info("using poll found in database as sample");
		}
		
		return poll;
	}
	
	public List<Team> getTeams(){
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		List<Team> teams = new ArrayList<Team>();
		
		try{
			Query q = em.createQuery("SELECT t FROM Team t");
			teams = (List<Team>)q.getResultList();
		}catch(Exception e){
			logger.warn("Exception while retrieving teams: "+e.getMessage(), e);
		}finally{
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return teams;
	}

	public Week getCurrentWeek(){
		logger.debug("retrieving current week");
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		Week week = null;
		
		try{
			Query q = em.createQuery("SELECT w FROM Week w where :today <= w.endDate ORDER BY w.endDate");
			q.setParameter("today", new Date());
			List<Week> weeks = (List<Week>) q.getResultList();
			
			if(weeks == null || weeks.isEmpty()){	//if there are weeks but all weeks are passed then create a week with id -1
				q = em.createQuery("SELECT COUNT(w.endDate) FROM Week w");
				long count = (Long) q.getSingleResult();
				
				if(count == 0)
					throw new Exception("No Week found in DB");
				
				week = new Week();
				week.setId(Week.NO_WEEK_LEFT);
			}else{
				week = weeks.get(0);
			}
		}catch(Exception e){
			logger.warn("Exception while retrieving week: "+e.getMessage(), e);
		}finally{
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return week;
	}
}