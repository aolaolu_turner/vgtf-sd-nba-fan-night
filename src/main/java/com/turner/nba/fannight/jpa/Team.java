package com.turner.nba.fannight.jpa;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Transient;

import org.apache.log4j.Logger;

@Entity
public class Team {
	@Transient private org.apache.log4j.Logger logger = Logger.getLogger(this.getClass());
	
	@Transient 	public final static String CITY = "city";
	@Transient public final static String NAME = "name";
	@Transient public final static String NICKNAME = "nickname";
	@Transient public final static String ABBREVIATION = "abbreviation";
	@Transient public final static String[] SEARCH_ORDER = { CITY, NAME, NICKNAME, ABBREVIATION };
	
	@Transient private boolean isLowerCased = false;
	
	@Id
	@Column(nullable=false)
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long Id;
	@Column(nullable=false)
	private String city;
	@Column(nullable=false)
	private String name;
	@Column(nullable=false)
	private String abbreviation;
	private String nickname;
	
	public Long getId() {
		return Id;
	}
	public void setId(Long id) {
		Id = id;
	}
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city.toLowerCase();
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name.toLowerCase();
	}
	
	public String getAbbreviation() {
		return abbreviation;
	}
	public void setAbbreviation(String abbreviation) {
		this.abbreviation = abbreviation.toLowerCase();
	}
	
	public String getNickname() {
		return nickname;
	}
	public void setNickname(String nickname) {
		this.nickname = nickname.toLowerCase();
	}
	
	public String toString(){
		return getCity() + "\t/\t" + getName() + "\t/\t" + getNickname() + "\t/\t" + getAbbreviation();
	}
	/***********************************************************************************************************************
	 * @deprecated this method has been shown to be incorrect 
	 *                e.g. 'hornets' would select 'nets' and 'golden state' would select 'den' which are both wrong
	 * @param text
	 * @return
	 ************************************************************************************************************************/
	@Deprecated
	public boolean isValidTeamName(String text){
		if(!isLowerCased){
			if(this.city != null)
				setCity(this.city);
			if(this.name != null)
				setName(this.name);
			if(this.abbreviation != null)
				setAbbreviation(this.abbreviation);
			if(this.nickname != null)
				setNickname(this.nickname);
			
			isLowerCased = true;
		}
		
		boolean value = (text != null && (
							(!(city == null || city.equals("")) && text.contains(city)) ||
							(!(name == null || name.equals("")) && text.contains(name)) ||
							(!(abbreviation == null || abbreviation.equals("")) && text.contains(abbreviation)) ||
							(!(nickname == null || nickname.equals("")) && text.contains(nickname))
							)
						);
		logger.debug("Testing ["+text+"] against ["+abbreviation+"] is "+value);
		
		return value;
	}
}
