package com.turner.nba.fannight.jpa;

import java.io.Serializable;

public class VotingCountsId implements Serializable {
	String source;
	String text;
	
    public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
}
