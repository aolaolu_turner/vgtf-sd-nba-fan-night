package com.turner.nba.fannight.jpa;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.OneToMany;

import com.turner.nba.fannight.jpa.PollValue;
import com.turner.nba.fannight.jpa.Week;

@Entity
public class Poll implements Serializable {
	private static final long serialVersionUID = 7098241874825042698L;
	
	private String id;
	private List<PollValue> values;
	private Date lastCogixPollDate;
	private Date previousTwitterVoteDate;
	private String previousTwitterVoteId = "0";
	private Week week;
	
	@Id
	@Column(nullable=false, insertable=true, updatable=false)
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	@OneToOne()
	public Week getWeek() {
		return week;
	}
	public void setWeek(Week week) {
		this.week = week;
	}
	
	@OneToMany(cascade=CascadeType.ALL, mappedBy="poll", fetch=FetchType.EAGER)
	public List<PollValue> getValues() {
		return values;
	}
	public void setValues(List<PollValue> values) {
		this.values = values;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastCogixPollDate() {
		return lastCogixPollDate;
	}
	public void setLastCogixPollDate(Date lastCogixPollDate) {
		this.lastCogixPollDate = lastCogixPollDate;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getPreviousTwitterVoteDate() {
		return previousTwitterVoteDate;
	}
	public void setPreviousTwitterVoteDate(Date lastTwitterVoteDate) {
		this.previousTwitterVoteDate = lastTwitterVoteDate;
	}
	
	public String getPreviousTwitterVoteId() {
		return previousTwitterVoteId;
	}
	public void setPreviousTwitterVoteId(String lastTwitterVoteId) {
		this.previousTwitterVoteId = lastTwitterVoteId;
	}

}
