package com.turner.nba.fannight.jpa;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.JoinColumn;
import javax.persistence.Transient;

import com.turner.nba.fannight.jpa.Poll;

@Entity
public class PollValue implements Serializable {
	private static final long serialVersionUID = 8454350237416744502L;
	@Id 
	private String id;
	private String value;
	@Transient
	private String count;
	@ManyToOne
	@JoinColumn(name="poll_id", nullable=false, updatable=false)
	private Poll poll;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value.toLowerCase();
	}
	public Poll getPoll() {
		return poll;
	}
	public void setPoll(Poll poll) {
		this.poll = poll;
	}
	public String getCount() {
		return count;
	}
	public void setCount(String count) {
		this.count = count;
	}
	
}
