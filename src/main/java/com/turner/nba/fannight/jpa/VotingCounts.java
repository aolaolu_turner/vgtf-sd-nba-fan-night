package com.turner.nba.fannight.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


//import org.hibernate.annotations.Index;

@Entity
@IdClass(VotingCountsId.class)
public class VotingCounts implements Serializable {
	private static final long serialVersionUID = 7098241874787042698L;
 
	private String source;
	private String text;
	private int    counter;
	private Date   lastUpdate;
	
	@Id
	@Column(nullable=false)
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	
    @Id
	@Column(nullable=false)
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	@Column()
	public int getCounter() {
		return counter;
	}

	public void setCounter(int counter) {
		this.counter = counter;
	}
	
	@Temporal(TemporalType.TIMESTAMP)
	public Date getLastUpdate() {
		return lastUpdate;
	}
	public void setLastUpdate(Date lastUpdate) {
		this.lastUpdate = lastUpdate;
	}
	
}
