package com.turner.nba.fannight.jpa;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Week implements Serializable {
	private static final long serialVersionUID = 4775058195663831348L;
	public static final Long NO_WEEK_LEFT = -1L;
	
	private Long id;
	private Date endDate;
	private int weekYear;
	private Poll poll;
	
	@Id
	@Column(insertable=true)
	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Temporal(TemporalType.TIMESTAMP)
	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date date) {
		this.endDate = date;
	}

	public int getWeekYear() {
		return weekYear;
	}

	public void setWeekYear(int weekYear) {
		this.weekYear = weekYear;
	}

	@OneToOne(mappedBy="week", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	public Poll getPoll() {
		return poll;
	}

	public void setPoll(Poll poll) {
		this.poll = poll;
	}
	
}
