package com.turner.nba.fannight.plugin;

import java.util.Hashtable;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;
import org.jaxen.jdom.JDOMXPath;
import org.jdom.Element;

import javax.jms.Message;

import org.apache.log4j.Logger;

import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;

import com.turner.loki.AbstractPlugin;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.nba.util.Helper;

public class SendVoteToCogix extends AbstractPlugin {
	private Logger logger = Logger.getLogger(this.getClass());
	
	private String cogixUrl = "";
	private String classReference = this.getClass().toString();
	
	@Override
	public void initialize(Hashtable props) throws InitializationException {
		logger.info("Initializing "+this.classReference + "...");

		if(props.containsKey("cogix_url"))
			cogixUrl = (String) props.get("cogix_url");
		
		if(cogixUrl == null || "".equals(cogixUrl.trim()))
			throw new InitializationException("Initialization error: Cogix URL is missing.");
	}

	@Override
	public Object process(Message m) throws PluginException {
		CogixConnection cogix = CogixConnection.getInstance();
		GenericMessage gm = null;

		try{
			if (m instanceof GenericMessage) 
				gm = (GenericMessage) m;
			else 
				gm = GenericMessageFactory.createGenericMessage(m);
			
			String bodyStr = gm.getStringProperty("body");
			Document doc = Helper.convertStringToDocument(bodyStr);	
			XPath teamXP = XPath.newInstance("//poll");
			Element teamElement = (Element)teamXP.selectSingleNode(doc);
			String id = (String) teamElement.getAttributeValue("id");
			String vote = (String) teamElement.getAttributeValue("vote");
			String source = (String) teamElement.getAttributeValue("source");
			
			cogix.sendVote(cogixUrl, id, vote, source );
			
		} catch (Exception e) {
			logger.error("Could not send vote to Cogix: "+e.getMessage(), e);
			throw new PluginException("Could not create GenericMessage", e);
		}
		
		return gm;
	}
}
