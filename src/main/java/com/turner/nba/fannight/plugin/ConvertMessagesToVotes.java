package com.turner.nba.fannight.plugin;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.jms.Message;

import org.apache.log4j.Logger;
import org.apache.commons.lang.StringUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.loki.core.interfaces.InitializableIF;
import com.turner.loki.core.interfaces.MonitorableIF;
import com.turner.loki.core.interfaces.MultiplexIF;
import com.turner.nba.fannight.jpa.FanNightDAO;
import com.turner.nba.fannight.jpa.Poll;
import com.turner.nba.fannight.jpa.VotingCounts;
import com.turner.nba.fannight.jpa.Week;
import com.turner.nba.fannight.util.MessageVote;
import com.turner.nba.fannight.util.TwitterVote;
import com.turner.nba.fannight.util.facebook.parsingDto.Data;
import com.turner.nba.fannight.util.facebook.parsingDto.Facebook;
import com.turner.nba.fannight.util.intragram.parsingDto.Instagram;
import com.turner.nba.fannight.util.twitter.parsingDto.Total;
import com.turner.nba.fannight.util.twitter.parsingDto.Twitter;

public class ConvertMessagesToVotes implements MonitorableIF, MultiplexIF,
		InitializableIF {
	private Logger logger = Logger.getLogger(this.getClass());
	private String classReference = this.getClass().toString();

	public static String sourceInstagram = "instagram";
	public static String sourceFacebook = "facebook";
	public static String sourceTwitter = "twitter";

	private Map<String, String> sourceMap = new HashMap<String, String>();
	
	@Override
	public void initialize(Hashtable props) throws InitializationException {
		logger.info("Initializing " + this.classReference + "...");
		for (int i = 0; i < 3; i++) {
			String key = String.format("source%d", i);
			String url = String.format("url%d", i);
			String keyValue = (String) props.get(key);
			String urlValue = (String) props.get(url);
			if (keyValue != null && !keyValue.equals(sourceInstagram)
					&& !keyValue.equals(sourceFacebook)
					&& !keyValue.equals(sourceTwitter)) {
				throw new InitializationException(
						String.format(
								"Initialization error: Only Sources Supported [%s] [%s] [%s] ",
								sourceInstagram, sourceTwitter, sourceFacebook));
			}
			if (keyValue != null && urlValue != null) {
				sourceMap.put(keyValue.trim(), urlValue.trim());
			}
		}

		if (sourceMap.isEmpty()) {
			throw new InitializationException(
					"Initialization error: No Source URLs listed.");
		}
		Iterator<String> it = sourceMap.keySet().iterator();
		while( it.hasNext() ) {
			String key = ( String ) it.next();
			String url = sourceMap.get( key );
			logger.info( String.format("initialize source [%s] url [%s]", key, url ));
 		}
	}


	public ArrayList<GenericMessage> multiplex(Message m) throws PluginException {
			
        ArrayList<GenericMessage> payLoadList = new ArrayList<GenericMessage>();
        List<MessageVote> messages = new ArrayList<MessageVote>();
		try {
		       Week week = FanNightDAO.getInstance().getCurrentWeek();
			
			   if(week != null && !week.getId().equals(Week.NO_WEEK_LEFT)) {
					Poll poll = week.getPoll();				//poll = DAO.getInstance().getSampleCogixPoll();
					logger.info("current week is "+week.getId());
					
					if(poll != null) {
						String urlF = sourceMap.get( sourceFacebook );
						if( urlF != null ) {
						   logger.info("Getting Messages from FaceBook");
						   messages.addAll(getVotesFacebook( urlF ));
						}
						String urlI = sourceMap.get( sourceInstagram );
						if( urlI != null ) {
						   logger.info("Getting Messages from Instagram");
						   messages.addAll(getVotesInstagram( urlI ));
						}
						String urlT = sourceMap.get( sourceTwitter );
						if( urlT != null ) {
						   logger.info("Getting Messages from Twitter");
						   messages.addAll(getVotesTwitter( urlT ));
						}
						for( MessageVote vote: messages ) {
						   for( int i = 0; i < vote.getCount(); i++) {	
							   GenericMessage gm = null;
							   gm = (m instanceof GenericMessage)? (GenericMessage) m : GenericMessageFactory.createGenericMessage(m);
							   gm.setStringProperty("body", "<poll id='"+poll.getId()+"' vote='"+vote.getText()+"' source='"+vote.getSource()+"' />");
							   payLoadList.add(gm);
						   }
						}
					}
			   }
	
		} catch (Exception e) {
			logger.error("Error Exception " + e.getMessage(), e );
		}
		
		return payLoadList;
	}
	/**
	 * 
	 * @param urlString
	 * @return
	 * @throws Exception
	 */
	public List<MessageVote> getVotesFacebook(String urlString) throws Exception {
		
		
		URL url = new URL( urlString );
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

		StringBuffer buffer = new StringBuffer();
		String inputLine = "";
		while ((inputLine = in.readLine()) != null)
		    buffer.append(inputLine);
		in.close();
		
		Gson gson = new Gson();
		
		List<MessageVote> votes = null;
			
		Type faceBook = new TypeToken<Facebook>() {}.getType();
		Facebook workFacebook = gson.fromJson(buffer.toString(), faceBook ); 
		if( workFacebook != null ) {
		   votes = new ArrayList<MessageVote> ();
		}
		
		if( workFacebook.getData() != null ) {
			for( Data workF: workFacebook.getData() ) {
               if( workF.getCounts() != null ) {
            	   String voteMsg = cleanUpMsg(workF.getHashtag(), sourceFacebook );
            	   if( voteMsg != null ) {
	            	   MessageVote vote = new MessageVote();
	            	   vote.setSource( sourceFacebook );
	            	   vote.setText( voteMsg );
	            	   if( workF.getCounts() != null )  {
	            		   vote.setCount( updateVoteCounts(workF.getCounts().getTotal(), vote ));
	                   }
	            	   votes.add( vote );
            	   }
            	}
		    }
		}
		
		return votes == null ? new ArrayList<MessageVote>() : votes;
	}
	/**
	 * 
	 * @param urlString
	 * @return
	 * @throws Exception
	 */
	public List<MessageVote> getVotesInstagram(String urlString) throws Exception {
		
		URL url = new URL( urlString );
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

		StringBuffer buffer = new StringBuffer();
		String inputLine = "";
		while ((inputLine = in.readLine()) != null)
		    buffer.append(inputLine);
		in.close();
		
		Gson gson = new Gson();
		
		List<MessageVote> votes = null;
			
	    Type instagam = new TypeToken<ArrayList<Instagram>>() {}.getType();
		List<Instagram> workInstagram = gson.fromJson(buffer.toString(), instagam ); 
		if( workInstagram != null && workInstagram.size() != 0 ) {
			   votes = new ArrayList<MessageVote> ();
		}
			
		for( Instagram workC: workInstagram) {
           if( workC.getCaption() != null ) {
        	   String voteMsg = cleanUpMsg( workC.getCaption().getText() , sourceInstagram );
        	   if( voteMsg != null ) {
	        	   MessageVote vote = new MessageVote();
	        	   vote.setSource( sourceInstagram );
	        	   vote.setText( voteMsg );
	        	   if( workC.getComments() != null )  {
	        		   vote.setCount( updateVoteCounts(workC.getComments().getCounts(), vote ) );
	               }
	        	   votes.add( vote );
        	   }
        	}
	    }
		
		return votes == null ? new ArrayList<MessageVote>() : votes;
	}
	
	/**
	 * 
	 * @param urlString
	 * @return
	 * @throws Exception
	 */
	public List<MessageVote> getVotesTwitter(String urlString) throws Exception {
		
		URL url = new URL( urlString );
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

		StringBuffer buffer = new StringBuffer();
		String inputLine = "";
		while ((inputLine = in.readLine()) != null)
		    buffer.append(inputLine);
		in.close();
		
		Gson gson = new Gson();
		
		List<MessageVote> votes = null;	
			
	    Type tvType = new TypeToken<Twitter>() {}.getType();
	    Twitter workTwitter = gson.fromJson(buffer.toString(), tvType);
		if( workTwitter != null ) {
	       votes = new ArrayList<MessageVote> ();
		}
		if( workTwitter.getBuckets() != null && workTwitter.getBuckets().getTotal() != null  ) {
			for( Total tot : workTwitter.getBuckets().getTotal() ) {
				if( tot != null ) {
					 String voteMsg = cleanUpMsg( tot.getTitle(), sourceTwitter  );
					 if( voteMsg != null ) {
						 MessageVote vote = new MessageVote();
						 vote.setText( voteMsg );
						 vote.setSource( sourceTwitter ); 
						 vote.setCount( updateVoteCounts(tot.getCount(), vote ));
						 votes.add( vote );
					 }					 
				}
			}
		}
		
		return votes == null ? new ArrayList<MessageVote>() : votes;
	}
	/**
	 * 
	 * @param count
	 * @param vote
	 * @return
	 * @throws Exception
	 */
	public int updateVoteCounts( int count, MessageVote vote ) throws Exception {
		
		
		FanNightDAO dao = FanNightDAO.getInstance();
		VotingCounts vCounts = dao.getVotingCounts( vote );
	
		int answer = count - vCounts.getCounter();
		
		vCounts.setCounter( count );
		dao.updateVotingCounts( vCounts );
		
		return answer;
	}
	/**
	 * 
	 * @param msg
	 * @return
	 */
	public String cleanUpMsg( String msg, String source  ) {
		
		StringBuffer workMsg = null;
		if( msg == null ) {
			logger.error(String.format("Invalid Vote Submitted Text <%s> Source <%s> ", msg , source ));
			return null;
		}
		if( msg.startsWith("#")) {
			workMsg = new StringBuffer(msg.substring( 1 ));
		} else {
			workMsg = new StringBuffer( msg );
		}
		if( workMsg.length() != 7 && workMsg.length() != 8 )
		{
			logger.error(String.format("Invalid Vote Submitted Text <%s> Source <%s> ", msg , source ));
			return null;
		}
		String first = workMsg.substring( 0 , 3 );
		first = first.toUpperCase();
		if( ! StringUtils.isAlpha( first )) {
			logger.error(String.format("Invalid Vote Submitted Text <%s> Source <%s> ", msg , source ));
			return null;
		}
		String second = null;
		if(  workMsg.length()  == 7 ) {       // AAAvBBB
			second = workMsg.substring( 4 );
		} else {                              // AAAvsBBB
			second = workMsg.substring( 5 );
		}
		second = second.toUpperCase();
		if( !  StringUtils.isAlpha( second )) {
			logger.error(String.format("Invalid Vote Submitted Text <%s> Source <%s> ", msg , source ));
			return null;
		}
		
		return first + "v" + second ;
	}
}
