package com.turner.nba.fannight.plugin;

import java.util.Hashtable;

import javax.jms.Message;

import org.apache.log4j.Logger;

import com.turner.loki.AbstractPlugin;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.nba.fannight.jpa.FanNightDAO;

public class DeleteVotingCountsTable extends AbstractPlugin {
	
    private Logger logger = Logger.getLogger(this.getClass());
	
	private String classReference = this.getClass().toString();

	
	@Override
	public void initialize(Hashtable props) throws InitializationException {
		
	}	
	
	@Override
	public Object process(Message m) throws PluginException {
		
		boolean answer = false;
		try {
			FanNightDAO dao = FanNightDAO.getInstance();
			answer = dao.deleteVotingCounts();
		} catch (Exception e) {
			logger.error("Deleting the Voting Counts table failed: "+e.getMessage(), e);
			throw new PluginException("Deleting the Voting Counts table failed: "+e.getMessage(), e);
		
		}
		logger.info("VotingCounts Table cleared out " );
		return null;
	}	
}
