package com.turner.nba.fannight.plugin;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Hashtable;

import javax.jms.Message;

import org.apache.log4j.Logger;

import com.turner.nba.fannight.jpa.FanNightDAO;
import com.turner.nba.fannight.jpa.Poll;
import com.turner.nba.fannight.jpa.Week;

import com.turner.loki.AbstractPlugin;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;

public class CacheLatestFanNightPoll extends AbstractPlugin {
	private static final String DELETE_START_FROM = "delete_start_time";
	private static final String DELETE_END_TO = "delete_end_time";
	
	private static final String DELETE_PARAM = "delete_on_these_days";
	private static final String RELOAD_PARAM = "reload_on_these_days";
	private static final String UPDATE_PARAM = "update_on_these_days";
	private static final String INSERT_PARAM = "insert_on_these_days";
	
	private static final SimpleDateFormat df = new SimpleDateFormat("EEEE");
	private static final SimpleDateFormat dt = new SimpleDateFormat("HHmm");
	
	private static final Object POLL_URL_PARAM = "cogix_url";
	
	Logger logger = Logger.getLogger(this.getClass().toString());
	String classReference = this.getClass().toString();

	private String deleteDays = "";
	private String reloadDays = "";
	private String updateDays = "";
	private String insertDays = "";
	
	private String deleteStartTime = "0000";
	private String deleteEndTime = "0800";
	
	private String pollUrl = null;
	
	@Override
	public void initialize(Hashtable props) throws InitializationException {
		logger.info("Initializing " + classReference + "...");
		
		if(props.containsKey(DELETE_START_FROM))
			deleteStartTime = ((String) props.get(DELETE_START_FROM)).toUpperCase();
		
		if(props.containsKey(DELETE_END_TO))
			deleteEndTime = ((String) props.get(DELETE_END_TO)).toUpperCase();
		
		if(props.containsKey(DELETE_PARAM))
			deleteDays = ((String) props.get(DELETE_PARAM)).toUpperCase();
		
		if(props.containsKey(RELOAD_PARAM))
			reloadDays = ((String) props.get(RELOAD_PARAM)).toUpperCase();
		
		if(props.containsKey(UPDATE_PARAM))
			updateDays = ((String) props.get(UPDATE_PARAM)).toUpperCase();
		
		if(props.containsKey(INSERT_PARAM))
			insertDays = ((String) props.get(INSERT_PARAM)).toUpperCase();
		
		if(props.containsKey(POLL_URL_PARAM))
			pollUrl = (String) props.get(POLL_URL_PARAM);
		
		logger.info("INSERTS ON "+insertDays);
		logger.info("DELETES ON "+deleteDays);
	}

	@Override
	public Object process(Message m) throws PluginException {
		FanNightDAO dao = FanNightDAO.getInstance();
		CogixConnection cogix = CogixConnection.getInstance();
		
		try{
			
			String dayOfWeek = df.format(new Date()).toUpperCase();
			String timeOfDay = dt.format(new Date()).toUpperCase();
			
			if( (deleteDays.startsWith(dayOfWeek) && timeOfDay.compareTo(deleteStartTime) > 0) ||
				(deleteDays.endsWith(dayOfWeek) && timeOfDay.compareTo(deleteEndTime) < 0) ||
				(deleteDays.contains(dayOfWeek) && !(deleteDays.startsWith(dayOfWeek) || deleteDays.endsWith(dayOfWeek)))
			){
					logger.info("Today is "+dayOfWeek+" "+timeOfDay+": I will delete the database!");
					dao.deletePolls();
			}else{
				Week week = dao.getCurrentWeek();
				
				if(week != null && !week.getId().equals(Week.NO_WEEK_LEFT)){
					logger.info("Current Week is Week "+week.getId());
					Poll poll = week.getPoll();

					if(poll == null){	//attempt to download the poll if it's not found in the db
						poll = cogix.downloadLatestPoll(week, pollUrl);
						//Poll poll = DAO.getInstance().getSampleCogixPoll();
						poll.setWeek(week);
					}

					logger.info("Current Poll is "+poll.getId());
					logger.info("Today is "+dayOfWeek+" "+timeOfDay+": I will attempt to insert a new poll!");
					dao.insertPoll(poll);

				} else if(week != null && week.getId().equals(Week.NO_WEEK_LEFT)) {
					logger.warn("No new weeks found - perhaps fan night voting is over. Process will not attempt to insert new poll");
				} else {
					logger.error("No Week Found. Process will not attempt to insert new poll");
				}
			}
			
			logger.info("found "+FanNightDAO.getInstance().getPolls().size()+" in db");
		}catch(Exception e){
			logger.error("Caching process failed: "+e.getMessage(), e);
			throw new PluginException(e);
		}
		
		GenericMessage gm = null;
		try {
			gm = (m instanceof GenericMessage)? (GenericMessage) m : GenericMessageFactory.createGenericMessage(m);
		} catch (Exception e) {
			throw new PluginException("Could not create GenericMessage", e);
		}

		return gm;
	}
}
