package com.turner.nba.fannight.plugin;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.turner.nba.fannight.jpa.Poll;
import com.turner.nba.fannight.jpa.PollValue;
import com.turner.nba.fannight.jpa.Week;

public class CogixConnection {
	Logger logger = Logger.getLogger(this.getClass().toString());
	String classReference = this.getClass().toString();
	
	private static CogixConnection instance = null;
	private CogixConnection(){}
	
	public static CogixConnection getInstance() {
		if(instance == null) 
			instance = new CogixConnection();
		
		return instance;
	}
	
	public String getPollUrl(long year, long week, String pollUrl) {
		if(pollUrl == null)
			return "http://poll.nba.com/ViewsFlash/servlet/viewsflash?cmd=getresults&pollid=TESTAREA!fn"+year+"_week"+week+"&spotname=TESTAREA&style=XMLResults";
		else
			return pollUrl.replaceAll("\\{week\\}", week+"").replaceAll("\\{year\\}", year + "").replaceAll("&amp;", "&");
	}

	public Poll downloadLatestPoll(Week week) throws Exception {
		return downloadLatestPoll(week, null);
	}
	
	public Poll downloadLatestPoll(Week week, String pollUrl) throws Exception {
		logger.info("retrieving latest poll from cogix");
		
		CogixConnection cogix = CogixConnection.getInstance();
		String pu = getPollUrl(week.getWeekYear(), week.getId(), pollUrl);
		
		URL url = new URL(pu);
		
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db = dbf.newDocumentBuilder();
		Document doc = (Document) db.parse(url.openStream());
		
		XPathFactory xpathFactory = XPathFactory.newInstance();
		XPath xpath = xpathFactory.newXPath();
		
		XPathExpression answersXpath = xpath.compile("//question[@name='Vote']/answer");
		XPathExpression pollIDXpath = xpath.compile("@pollid");
		XPathExpression valueXpath = xpath.compile("@value");
		XPathExpression countXpath = xpath.compile("@count");
		NodeList list = (NodeList) answersXpath.evaluate(doc.getDocumentElement(), XPathConstants.NODESET);
	
		String id = (String) pollIDXpath.evaluate(doc.getDocumentElement(), XPathConstants.STRING);
		
		Poll poll = new Poll();
		poll.setId(id);
		List<PollValue> values = new ArrayList<PollValue>();
		
		for(int i = 0; i < list.getLength(); i++){
			String v = (String) valueXpath.evaluate(list.item(i), XPathConstants.STRING);
			
			PollValue pv = new PollValue();
			pv.setId(v);
			pv.setValue(v);
			pv.setPoll(poll);
			
			String c = (String) countXpath.evaluate(list.item(i), XPathConstants.STRING);
			pv.setCount(c);
	
			values.add(pv);
		}
		
		poll.setValues(values);
		poll.setWeek(week);
		
		return poll;
	}

	public boolean sendVote(String cogixUrl, String pollId, String text, String source ) throws Exception{
		URL url = new URL(cogixUrl);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoInput(true);
		conn.setDoOutput(true);
		conn.setInstanceFollowRedirects(true);
		conn.setRequestMethod("POST");
		conn.setRequestProperty("Content-Type", "application/x-www-form-urlencoded"); 
		conn.setRequestProperty("charset", "utf-8");
		String paramsText = "pollid="+pollId+"&%s=yes&cmd=tally&Vote="+text+"&source=%s";
		
		String params = String.format( paramsText, source, source );
		logger.info("Sending votes to "+cogixUrl+"?"+params);
		conn.setRequestProperty("Content-Length", "" + Integer.toString(params.getBytes().length));
		conn.setUseCaches (false);
	
		DataOutputStream dout = new DataOutputStream(conn.getOutputStream());
		dout.writeBytes(params);
		dout.flush();
		dout.close();
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(new DataInputStream(conn.getInputStream())));
		String line;
		StringBuffer buffer = new StringBuffer();
		while((line = reader.readLine()) != null)
			buffer.append(line);
		
		logger.debug(buffer.toString());
		
		reader.close();
		conn.disconnect();
		
		logger.info("Done pushing vote to cogix: "+text);
		return true;
	}

}
