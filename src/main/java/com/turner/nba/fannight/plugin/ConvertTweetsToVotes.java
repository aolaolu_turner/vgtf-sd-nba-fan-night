package com.turner.nba.fannight.plugin;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Hashtable;
import java.util.List;

import javax.jms.Message;

import org.apache.log4j.Logger;

import com.turner.nba.fannight.jpa.FanNightDAO;
import com.turner.nba.fannight.jpa.Poll;
import com.turner.nba.fannight.jpa.PollValue;
import com.turner.nba.fannight.jpa.Team;
import com.turner.nba.fannight.jpa.Week;
import com.turner.nba.fannight.util.TeamFieldLengthComparator;
import com.turner.nba.fannight.util.TwitterVote;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;

import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.loki.core.interfaces.InitializableIF;
import com.turner.loki.core.interfaces.MonitorableIF;
import com.turner.loki.core.interfaces.MultiplexIF;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;


public class ConvertTweetsToVotes implements MonitorableIF, MultiplexIF, InitializableIF {
	private Logger logger = Logger.getLogger(this.getClass());
	
	private String massRelevanceUrl = "http://tweetriver.com/NBATV/fan-night-voting.json";
	private String classReference = this.getClass().toString();

	@Override
	public void initialize(Hashtable props) throws InitializationException {
		logger.info("Initializing "+this.classReference + "...");

		if(props.containsKey("tweetriver_url"))
			massRelevanceUrl = (String) props.get("tweetriver_url");
		else
			logger.info("No tweetriver_url param set. Using default value");
		
		if(massRelevanceUrl == null || "".equals(massRelevanceUrl.trim()))
			throw new InitializationException("Initialization error: Mass Relevance URL is missing.");
	}

	public ArrayList<GenericMessage> multiplex(Message m) throws PluginException {
		ArrayList<GenericMessage> al = new ArrayList<GenericMessage>();
		
		Week week = null;
		Poll poll = null;
		List<TwitterVote> tweets = new ArrayList<TwitterVote>();
		
		try{
			week = FanNightDAO.getInstance().getCurrentWeek();
			
			if(week != null && !week.getId().equals(Week.NO_WEEK_LEFT)){
				poll = week.getPoll();				//poll = DAO.getInstance().getSampleCogixPoll();
				logger.info("current week is "+week.getId());
				
				if(poll != null){
					tweets = getTweets(poll.getPreviousTwitterVoteId());	//java.util.Collections.sort(tweets); 
					logger.info("Retrieved tweets as votes: "+tweets.size());
	
					if(!tweets.isEmpty()){
						List<Team> teams = FanNightDAO.getInstance().getTeams();
						al = processTweets(m, poll, tweets, teams);
						logger.info("attempting to send " + al.size() +" votes to cogix");
	
						//assert(poll.getPreviousTwitterVoteId() != "0");		//update the database with the most recent tweets
						FanNightDAO.getInstance().updatePoll(poll.getId(), poll.getPreviousTwitterVoteId());
						logger.info("updated cache with last vote information");
					}else
						logger.warn("No tweets found. Skipping rest of process");
				}else
					logger.warn("No poll found: wait for Cache process to download poll");
			}else
				logger.warn("No week found: This process will not continue without a valid week");
		} catch (Exception e) {
			logger.error("Process failed: "+e.getMessage(), e);
			throw new PluginException("Could not create GenericMessage", e);
		}
		
		return al;
	}

	public ArrayList<GenericMessage> processTweets(Message m, Poll poll, List<TwitterVote> tweets, List<Team> teams)
			throws Exception {
		ArrayList<GenericMessage> list = new ArrayList<GenericMessage>();
		
		for(TwitterVote tweet:tweets){		//send the tweets to cogix
			logger.info("processing tweet: "+tweet.getText());
			poll.setPreviousTwitterVoteId(tweet.getId());

			String text = convertTweetToPollValueID(poll, teams, tweet.getText());
			
			try{
				if(text != null){
					logger.info("Submitting "+text+" for "+tweet.getText());
					
					GenericMessage gm = null;
					gm = (m instanceof GenericMessage)? (GenericMessage) m : GenericMessageFactory.createGenericMessage(m);
					gm.setStringProperty("body", "<poll id='"+poll.getId()+"' vote='"+text+"' />");
					list.add(gm);
				}else
					logger.warn("Nothing done to Tweet: "+tweet.getText() +" maps to nothing!");
			}catch(Exception e){
				logger.warn("Could not send "+text+" to cogix: "+e.getMessage(), e);
			}
		}
		
		return list;
	}
	
	public List<TwitterVote> getTweets(String previousId) throws Exception {
		logger.info("retrieving latest feeds from mass relevance using id "+previousId);
		
		URL url = new URL(massRelevanceUrl+"?since_id="+(previousId==null || previousId.trim().equals("")?"0":previousId));
		BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));

		StringBuffer buffer = new StringBuffer();
		String inputLine = "";
		while ((inputLine = in.readLine()) != null)
		    buffer.append(inputLine);
		in.close();
		
		Gson gson = new Gson();
		Type tvType = new TypeToken<ArrayList<TwitterVote>>() {}.getType();
		List<TwitterVote> votes = gson.fromJson(buffer.toString(), tvType);
		
		if(votes != null){
			Collections.sort(votes);
			logger.info("Found "+votes.size()+ " votes");
			for(TwitterVote vote:votes)
				logger.info(vote);
		}
		
		return votes == null ? new ArrayList<TwitterVote>() : votes;
	}
	
	public String convertTweetToPollValueID(Poll poll, List<Team> teams, String text) throws Exception {
		try{
			if(teams.isEmpty())
				throw new Exception("No Team information found. Cannot continue");
			
			Team voted = getMatchingTeam(teams, text.toLowerCase());
			
			if(voted != null){
				logger.info("Found team for tweet [" + text + "]: " + voted.toString());
				
				for(PollValue pv : poll.getValues()){	
					if(pv.getValue().toLowerCase().contains(voted.getAbbreviation().toLowerCase()))
						return pv.getId();
					//else
						//logger.info(voted.getAbbreviation() + " not Found: "+pv.getValue());
				}
			}
			
			logger.warn("No vote found or there are no games matching the text: "+text);
			return null;
		}catch(Exception e){
			logger.error("Cannot convert tweet to corresponding poll vote: "+e.getMessage(), e);
			throw new Exception(e);
		}
	}

	public Team getMatchingTeam(List<Team> teams, String text) throws Exception {
		if(teams == null || teams.isEmpty())
			throw new Exception("No Teams Found. Nothing to do - I will attempt to crash!");
		
		//search through the teams first using all cities, then again through all teams using name, etc
		for(String category: Team.SEARCH_ORDER){
			java.util.Collections.sort(teams, new TeamFieldLengthComparator(category));
			//logger.info("Category: "+category);
			
			for(Team team:teams){
				//logger.info(text+ "\t=>\t"+team.toString());
				if(category.equals(Team.CITY) && !team.getCity().trim().equals("") && text.contains(team.getCity().toLowerCase()))
					return team;
				if(category.equals(Team.NAME) && !team.getName().trim().equals("") && text.contains(team.getName().toLowerCase()))
					return team;
				if(category.equals(Team.NICKNAME) && team.getNickname() != null && !team.getNickname().trim().equals("") && text.contains(team.getNickname().toLowerCase()))
					return team;
				if(category.equals(Team.ABBREVIATION) && !team.getAbbreviation().trim().equals("") && text.contains(team.getAbbreviation().toLowerCase()))
					return team;
			}
		}
		
		logger.warn("No team found matching tweet: "+text);		
		return null;
	}

	public String getMassRelevanceUrl() {
		return massRelevanceUrl;
	}
}
