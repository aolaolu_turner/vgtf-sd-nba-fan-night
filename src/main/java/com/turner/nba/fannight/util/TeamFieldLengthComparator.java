package com.turner.nba.fannight.util;

import java.util.Comparator;

import com.turner.nba.fannight.jpa.Team;

public class TeamFieldLengthComparator implements Comparator<Team>{
	private String field = Team.CITY;
	
	public TeamFieldLengthComparator(String fieldname){
		this.field = fieldname;
	}
	
	public int compare(Team o1, Team o2) {
		int length1 = 0;
		int length2 = 0;
		
		if(field.equals(Team.CITY)){
			length1 = o1.getCity().length();
			length2 = o2.getCity().length();
		}
		
		if(field.equals(Team.NAME)){
			length1 = o1.getName().length();
			length2 = o2.getName().length();
		}
		
		if(field.equals(Team.NICKNAME)){	//check for nulls since sometimes we don't have nicknames
			if(o1.getNickname() == null)
				return 1;
			else if(o2.getNickname() == null)
				return -1;
			
			length1 = o1.getNickname().length();
			length2 = o2.getNickname().length();
		}
		
		if(field.equals(Team.ABBREVIATION)){
			length1 = o1.getAbbreviation().length();
			length2 = o2.getAbbreviation().length();
		}
			
		return length1 < length2 ? 1 : length1 > length2 ? -1 : 0;
	}
	
}