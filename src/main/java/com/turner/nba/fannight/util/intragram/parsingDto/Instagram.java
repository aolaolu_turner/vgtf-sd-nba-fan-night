package com.turner.nba.fannight.util.intragram.parsingDto;

import java.util.ArrayList;
import java.util.List;

public class Instagram {
	
    private Caption    caption;
	private Comments   comments;
    
	
	public Caption getCaption() {
		return caption;
	}
	public void setCaption(Caption caption) {
		this.caption = caption;
	}
    public Comments getComments() {
		return comments;
	}
	public void setComments(Comments comments) {
		this.comments = comments;
	}

   
}
