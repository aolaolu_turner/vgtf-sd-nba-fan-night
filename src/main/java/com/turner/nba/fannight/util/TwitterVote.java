package com.turner.nba.fannight.util;

import java.math.BigInteger;

public class TwitterVote implements Comparable<TwitterVote> {
	private String id;
	private String text;

	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getText() {
		return text.toLowerCase().replaceAll("@nbatv", "").replaceAll("#fannightvote", "");
	}
	public void setText(String text) {
		this.text = text.toLowerCase();
	}

	@Override
	public String toString(){
		return "tweetID: "+id+" => "+text;
	}
	
	public int compareTo(TwitterVote o) {
		BigInteger lhs = new BigInteger(id);
		BigInteger rhs = new BigInteger(o.getId());
			
		return lhs.compareTo(rhs);
	}
}
