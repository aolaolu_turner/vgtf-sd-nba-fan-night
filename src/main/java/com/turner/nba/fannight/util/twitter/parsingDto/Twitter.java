package com.turner.nba.fannight.util.twitter.parsingDto;

public class Twitter {
	
  private Buckets buckets;

  public Buckets getBuckets() {
	return buckets;
  }

  public void setBuckets(Buckets buckets) {
	this.buckets = buckets;
  }
}
