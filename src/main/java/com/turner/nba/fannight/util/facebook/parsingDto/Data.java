package com.turner.nba.fannight.util.facebook.parsingDto;

public class Data {
	private String hashtag;
	private Counts counts;
	
    public String getHashtag() {
		return hashtag;
	}
	public void setHashtag(String hashtag) {
		this.hashtag = hashtag;
	}
	public Counts getCounts() {
		return counts;
	}
	public void setCounts(Counts counts) {
		this.counts = counts;
	}

}
