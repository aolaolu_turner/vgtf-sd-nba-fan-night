package com.turner.nba.fannight.util.facebook.parsingDto;

import java.util.List;

public class Facebook {
   private List<Data> data;

   public List<Data> getData() {
	  return data;
   }

   public void setData(List<Data> data) {
	  this.data = data;
   }
}
