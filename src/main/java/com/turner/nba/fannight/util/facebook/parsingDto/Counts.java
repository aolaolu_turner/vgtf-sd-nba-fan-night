package com.turner.nba.fannight.util.facebook.parsingDto;

public class Counts {
  private int total;

  public int getTotal() {
	return total;
  }

  public void setTotal(int total) {
	this.total = total;
  }
}
