package com.turner.nba.ws.dao;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;

import javax.persistence.EntityManager;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.turner.loki.util.PersistenceManager;

/***************************************************************************************************************
 * 
 * @author Jerry You
 * @author Akinola
 *
 * This class has quite a few bugs/restrictions:
 * 
 * 1. column name retrieval from the script fails if there are nested functions in the column names. 
 *   		As a work-around, you can pass the expected column names directed to the appropriate function
 *   
 * 2. this class was designed mainly for 'select' statements. I won't recommend attempting an update with it
 * 
 * 3. column name retrieval from the script also fails if you use distinct or top after 'select' and before 'from'.
 *   		As a work-around, you can pass the expected column names directed to the appropriate function
 *   
 */
public class HelloServiceDAO {
	Logger log = Logger.getLogger(this.getClass());
	
	public String getJsonResultListByNativeQuery(String persistenceUnitName, String sql){
		List<Map<String, String>> result = getResultListByNativeQuery(persistenceUnitName, sql);
		Gson gson = new Gson();
		return gson.toJson(result);
	}
	
	public String getJsonResultListByNativeQuery(String persistenceUnitName, String sql, String[] columns){
		List<Map<String, String>> result = getResultListByNativeQuery(persistenceUnitName, sql, columns);
		Gson gson = new Gson();
		return gson.toJson(result);
	}
	
	public List<Map<String, String>> getResultListByNativeQuery(String persistenceUnitName, String sql){
		//parse the input sql, find the column names between the "select" and "from"
		//for example, select game_id, game_dt from game where game_id = "12345"
		String cleanSql = sql.trim();
		int selectionBegin = cleanSql.toLowerCase().indexOf("select")+"select".length();
		int selectionEnd = cleanSql.toLowerCase().indexOf("from", selectionBegin)-1;
		String selections = cleanSql.substring(selectionBegin, selectionEnd);
		//need to remove the function ( ) since they tend to have , inside
		String cleanSelections = selections.replaceAll("\\(.+?\\)", "functionstuff");
		log.debug(cleanSelections);
		String[] columns = cleanSelections.split(",");
		
		return getResultListByNativeQuery(persistenceUnitName, sql, columns);
	}
	
	public List<Map<String, String>> getResultListByNativeQuery(String persistenceUnitName, String sql, String[] columns){
		List<Map<String, String>> results = new ArrayList<Map<String, String>>();
		EntityManager em = null;
		
		try{
			em = PersistenceManager.getEntityManager(persistenceUnitName);
			
			List tmp = (List) em.createNativeQuery(sql).getResultList();
			log.info("Found "+tmp.size()+" record...");
			List<Object[]> reses = new ArrayList<Object[]>();
			
			//if it's a single column query hibernate returns only one object instead of an array of objects
			if(columns.length == 1){
					String column = (String)tmp.get(0);
					
					for(Object i: tmp)
						reses.add(new String[]{ (String)i });
			}else{
					reses = tmp;		
			}
			
			Iterator it = reses.iterator();
			while(it.hasNext()){
				Object[] res =(Object[]) it.next();
			
				if(res == null){
					log.warn("Found wierd null record :(");
					continue;
				}
				
				Map<String, String> pretty = new HashMap<String, String>();
				for(int i=0;i<columns.length;i++){
					String column = columns[i].trim();

					if(column.toLowerCase().indexOf(" as ")==-1){
						//no "as"
						pretty.put(column.trim().toUpperCase(), res.length <= i ||  res[i]==null?"":res[i].toString());
					}else{
						// to_char(game_dt,'yyyymmdd') as game_date
						String alias = column.substring(column.indexOf(" as ")+" as ".length());
						pretty.put(alias.trim().toUpperCase(), res.length <= i || res[i]==null?"":res[i].toString());
					}
				}
				results.add(pretty);
			}
			return results;
		}finally{
			if(em!=null){
				PersistenceManager.closeEntityManager(persistenceUnitName);
			}
		}
	}
}
