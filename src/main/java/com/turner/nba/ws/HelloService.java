package com.turner.nba.ws;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.jms.Message;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.sun.jersey.api.core.HttpContext;
import com.turner.nba.util.Helper;
import com.turner.nba.ws.dao.HelloServiceDAO;

import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;

import com.turner.loki.AbstractPlugin;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;

import org.jdom.Document;
import org.jdom.JDOMException;
import org.jdom.xpath.XPath;
import org.jaxen.jdom.JDOMXPath;
import org.jdom.Element;

@Path("/hello")
public class HelloService  extends AbstractPlugin {
	Logger log = Logger.getLogger(this.getClass());
	HelloServiceDAO dao = new HelloServiceDAO();
	
	@Context
    private HttpServletRequest request;
	@Context
    private HttpContext hc;
	
	@GET
	@Produces("text/plain")
	public String getGreeting(){
		return "salut, mon ami";
	}
	
	@Path("/{persistenceUnit}")
	@POST
	public String query(@PathParam("persistenceUnit") String persistenceUnit){
		log.info("at least I got here :)");
		Map<String, String> formParams = queryParams();
		String returnVal = "{}";
		
		if(formParams.containsKey("select")){
			try{
				List<Map<String, String>> list = 
					dao.getResultListByNativeQuery(persistenceUnit, formParams.get("select"));
			
				Gson g = new Gson();
				returnVal = g.toJson(list);
			}catch(Exception e){
				log.error(e.getMessage(), e);
			}
		}
		
		return returnVal;
	}
	
	private Map<String, String> queryParams(){
		MultivaluedMap<String, String> params = hc.getUriInfo().getQueryParameters();
		log.info("request:" + hc.getUriInfo().getRequestUri());
		
		Map<String, String> formParams = new HashMap<String, String>();
		
		for(Entry<String, List<String>> entry: params.entrySet()){
			List<String> values = entry.getValue();
			String value = (values == null || values.isEmpty()?"":values.get(0));
			
			formParams.put(entry.getKey(), value);
		}
			
		log.debug(formParams);
		return formParams;
	}

	@Override
	public void initialize(Hashtable arg0) throws InitializationException {
		
	}

	@Override
	public Object process(Message m) throws PluginException {
		GenericMessage gm = null;

		try{
			if (m instanceof GenericMessage) 
				gm = (GenericMessage) m;
			else 
				gm = GenericMessageFactory.createGenericMessage(m);
			
			String bodyStr = gm.getStringProperty("body");
			log.info(bodyStr);
			
			Document doc = Helper.convertStringToDocument(bodyStr);	
			
			gm.setStringProperty("body", "{}");
		} catch (Exception e) {
			log.error("Could not run query: "+e.getMessage(), e);
			throw new PluginException("Could not create GenericMessage", e);
		}
		
		
		return gm;
	}
	
}
