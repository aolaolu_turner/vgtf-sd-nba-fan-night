package com.turner.nba.tweets.plugin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.jms.Message;

import org.apache.log4j.Logger;
import org.joda.time.DateTimeZone;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.loki.AbstractPlugin;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.nba.tweets.jpa.TeamTweetDAO;
import com.turner.nba.tweets.util.TimeCount;

/**
 * This plugin create the tweet counts feed by couning tweets with predefined interval
 *
 */
public class CountGameTweets  extends AbstractPlugin{
	private static final String SHOW_ONLY_TOTALS = "showOnlyTotals";
	public org.apache.log4j.Logger logger = Logger.getLogger(this.getClass());
	int publishIntervalInMinutes = 1; //1 minute
	int publishMinutesBeforeGameStart = 15; //15 minutes
	DateTimeZone timezone = DateTimeZone.forID("America/New_York");
	static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	private String path = "/nba/data/json/cms/noseason/";
	private String[] teamNamesWithTotalsOnly = {};
	private String teamTweetSuffix = "";
	
	//we have team nick name 76ers, but at twitter and our teamtweet table we have "sixers"
	private Map<String, String> nickNameToSocialNameMap = new HashMap<String, String>();
	
	@Override
	public void initialize(Hashtable props) throws InitializationException {
		if(props.containsKey("publishIntervalInMinutes")){
			publishIntervalInMinutes = Integer.parseInt((String)props.get("publishIntervalInMinutes"));
		}else{
			throw new InitializationException("You need to set the publishIntervalInMinutes");
		}
		if(props.containsKey("publishMinutesBeforeGameStart")){
			publishMinutesBeforeGameStart = Integer.parseInt((String)props.get("publishMinutesBeforeGameStart"));
		}else{
			throw new InitializationException("You need to set the publishMinutesBeforeGameStart");
		}
		if(props.containsKey("path")){
			path = (String) props.get("path");
		}
		
		if(props.containsKey("teamTweetSuffix"))	//this allows us to search for heat-fans instead of heat, for instance. '-fans' is the suffix
			teamTweetSuffix = (String)props.get("teamTweetSuffix");
		
		nickNameToSocialNameMap.clear();
		
		for(Object key:props.keySet()){
			
			if(((String)key).startsWith("nickNameToSocialNameMap")){
				String property = (String) props.get(key);
				String[] pair = property.split("\\|");
				
				if(pair.length >= 2){
					nickNameToSocialNameMap.put(pair[0], pair[1]);
				}else
					logger.warn("This value isn't well set: "+property);
			}
		}
		
		if(props.containsKey(SHOW_ONLY_TOTALS)){
			String value = (String) props.get(SHOW_ONLY_TOTALS);
			teamNamesWithTotalsOnly = value.split("\\|");
		}else{
			logger.warn("Creating full social volume feed for all teams - no summary feeds will be created!");
		}
	}

	@Override
	public Object process(Message m) throws PluginException {
		GenericMessage gm = null;
		try{
			if (m instanceof GenericMessage) {
				gm = (GenericMessage) m;
			}else{ 
				gm = GenericMessageFactory.createGenericMessage(m);
			}
			//
			String filename = "social_volume.json";
			String gameId = gm.getStringProperty("gameId");
			if(gameId != null && gameId.endsWith(GetGamesInWindow.SYNC_SUFFIX)){
				gameId = gameId.split(GetGamesInWindow.SYNC_SUFFIX)[0];
				filename = "social_volume_v2.json";
			}
			
			String gameDateStr = gm.getStringProperty("gameDate");
			String visitor = gm.getStringProperty("visitor");
			String home = gm.getStringProperty("home");
			Date gameDate = gameDateStr==null?null:dateFormat.parse(gameDateStr);
			if(gameId!=null && (gameDateStr==null || visitor==null || home==null)){
				Game g = TeamTweetDAO.getInstance().getGameById(gameId);
				visitor = g.getVisitorNickname();
				home = g.getHomeNickname();
				gameDate = g.getGameDate();
			}
			
			String homeSocialName = this.getSocialNameFromNickName(home);
			String visitorSocialName = this.getSocialNameFromNickName(visitor);
			logger.debug(gameId + " " + visitor+ " " + visitorSocialName +" " + home + " " + homeSocialName+" "+ gameDateStr);
			
			boolean skipBuckets = false;
			for(String name: teamNamesWithTotalsOnly){
				if(name.equals(homeSocialName)){
					skipBuckets = true;
					break;
				}
			}
			
			if(this.timeIsRight(gameDate, publishIntervalInMinutes)){
				Date now = new Date();
				if(gm.getStringProperty("now")!=null){
					now = dateFormat.parse(gm.getStringProperty("now"));
				}
				Map<String, Object> visitorCounts = this.countTweetsBetweenTimestamp(visitorSocialName, gameDate, this.publishMinutesBeforeGameStart, this.publishIntervalInMinutes, now);
				Map<String, Object> homeCounts = this.countTweetsBetweenTimestamp(homeSocialName, gameDate, this.publishMinutesBeforeGameStart, this.publishIntervalInMinutes, now);
				//create the output
				JSONObject jsonObject = jsonizeGameTweetCounts(visitorCounts, homeCounts, skipBuckets);
				String jsonString = jsonObject.toString();
				//write it out
				gm.setStringProperty("body", jsonString);
				logger.info("publishing "+path+"game/"+gameId+"/social/"+publishIntervalInMinutes+"m/" + filename);
				gm.setStringProperty("filename", path+"game/"+gameId+"/social/"+publishIntervalInMinutes+"m/" + filename);
				gm.setStringProperty("absoluteDestinationFileNames", path+"game/"+gameId+"/social/"+publishIntervalInMinutes+"m/" + filename);
			}else{
				logger.debug("nope I am gonna wait");
			}
		}catch(Exception e){
			logger.error("Process failed: "+e.getMessage(), e);
			throw new PluginException("Can't process message.", e);
		}
		
		return gm;
	}

	
   public String getSocialNameFromNickName(String name) {
		// given 76ers return sixers
		// given trail blazers return trailblazers
		// first remove space
		String nom = name.replaceAll(" ", "").toLowerCase();
		
		if(nickNameToSocialNameMap.containsKey(nom))
			return nickNameToSocialNameMap.get(nom);
		else
		return nom;
		
	}

/**
 * Create the output JSON from the visitor and home tweets data
 * @param visitorCounts
 * @param homeCounts
 * @return
 */
public JSONObject jsonizeGameTweetCounts(
			Map<String, Object> visitorCounts, Map<String, Object> homeCounts, boolean skipBuckets) {
		SimpleDateFormat outputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		JSONObject jsonObject = new JSONObject();
		//summary
		Integer visitorTotal = (Integer)visitorCounts.get("total");
		Integer homeTotal = (Integer)homeCounts.get("total");
		Date publishTime = (Date)visitorCounts.get("timestamp");
		Date publishUtc = new Date(timezone.convertLocalToUTC(publishTime.getTime(), false));
		JSONObject totalObject = new JSONObject();
		totalObject.put("t", outputFormat.format(publishUtc)+"Z");
		totalObject.put("tm", publishTime.getTime());
		totalObject.put("v", visitorTotal);
		totalObject.put("h", homeTotal);
		jsonObject.put("total", totalObject);
		
		if(skipBuckets)
			return jsonObject;
		
		List<TimeCount> visitorBuckets = (List<TimeCount>)visitorCounts.get("buckets");
		List<TimeCount> homeBuckets = (List<TimeCount>)homeCounts.get("buckets");
		//the two lists have guaranteed time-ordering since they come from same game
		if(visitorBuckets.size()!=homeBuckets.size()){
			throw new RuntimeException("it is weired! the visitor buckets and home buckets have different size");
		}
		JSONArray counts = new JSONArray();
		for(int i=0;i<visitorBuckets.size();i++){
			TimeCount visitorTc = visitorBuckets.get(i);
			TimeCount homeTc = homeBuckets.get(i);
			Date time = visitorTc.time; //system time Eastern
			DateTimeZone timezone = DateTimeZone.forID("America/New_York");
			Date dateUtc = new Date(timezone.convertLocalToUTC(time.getTime(), false));
			Integer visitorTweets = visitorTc.count;
			Integer homeTweets = homeTc.count;
			//percentage
			Integer persentageVisitor = 50;
			Integer persentageHome = 50;
			if(visitorTweets+homeTweets!=0){
				persentageVisitor = (int)Math.round((double)(visitorTweets*100/(visitorTweets+homeTweets)));
				persentageHome = 100-persentageVisitor; 
			}
			
			JSONObject visitorAndHome = new JSONObject();
			visitorAndHome.put("t", outputFormat.format(dateUtc)+"Z");
			visitorAndHome.put("tm", time.getTime());
			//visitor number and percentage
			JSONObject visitor = new JSONObject();
			visitor.put("c", visitorTweets);
			visitor.put("p", persentageVisitor);
			visitorAndHome.put("v", visitor);
			//home number and percentage
			JSONObject home = new JSONObject();
			home.put("c", homeTweets);
			home.put("p", persentageHome);
			visitorAndHome.put("h", home);
				
			counts.add(visitorAndHome);
			
		}
		jsonObject.put("counts", counts);
		return jsonObject;
	}


	/**
	 * count the team tweets, it will first create the buckets based on the game start time and publish-minutes-before-game-start,
	 * then it iterate the tweets in database, and group the tweets into buckets
	 * 
	 * @param teamName
	 * @param gameStartTime
	 * @param publishIntervalInMinutes
	 * @param stopTime
	 * @return
	 */
	public Map<String, Object>  countTweetsBetweenTimestamp(String teamName,
			Date gameStartTime, int publishMinutesBeforeGameStart,  int publishIntervalInMinutes, Date stopTime) {
		Map<String, Object> res = new HashMap<String, Object>();
		res.put("team", teamName);
		//create the empty buckets based on the game start time
		List<TimeCount> buckets = this.createTweetCountBuckets(gameStartTime, publishMinutesBeforeGameStart, publishIntervalInMinutes, stopTime);
		//we will query database and group the feeds using above time-slots, note that in database, we use the column "collectionTime"
		//which is the time we poll the feeds (not exactly the feed post time)
		//
		//in our database there are "gaps" in the time table when there is no new feed at all
		//CollectTime	ID
		//6:45:01 		1
		//6:45:01		2
		//6:45:01		3 (chances are the first collectionTime has more feeds than other time since  it has all the feeds before)
		//......
		//6:45:31 		n
		//6:46:01 		n+1
		//6:48:31		n+2
		//....
		//select to_char(collectionTime,'YYYY-MM-DD HH:MI:00') as collectionTimeAtMinute, count(id) as tweetsCount from teamtweet where team=:teamName group by to_char(collectionTime,'YYYY-MM-DD HH:MI:00')
		//we will have
		//			    6:45:00				    x
		//				6:46:00					y
		//				6:48:00					z
		//				6:50:00					v
		//				6:56:00					z
		List<TimeCount> tweetsCountByMinutes =  TeamTweetDAO.getInstance().countTweetsByMinute(teamName+teamTweetSuffix);
/*		if(logger.isDebugEnabled()){
			for(TimeCount tc:tweetsCountByMinutes){
				logger.debug(tc.time+ " " + tc.count);
			}
		}*/
		//fill in the buckets using the database as reference
		fillBucketsWithTweetCounts(buckets, tweetsCountByMinutes);
		
		res.put("buckets", buckets);
		//calculate total tweet counts
		Integer total = 0;
		Calendar publishTime = Calendar.getInstance();
		publishTime.set(Calendar.SECOND, 0);
		if(buckets.isEmpty()){
			
		}else{
			//calculate total sum, and use the last time point as publish time
			for(TimeCount tc:buckets){
				logger.debug("\t"+dateFormat.format(tc.time) +"\t\t"+tc.count);
				total+=tc.count;
				publishTime.setTime(tc.time);
			}
		}
		logger.debug("At " + dateFormat.format(publishTime.getTime()) + " total:" + total);
		res.put("total", total);
		res.put("timestamp", publishTime.getTime());
		return res;
	}
	
	/**
	 * create the empty buckets from the game start time
	 * given game start time, we know exactly the time markers, starting from 15 minutes before game until now
	 * say game start at 7:00
	 * 6:45:00 6:55:00 7:05:00 7:15:00 7:25:00 7:35:00 .....now(10-min)
	 * @param gameStartTime
	 * @param publishMinutesBeforeGameStart
	 * @param publishIntervalInMinutes
	 * @param stopTime
	 * @return
	 */
	public List<TimeCount> createTweetCountBuckets(Date gameStartTime, int publishMinutesBeforeGameStart, int publishIntervalInMinutes, Date stopTime){
		List<TimeCount> counts = new ArrayList<TimeCount>();
		
		Calendar endTime = Calendar.getInstance();
		endTime.setTime(stopTime);
		
		Calendar firstTimeMarker = Calendar.getInstance();
		firstTimeMarker.setTime(gameStartTime);
		firstTimeMarker.set(Calendar.SECOND, 0);
		firstTimeMarker.add(Calendar.MINUTE, -publishMinutesBeforeGameStart);
		
		if(endTime.before(firstTimeMarker)){
			//should not happen....because this action is executed during the 15-min game window
			return counts;
		}
		
		//create the map with default 0 tweet
		Calendar c = firstTimeMarker;
		while(!c.after(endTime)){
			counts.add(new TimeCount(c.getTime(),0));
			//next time marker
			c.add(Calendar.MINUTE, publishIntervalInMinutes);
		}
/*		if(logger.isDebugEnabled()){
			StringBuilder sb = new StringBuilder();
			sb.append("game start at:"+dateFormat.format(gameStartTime) + " publish before game starts:"+publishMinutesBeforeGameStart + " every " + publishIntervalInMinutes + " until " + dateFormat.format(stopTime));
			for(TimeCount map:counts){
				sb.append("\n\t"+dateFormat.format(map.time) +"->"+ map.count);
			
			}
			logger.debug("Initial Map\n"+sb.toString());
		}*/
		return counts;
	}
	
	/**
	 * fill in the buckets with grouped tweet counts from database
	 * 
	 * @param tweetCountsBuckets
	 * @param tweetsCountByMinutes
	 */
	public void fillBucketsWithTweetCounts(List<TimeCount> tweetCountsBuckets, List<TimeCount> tweetsCountByMinutes ){
		//in tweetCountsBuckets, the key is fixed time marker (every 10 minutes), value by default is 0
		// 6:45:00	0
		// 6:55:00  0
		// 7:05:00	0
		//...
		//then we re-group by the time-marker
		//				(,6:45:00)				x
		//				(6:45:00,6:55:00]		y+z+v
		
		for(int i=0;i<tweetCountsBuckets.size();i++){
			TimeCount currentTimeSlot = tweetCountsBuckets.get(i);
			Date currentTime = currentTimeSlot.time;
			String currentTimeString = this.dateFormat.format(currentTime);
			int sum = 0;
			if(i==0){
					//for the first entry, we do not want to show the number of tweets BEFORE this point, otherwise, you will see
					// 6:45:00		100
					// 6:55:00		0
					// 7:05:00		1
					//this will give a wrong impression that there is a huge drop of tweets
					// 
			}else{
					TimeCount lastTimeSlot = tweetCountsBuckets.get(i-1);
					Date lastTime = lastTimeSlot.time;
					String lastTimeString = this.dateFormat.format(lastTime);
					//find the sum of tweets between lastTime and currentTime
					//logger.debug("filling for " + currentTimeString + " between " + lastTimeString + " to " + currentTimeString);
					for(int j=0; j<tweetsCountByMinutes.size();j++){
						TimeCount timeAndCount = tweetsCountByMinutes.get(j);
						String tweetsTime = dateFormat.format(timeAndCount.time);
						Integer count = timeAndCount.count; 
						//logger.debug("checking " + tweetsTime + " " + count);
						if(tweetsTime.compareTo(lastTimeString)>0 && (!(tweetsTime.compareTo(currentTimeString)>0))){
							//(lastTime,currentTime]
							//logger.debug("add [" + tweetsTime + "] between [" + lastTimeString+"] and [" + currentTimeString+"] compare:" + tweetsTime.compareTo(lastTimeString) + " " + tweetsTime.compareTo(currentTimeString));
							sum=sum+count;
						}
					}
					
			}
			//reset value
			currentTimeSlot.count=sum;
		}
	}


	public boolean timeIsRight(Date gameDate, int publishIntervalInMinutes) {
		// always publish - 
		return true;
	}



}
