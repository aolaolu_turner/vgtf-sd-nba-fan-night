package com.turner.nba.tweets.plugin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.jms.Message;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.loki.core.interfaces.InitializableIF;
import com.turner.loki.core.interfaces.MonitorableIF;
import com.turner.loki.core.interfaces.MultiplexIF;
import com.turner.nba.tweets.jpa.TeamTweetDAO;

/**
 * This plugin gets the games within 15 minutes window before and after game, and find the
 * teams to multiplex. Also, it will delete tweets not belonging to those teams.
 *
 * This plugin is not used any more, instead it is replaced by the combination of two plugins
 * GetGamesInWindow - to only get games
 * GetTeamsWithGames - to get game teams 
 * 
 * @deprecated - This class is no longer in use - we now use GetGamesInWindow 
 */
@Deprecated
public class GetTeamsWithGamesIn15MinuteWindow implements MonitorableIF, MultiplexIF, InitializableIF {
	Logger logger = Logger.getLogger(this.getClass());
	
	private static final String URL_PARAM = "team_tweet_url";
	private static final String TEAM_GAME_WINDOW = "game_window_in_minutes";
	
	private int teamGameWindow = 15;
	private String teamJsonUrl = "";
	private Map<String, String> nameMap = new TreeMap<String, String>();
	boolean keepTweets=false;
	public void initialize(Hashtable props) throws InitializationException {
		if(props.containsKey(TEAM_GAME_WINDOW))
			teamGameWindow = Integer.parseInt((String) props.get(TEAM_GAME_WINDOW));
		
		if(props.containsKey(URL_PARAM))
			teamJsonUrl = (String) props.get(URL_PARAM);
		
		if(props.containsKey("keepTweets")){
			keepTweets = Boolean.valueOf((String)props.get("keepTweets"));
		}
		
		nameMap.clear();
		
		for(Object key:props.keySet()){
			
			if(((String)key).startsWith("nameMap")){
				String property = (String) props.get(key);
				String[] pair = property.split("\\|");
				
				if(pair.length >= 2){
					nameMap.put(pair[0], pair[1]);
				}else
					logger.warn("This value isn't well set: "+property);
			}
		}
	}

	public ArrayList<GenericMessage> multiplex(Message m) throws PluginException {
		ArrayList<GenericMessage> al = new ArrayList<GenericMessage>();

		try {
			Date now = new Date();
			List<Game> games = TeamTweetDAO.getInstance().getGamesWithinWindow(teamGameWindow, now);
			logger.info("There are "+games.size()+" games currently taking place");
			
			List<String> names = new ArrayList<String>();
			process(m, al, games, names);
			
			//delete all teams that are NOT IN currently playing name set
			if(!keepTweets){
				TeamTweetDAO.getInstance().deleteTweets(names);
			}
		}catch(Exception e){
			throw new PluginException("Can't process message.", e);
		}
		
		return al;
	}

	/**********************************************************************************************
	 * 
	 * @param m
	 * @param messages
	 * @param games
	 * @param names
	 * @throws Exception
	 */
	public void process(Message m, ArrayList<GenericMessage> messages, List<Game> games,
			List<String> names) throws Exception {
		SimpleDateFormat fd = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		
		for(Game g:games){
			String[] tt = { fixTeamName(g.getVisitorNickname()), fixTeamName(g.getHomeNickname()) };
			
			for(String t:tt){
				Calendar cal = Calendar.getInstance();
				cal.setTime(g.getGameDate());
				cal.add(Calendar.MINUTE, -teamGameWindow);
				
				DateTimeZone timezone = DateTimeZone.forID("America/New_York");
				Date date = new Date(timezone.convertLocalToUTC(cal.getTimeInMillis(), false));
				
				String url = teamJsonUrl.replaceAll("\\{team\\}", t);
				String text = "<team name='"+t+"' url='"+url+"' gamedate='" +fd.format(date) + " +0000' />";
			
				GenericMessage gm = GenericMessageFactory.createGenericMessage(m);
				gm.setStringProperty("body", text);
				messages.add(gm);
				
				names.add("'"+t+"'");
			}
		}
	}
	
	public String fixTeamName(String name){
		String nom = name.replaceAll(" ", "").toLowerCase();
		
		if(nameMap.containsKey(nom))
			return nameMap.get(nom);
		else
			return nom;
	}
	
	//String[] teams = {
	//"celtics", "nets", "knicks", "76ers", "raptors", "bulls", "cavaliers", "pistons", "pacers", "bucks", "hawks", 
	//"bobcats", "heat", "magic", "wizards", "mavericks", "rockets", "grizzlies", "hornets", "spurs", "nuggets", "timberwolves", 
	//"trailblazers", "thunder", "jazz", "warriors", "clippers", "lakers", "suns"
	//};
}
