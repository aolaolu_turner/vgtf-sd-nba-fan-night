package com.turner.nba.tweets.plugin;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import javax.jms.Message;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.loki.core.interfaces.InitializableIF;
import com.turner.loki.core.interfaces.MonitorableIF;
import com.turner.loki.core.interfaces.MultiplexIF;
import com.turner.nba.tweets.jpa.TeamTweet;
import com.turner.nba.tweets.jpa.TeamTweetDAO;
import com.turner.nba.tweets.util.ModifiedTweet;
import com.turner.nba.tweets.util.Tweet;
import com.turner.nba.util.Helper;

public class GetTeamTweets  implements MonitorableIF, MultiplexIF, InitializableIF {
	private static final String ALL_TEAM_TWEET_LIMIT = "*";
	public org.apache.log4j.Logger logger = Logger.getLogger(this.getClass());
	public String classReference = this.getClass().toString();
	
	public SAXBuilder builder = new SAXBuilder();
	
	private static String PATH = "path";
	private String path = "/nba/data/noseason/cms/";
	
	private static String SORT_TWEETS = "sort_tweet";
	private boolean sortTweetsAsc = false;
	
	private static String LIMIT_TWEETS = "limitTweetsForTeam";
	private Map<String, Integer> tweetLimitMap = new TreeMap<String, Integer>();
	
	private Map<String, String> duplicateNameMap = new TreeMap<String, String>();
	private Map<String, String> updateNameMap = new TreeMap<String, String>();
	
	public void initialize(Hashtable props) throws InitializationException {
		if(props.containsKey(PATH)){
			path = (String) props.get(PATH);
			path += "/";
		}
		
		tweetLimitMap.clear();
		if(props.containsKey(LIMIT_TWEETS)){
			String value = (String) props.get(LIMIT_TWEETS);
			
			if(value != null){
				String[] values = value.split(",");
				
				for(String entry: values){
					String[] pair = entry.trim().split("\\|");
					
					if(pair.length > 1 && !(pair[1].trim().equals("") || pair[0].trim().equals("") ))
						try{
							tweetLimitMap.put(pair[0].trim(), Integer.parseInt(pair[1].trim()));
						}catch(Exception e){
							logger.warn("Could not set tweet limit for " + pair[0]+" -> "+pair[1], e);
						}
				}
			}
		}
		
		duplicateNameMap.clear();
		updateNameMap.clear();
		
		for(Object key:props.keySet()){
			
			if(((String)key).startsWith("duplicateNameMap")){
				String property = (String) props.get(key);
				String[] pair = property.split("\\|");
				
				if(pair.length >= 2){
					duplicateNameMap.put(pair[0], pair[1]);
				}else
					logger.warn("This duplicateNameMap value isn't well set: "+property);
			}
			
			if(((String)key).startsWith("updateNameMap")){
				String property = (String) props.get(key);
				String[] pair = property.split("\\|");
				
				if(pair.length >= 2){
					updateNameMap.put(pair[0], pair[1]);
					logger.info("Added "+pair[0]+" -> "+pair[1]+" to updateNameMap");
				}else
					logger.warn("This updateNameMap value isn't well set: "+property);
			}
		}
		
		boolean a = props.containsKey(SORT_TWEETS) && "desc".equals((String)props.get(SORT_TWEETS));
		sortTweetsAsc = !a;
	}

	public ArrayList<GenericMessage> multiplex(Message m) throws PluginException {
		ArrayList<GenericMessage> al = new ArrayList<GenericMessage>();

		GenericMessage gm = null;
		try{
			if (m instanceof GenericMessage) 
				gm = (GenericMessage) m;
			else 
				gm = GenericMessageFactory.createGenericMessage(m);
			
			//logger.info("Using message: "+bodyStr);
			String bodyStr = gm.getStringProperty("body");
			String[] list = getParameters(bodyStr);
			String name = list[0];
			String link = list[1];
			String gamedate = list[2];
			logger.info("Retrieved Team name: "+name+", Team link: "+link+", Started @ "+gamedate);

			int limit = TeamTweetDAO.NO_TWEET_LIMIT;
			if(tweetLimitMap.containsKey(name) || tweetLimitMap.containsKey(ALL_TEAM_TWEET_LIMIT)){
				limit = tweetLimitMap.containsKey(name) ? tweetLimitMap.get(name) : tweetLimitMap.get(ALL_TEAM_TWEET_LIMIT);
			}
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			Comparator<ModifiedTweet> comparator = new ModifiedTweet.CreationTimeComparator(sortTweetsAsc);
			
			//get tweets stored in the database
			List<ModifiedTweet> massRelevanceTweets = new ArrayList<ModifiedTweet>();
			//List<TeamTweet> data = TeamTweetDAO.getInstance().getTweets(name);
			List<TeamTweet> data = TeamTweetDAO.getInstance().getTweets(name, limit);
			for(TeamTweet datum:data)
				massRelevanceTweets.add(gson.fromJson(datum.getTweetJson(), ModifiedTweet.class));
			logger.info("Added " + massRelevanceTweets.size() + " tweets found in DB to list");
			
			//get new tweets from mass relevance
			Collections.sort(massRelevanceTweets, comparator);
			long lastID = massRelevanceTweets.isEmpty() ? 0L : ( sortTweetsAsc ? massRelevanceTweets.get(massRelevanceTweets.size() - 1).getId() : massRelevanceTweets.get(0).getId() );
			java.lang.reflect.Type tvType = new TypeToken<java.util.ArrayList<Tweet>>() {}.getType();
			List<Tweet> tweets = gson.fromJson(getStream(link, lastID, false), tvType);
			logger.debug("Generated "+tweets.size()+" tweet objects using gson");
			
			//convert new tweets to modified objects
			List<ModifiedTweet> newModifiedTweets = new ArrayList<ModifiedTweet>();
			List<TeamTweet> tweets4db = new ArrayList<TeamTweet>();
			for(Tweet tweet:tweets){
				ModifiedTweet tt = new ModifiedTweet(tweet);
			
				//if there were no tweets in the database filter out and use only tweets created after game start time
				if(lastID == 0L && tt.getCreated_at().compareTo(gamedate) < 0){
					logger.info("Skipping tweet created at "+tt.getCreated_at() + ". Game starts at " + gamedate);
				}else{
					newModifiedTweets.add(tt);
					
					TeamTweet teamTweet = new TeamTweet();
					teamTweet.setTeam(name);
					teamTweet.setTweetJson(gson.toJson(tt, ModifiedTweet.class));
					teamTweet.setCollectionTime(new Date());
					tweets4db.add(teamTweet);
				}
				//foundErrors = checkForErrors(tt, link, foundErrors, gson);	//used to debug for specific issues in generated tweets
			}
			logger.debug("Modified tweet objects: ready to convert back to json");
			
			//convert all viable modified tweets back to json - allowing no duplicates
			massRelevanceTweets.addAll(newModifiedTweets);
			Set<ModifiedTweet> set = new TreeSet<ModifiedTweet>(new ModifiedTweet.TweetIdComparator(true));
			set.addAll(massRelevanceTweets);
			massRelevanceTweets.clear();
			massRelevanceTweets.addAll(set);
			Collections.sort(massRelevanceTweets, comparator);
			
			if(limit != TeamTweetDAO.NO_TWEET_LIMIT && limit < massRelevanceTweets.size()){
				logger.info("Returning only latest "+limit+" tweets for " + name);
				massRelevanceTweets = massRelevanceTweets.subList(0, limit);
			}
			
			java.lang.reflect.Type ttType = new TypeToken<java.util.ArrayList<ModifiedTweet>>() {}.getType();
			String json = gson.toJson(massRelevanceTweets, ttType);
			logger.debug("Generated modified tweet json using gson");
			
			//writeJsonToDisk(link, json);
			try{ 
					TeamTweetDAO.getInstance().saveTweets(tweets4db);
					logger.info("Saved "+tweets4db.size()+" items to the database");
			}catch(Exception e){
					logger.error("Could not save "+tweets4db.size()+" new tweets to "+name+" database", e);
			}
				
			for(String key: updateNameMap.keySet()){
				if(name.contains(key))
					name = name.replace(key, updateNameMap.get(key));
			}
			
			gm.setStringProperty("body", massRelevanceTweets.isEmpty()? "[]" : json);		//TODO hack until I can verify what gson produces for empty list
			gm.setStringProperty("filename", path+name+"/twitter.json");
			gm.setStringProperty("absoluteDestinationFileNames", path+name+"/twitter.json");
			al.add(gm);
			
			logger.info("sending "+path+name+"/twitter.json to LMS");

			if(duplicateNameMap.containsKey(name)){
				gm = GenericMessageFactory.createGenericMessage(m);
				
				name = duplicateNameMap.get(name) ;
				
				gm.setStringProperty("body", massRelevanceTweets.isEmpty()? "[]" : json);		//TODO hack until I can verify what gson produces for empty list
				gm.setStringProperty("filename", path+name+"/twitter.json");
				gm.setStringProperty("absoluteDestinationFileNames", path+name+"/twitter.json");
				
				logger.info("sending "+path+name+"/twitter.json to LMS");
				al.add(gm);
			}
			
			logger.info("Done processing "+link);
		}catch(Exception e){
			logger.error("Process failed: "+e.getMessage(), e);
			throw new PluginException("Can't process message.", e);
		}
		
		return al;
	}

	public String[] getParameters(String bodyStr) throws Exception {
		
		Document doc = Helper.convertStringToDocument(bodyStr);	
		XPath teamXP = XPath.newInstance("//team");
		Element teamElement = (Element)teamXP.selectSingleNode(doc);
		String name = (String) teamElement.getAttributeValue("name");
		String link = (String) teamElement.getAttributeValue("url");
		String date = (String) teamElement.getAttributeValue("gamedate");
		return new String[]{ name, link, date };
	}
	
	//used for testing... ignore
	private boolean checkForErrors(ModifiedTweet tt, String link, Boolean foundError, Gson gson) throws IOException {
		Boolean f = false;
		if(tt.getText().contains("http://t.co") && tt.getEntities().getUrls().isEmpty() && !foundError)
			f = true;
		
		String filename = "data/" + link.split("/")[4]+"."+tt.getId();
		BufferedWriter out = new BufferedWriter(new FileWriter(filename));
		out.write(gson.toJson(tt, ModifiedTweet.class));
		out.close();

		return f;
	}

	//used for testing... ignore
	private void writeJsonToDisk(String link, String json) throws IOException {
		String filename = "data/" + link.split("/")[4];
		BufferedWriter out = new BufferedWriter(new FileWriter(filename));
		out.write(json);
		out.close();
	}
	
	public Reader getStream(String link, long lastID, Boolean showText) throws Exception {
		logger.info("Processing tweets: "+link+"?since_id="+lastID);
		URL url = new URL(link+"?since_id="+lastID);
		URLConnection conn = url.openConnection();
		conn.setConnectTimeout(5000);		//5 seconds to connect to server
		conn.setReadTimeout(5000);			//another 5 seconds to read stuff from the server
		
		BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
		
		if(!showText)
			return in;
		
		//******************here for testing***************************************************
		StringBuilder sb = new StringBuilder();
		String line = null;
		while((line = in.readLine()) != null)
			sb.append(line);
		in.close();
		
		return new StringReader(sb.toString());
	}
}
