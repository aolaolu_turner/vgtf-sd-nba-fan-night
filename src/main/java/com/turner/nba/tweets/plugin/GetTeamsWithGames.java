package com.turner.nba.tweets.plugin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;

import javax.jms.Message;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.loki.core.interfaces.InitializableIF;
import com.turner.loki.core.interfaces.MonitorableIF;
import com.turner.loki.core.interfaces.MultiplexIF;
import com.turner.nba.tweets.jpa.TeamTweetDAO;

/**
 * This Plugin multiplex the teams from the games, which are from the incoming message.
 * Also it will delete the tweets not belonging to the teams.
 * 
 * This class is copied from original GetTeamsWithGamesIn15Minutes plugin.
 *
 */
public class GetTeamsWithGames implements MonitorableIF, MultiplexIF, InitializableIF {
	Logger logger = Logger.getLogger(this.getClass());
	
	private static final String URL_PARAM = "team_tweet_url";
	private static final String TEAM_GAME_WINDOW = "game_window_in_minutes";
	
	private static final String EXCLUDED_TEAM_NAMES = "excluded_team_names";
	private static final String SUMMERLEAGUE_FEED_NAME = "summerleague.feed_name";
	private static final String ALL_STAR_TEAM_NAME = "all_star.team_name";
	private static final String ALL_STAR_FEED_NAME = "all_star.feed_name";
	
	private static final String TEAM_FAN_SUFFIX = "team_fan_suffix";
	private String teamFanSuffix = null;
	
	private String allStarTeamName = null;
	private String allStarFeedName = null;
	private String summerleagueFeedName = null;
	private String[] excludedTeamNames = {};
	
	private boolean processAllStarFeed = false;
	
	private int teamGameWindow = 15;
	private String teamJsonUrl = "";
	private Map<String, String> nameMap = new TreeMap<String, String>();
	boolean keepTweets=false;
	
	public void initialize(Hashtable props) throws InitializationException {
		if(props.containsKey(TEAM_FAN_SUFFIX))
			teamFanSuffix = (String) props.get(TEAM_FAN_SUFFIX);
		
		if(props.containsKey(TEAM_GAME_WINDOW))
			teamGameWindow = Integer.parseInt((String) props.get(TEAM_GAME_WINDOW));
		
		if(props.containsKey(URL_PARAM))
			teamJsonUrl = (String) props.get(URL_PARAM);
		
		if(props.containsKey("keepTweets")){
			keepTweets = Boolean.valueOf((String)props.get("keepTweets"));
		}
		
		if(props.containsKey(SUMMERLEAGUE_FEED_NAME))
			summerleagueFeedName = (String) props.get(SUMMERLEAGUE_FEED_NAME);
		
		if(props.containsKey(ALL_STAR_TEAM_NAME))
			allStarTeamName = (String) props.get(ALL_STAR_TEAM_NAME);
		
		if(props.containsKey(ALL_STAR_FEED_NAME))
			allStarFeedName = (String) props.get(ALL_STAR_FEED_NAME);
		
		processAllStarFeed = (allStarTeamName != null && !allStarTeamName.equals(""));
		
		if(props.containsKey(EXCLUDED_TEAM_NAMES)){
			String names = (String) props.get(EXCLUDED_TEAM_NAMES);
			if(names != null){
				excludedTeamNames = names.split(",");
			}
		}
		
		nameMap.clear();
		
		for(Object key:props.keySet()){
			
			if(((String)key).startsWith("nameMap")){
				String property = (String) props.get(key);
				String[] pair = property.split("\\|");
				
				if(pair.length >= 2){
					nameMap.put(pair[0], pair[1]);
				}else
					logger.warn("This value isn't well set: "+property);
			}
		}
	}

	public ArrayList<GenericMessage> multiplex(Message m) throws PluginException {
		ArrayList<GenericMessage> al = new ArrayList<GenericMessage>();

		try {
			GenericMessage gm = GenericMessageFactory.createGenericMessage(m);
			//List<Game> games = TeamTweetDAO.getInstance().getGamesWithinWindow(teamGameWindow);
			List<Game> games = (List<Game>)gm.getObjectProperty("games");
			logger.info("There are "+games.size()+" games currently taking place");
			
			List<String> names = new ArrayList<String>();
			process(m, al, games, names);
			
			//delete all teams that are NOT IN currently playing name set
			if(!keepTweets){
				TeamTweetDAO.getInstance().deleteTweets(names);
			}
		}catch(Exception e){
			throw new PluginException("Can't process message.", e);
		}
		
		return al;
	}

	/**********************************************************************************************
	 * 
	 * @param m
	 * @param messages
	 * @param games
	 * @param names
	 * @throws Exception
	 */
	public void process(Message m, ArrayList<GenericMessage> messages, List<Game> games,
			List<String> names) throws Exception {
		SimpleDateFormat fd = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		
		for(Game g:games){
			//we have two different feeds that this handles: http://tweetriver.com/nbadproducts/bobcats-fans.xml and http://tweetriver.com/nbadproducts/bobcats.xml
			List<String> teamNames = new ArrayList<String>();
			
			String visitorName = fixTeamName(g.getVisitorNickname());
			teamNames.add(visitorName);
			String homeName = fixTeamName(g.getHomeNickname()); 
			teamNames.add(homeName);
			
			if(!(teamFanSuffix == null || teamFanSuffix.equals(""))){
				teamNames.add(visitorName + teamFanSuffix);
				teamNames.add(homeName + teamFanSuffix);
			}
			
			boolean hasAllstarFeed = false;

			teamloop:
			for(String teamName:teamNames){
				
				for(String excluded: excludedTeamNames)
					if(excluded.equals(teamName)){
						logger.info("Skipping team: " + excluded.toUpperCase() + " based on workflow configuration");
						continue teamloop;
					}
					
				Calendar cal = Calendar.getInstance();
				cal.setTime(g.getGameDate());
				cal.add(Calendar.MINUTE, -teamGameWindow);
				
				DateTimeZone timezone = DateTimeZone.forID("America/New_York");
				Date date = new Date(timezone.convertLocalToUTC(cal.getTimeInMillis(), false));
				
				//check if this is an all star game or not
				String inputFeedName = 
						(processAllStarFeed && g.getGameId().charAt(2) == '3') ? allStarTeamName : 
						(g.getGameId().startsWith("00")) ? teamName :
						summerleagueFeedName;		//e.g. 0031200002 or 0031200001
				
				String url = teamJsonUrl.replaceAll("\\{team\\}", inputFeedName);
				String text = "<team name='"+teamName+"' url='"+url+"' gamedate='" +fd.format(date) + " +0000' />";
				
				GenericMessage gm = GenericMessageFactory.createGenericMessage(m);
				//object property can not be passed to queue, remove it now
				gm.removeProperty("games");
				gm.setStringProperty("body", text);
				messages.add(gm);
				
				names.add("'"+teamName+"'");
				
				if(inputFeedName.equals(allStarTeamName) && !hasAllstarFeed){
					hasAllstarFeed = true;	//this would introduce 2 all star games per game - to avoid having double allstar feed
					
					GenericMessage msg = GenericMessageFactory.createGenericMessage(m);
					//object property can not be passed to queue, remove it now
					msg.removeProperty("games");
					msg.setStringProperty("body", "<team name='"+inputFeedName+"' url='"+url+"' gamedate='" +fd.format(date) + " +0000' />");
					messages.add(msg);
					
					names.add("'"+inputFeedName+"'");
				}
			}
		}
	}
	
	public String fixTeamName(String name){
		String nom = name.replaceAll(" ", "").toLowerCase();
		
		if(nameMap.containsKey(nom))
			return nameMap.get(nom);
		else
			return nom;
	}
	
	//String[] teams = {
	//"celtics", "nets", "knicks", "76ers", "raptors", "bulls", "cavaliers", "pistons", "pacers", "bucks", "hawks", 
	//"bobcats", "heat", "magic", "wizards", "mavericks", "rockets", "grizzlies", "hornets", "spurs", "nuggets", "timberwolves", 
	//"trailblazers", "thunder", "jazz", "warriors", "clippers", "lakers", "suns"
	//};
}
