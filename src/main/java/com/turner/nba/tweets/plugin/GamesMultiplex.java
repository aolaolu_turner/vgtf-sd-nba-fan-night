package com.turner.nba.tweets.plugin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.jms.Message;

import org.apache.log4j.Logger;

//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.loki.core.interfaces.InitializableIF;
import com.turner.loki.core.interfaces.MonitorableIF;
import com.turner.loki.core.interfaces.MultiplexIF;
import com.turner.nba.tweets.jpa.TeamTweetDAO;

/**
 * This plugin simply multiplex games
 *
 */
public class GamesMultiplex implements MonitorableIF, MultiplexIF, InitializableIF {
	public Logger logger = Logger.getLogger(this.getClass());
	public static SimpleDateFormat fd = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	@Override
	public void initialize(Hashtable props) throws InitializationException {
		
		
	}

	@Override
	public ArrayList multiplex(Message m) throws PluginException {
		ArrayList<GenericMessage> al = new ArrayList<GenericMessage>();

		try {
			List<Game> games = (List<Game>)(GenericMessageFactory.createGenericMessage(m).getObjectProperty("games"));
			
			logger.info("There are "+games.size()+" games currently taking place");
			if(!games.isEmpty()){
				for(Game g:games){
					String gameId = g.getGameId();
					String visitor = g.getVisitorNickname();
					String home = g.getHomeNickname();
					String gameDate = fd.format(g.getGameDate());
					GenericMessage gm = new GenericMessage();
					//give you both XML and message properties
					String text = "<game gameId='"+gameId+"' visitor='"+visitor+"' home='"+home+"' gameDate='"+gameDate+"' />";
					gm.setStringProperty("gameId", gameId);
					gm.setStringProperty("visitor", visitor);
					gm.setStringProperty("home", home);
					gm.setStringProperty("gameDate", gameDate);
					gm.setStringProperty("body", text);
					al.add(gm);
				}
			}
		}catch(Exception e){
			throw new PluginException("Can't process message.", e);
		}
		
		return al;
	}

}
