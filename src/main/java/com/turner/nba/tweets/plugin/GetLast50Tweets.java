package com.turner.nba.tweets.plugin;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

import javax.jms.Message;

import org.apache.log4j.Logger;
import org.jdom.input.SAXBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.loki.core.interfaces.InitializableIF;
import com.turner.loki.core.interfaces.MonitorableIF;
import com.turner.loki.core.interfaces.MultiplexIF;
import com.turner.nba.tweets.util.ModifiedTweet;

public class GetLast50Tweets  implements MonitorableIF, MultiplexIF, InitializableIF {
	public org.apache.log4j.Logger logger = Logger.getLogger(this.getClass());
	public String classReference = this.getClass().toString();
	
	public SAXBuilder builder = new SAXBuilder();
	
	private static String PATH = "path";
	private String path = "/nba/data/noseason/cms/nba/";
	
	public void initialize(Hashtable props) throws InitializationException {
		if(props.containsKey(PATH)){
			path = (String) props.get(PATH);
			path += "/";
		}		
	}

	public ArrayList<GenericMessage> multiplex(Message m) throws PluginException {
		ArrayList<GenericMessage> al = new ArrayList<GenericMessage>();

		GenericMessage gm = null;
		try{
			if (m instanceof GenericMessage) 
				gm = (GenericMessage) m;
			else 
				gm = GenericMessageFactory.createGenericMessage(m);
			
			String name = gm.getStringProperty("name");
			String tweets = gm.getStringProperty("body");
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			
			//get tweets from json
			java.lang.reflect.Type tvType = new TypeToken<java.util.ArrayList<ModifiedTweet>>() {}.getType();
			List<ModifiedTweet> massRelevanceTweets = gson.fromJson(tweets, tvType);
			logger.debug("Found " + massRelevanceTweets.size() + " tweet objects using gson");
			//retrieve the last 50
			massRelevanceTweets = massRelevanceTweets.subList(0, 50);
			//convert tweets back to json
			java.lang.reflect.Type ttType = new TypeToken<java.util.ArrayList<ModifiedTweet>>() {}.getType();
			String json = gson.toJson(massRelevanceTweets, ttType);
			//put tweet json back into message
			gm.setStringProperty("body", massRelevanceTweets.isEmpty()? "[]" : json);		//TODO hack until I can verify what gson produces for empty list
			gm.setStringProperty("filename", path+name+"/twitter.json");
			gm.setStringProperty("absoluteDestinationFileNames", path+name+"/twitter.json");
			al.add(gm);
			
			logger.info("sending "+path+name+"/twitter.json to LMS");
			logger.info("Done processing last " + massRelevanceTweets.size() + " tweets for "+ name);
		}catch(Exception e){
			logger.error("Process failed: "+e.getMessage(), e);
			throw new PluginException("Can't process message.", e);
		}
		
		return al;
	}
}
