package com.turner.nba.tweets.plugin;

import java.io.Reader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import javax.jms.JMSException;
import javax.jms.Message;

import org.apache.log4j.Logger;
import org.jdom.input.SAXBuilder;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.loki.core.interfaces.InitializableIF;
import com.turner.loki.core.interfaces.MonitorableIF;
import com.turner.loki.core.interfaces.MultiplexIF;
import com.turner.nba.tweets.jpa.TeamLast50Tweet;
import com.turner.nba.tweets.jpa.TeamTweetDAO;
import com.turner.nba.tweets.util.ModifiedTweet;
import com.turner.nba.tweets.util.Tweet;

public class GetAllStarLast50Tweets  implements MonitorableIF, MultiplexIF, InitializableIF {
	
	public org.apache.log4j.Logger logger = Logger.getLogger(this.getClass());
	public String classReference = this.getClass().toString();
	
	public SAXBuilder builder = new SAXBuilder();
	
	protected static String PATH = "path";
	private String path = "/nba/data/noseason/cms/nba/";
	
	protected static String START_DATE = "start_date";
	private String startDate = null;
	
	protected static String END_DATE = "end_date";
	private String endDate = null;
	
	protected static String TWEET_URL = "tweet_url";
	private String tweetURL = null;
	
	protected static String FEED_NAME = "feed_name";
	private String feedName = "allstar";
	
	protected static String FINAL_FEED_NAME = "final_feed_name";
	private String finalFeedName = null;
	
	protected static String SORT_TWEETS = "sort_tweet";
	private boolean sortTweetsAsc = false;
	
	protected static String COUNT = "count";
	private int count = 50;
	
	protected static final String KEEP_TWEETS = "keep_tweets";
	private boolean keepTweets=false;
	
	private DateFormat df = new SimpleDateFormat("yyyyMMdd HH:mm:ss +0000");
	
	public void initialize(Hashtable props) throws InitializationException {
		if(props.containsKey(PATH)){
			path = (String) props.get(PATH);
			path += "/";
		}
		
		if(props.containsKey(START_DATE))
			try{
				startDate = (String)props.get(START_DATE);
			}catch(Exception e){
				logger.warn("Could not set the START DATE for pulling rolling window all star tweets", e);
			}
		
		if(props.containsKey(END_DATE))
			try{
				endDate = (String)props.get(END_DATE);
			}catch(Exception e){
				logger.warn("Could not set the END DATE for pulling rolling window all star tweets", e);
			}
		
		if(props.containsKey(TWEET_URL))
			tweetURL = (String) props.get(TWEET_URL);
		
		if(tweetURL == null || tweetURL.trim().equals(""))
			throw new InitializationException("''tweet_url' param is missing. I don't know where to pull the all star tweets from!");
		
		if(props.containsKey(FEED_NAME))
			feedName = (String) props.get(FEED_NAME);
		
		if(props.containsKey(FINAL_FEED_NAME))
			finalFeedName = (String) props.get(FINAL_FEED_NAME);
		
		if( finalFeedName == null || finalFeedName.trim().equals(""))
			throw new InitializationException("''final_feed_name' param is missing. I don't know what the final file name is!");
			
		if(props.containsKey(COUNT))
			try{
				count = Integer.parseInt((String) props.get(COUNT));
			}catch(Exception e){
				logger.warn("Count was not properly set. Will process using default last 50 tweets");
			}
		
		boolean a = props.containsKey(SORT_TWEETS) && "desc".equals((String)props.get(SORT_TWEETS));
		sortTweetsAsc = !a;
		
		if(props.containsKey("keep_tweets")){
			keepTweets = Boolean.valueOf((String)props.get("keep_tweets"));
		}
	}
	
	public ArrayList<GenericMessage> multiplex(Message m) throws PluginException {
		ArrayList<GenericMessage> al = new ArrayList<GenericMessage>();

		GenericMessage gm = null;
		try{
			if (m instanceof GenericMessage) 
				gm = (GenericMessage) m;
			else 
				gm = GenericMessageFactory.createGenericMessage(m);
			
			String date = df.format(new Date());
			
			if(startDate != null && endDate != null && date.compareTo(startDate) >= 0 && date.compareTo(endDate) <= 0){
				processTweets(gm);
				al.add(gm);
			}else
				if(!keepTweets)
					TeamTweetDAO.getInstance().deleteLast50Tweets(feedName);		//deleteTweets();
		}catch(Exception e){
			logger.error("Process failed: "+e.getMessage(), e);
			throw new PluginException("Can't process message.", e);
		}
		
		return al;
	}
	
	public void processTweets(GenericMessage gm) throws Exception {
		Gson gson = new GsonBuilder().create();
		
		//get tweets stored in the database as modified objects
		List<ModifiedTweet> massRelevanceTweets = getModifiedTweetsFromDatabase(gson, false);
		
		//get new tweets from mass relevance
		long lastID = massRelevanceTweets.isEmpty() ? 0L : 
						(sortTweetsAsc ? 
							massRelevanceTweets.get(massRelevanceTweets.size() - 1).getId() : 
							massRelevanceTweets.get(0).getId() 
						);
		List<Tweet> tweets = getTweetsFromMassRelevance(gson, lastID);
		
		//convert new MR tweets to modified objects
		List<ModifiedTweet> newModifiedTweets = new ArrayList<ModifiedTweet>();
		for(Tweet tweet:tweets){
			ModifiedTweet tt = new ModifiedTweet(tweet);
			
			if(lastID == 0L && tt.getCreated_at().compareTo(startDate) < 0)
				logger.debug("Skipping tweet created at "+tt.getCreated_at() + ". We start processing tweets from " + startDate);
			else
				newModifiedTweets.add(tt);
		}
		logger.debug("Modified tweet objects: ready to convert back to json");
			
		//convert all viable modified tweets back to json - allowing no duplicates
		if(newModifiedTweets.size() > count){
			massRelevanceTweets = newModifiedTweets;
		}else{
			massRelevanceTweets.addAll(newModifiedTweets);
			Set<ModifiedTweet> set = new TreeSet<ModifiedTweet>(new ModifiedTweet.TweetIdComparator(true));
			set.addAll(massRelevanceTweets);
			massRelevanceTweets.clear();
			massRelevanceTweets.addAll(set);
		}
		//sort by date in order specified in init param
		Comparator<ModifiedTweet> comparator = new ModifiedTweet.CreationTimeComparator(sortTweetsAsc);
		Collections.sort(massRelevanceTweets, comparator);
		//pick last 50 tweets
		int size = massRelevanceTweets.size();
		int maxSize = size > count ? count : size;
		massRelevanceTweets = sortTweetsAsc ? massRelevanceTweets.subList(size - count, size) : massRelevanceTweets.subList(0, maxSize);
		
		//save last 50 tweets to the database
		List<ModifiedTweet> tweetts = keepTweets ? newModifiedTweets : massRelevanceTweets;	//avoid writing duplicate tweets to db
		saveModifiedTweetsToDatabase(gson, tweetts);
		
		//create message to send out
		java.lang.reflect.Type ttType = new TypeToken<java.util.ArrayList<ModifiedTweet>>() {}.getType();
		String json = gson.toJson(massRelevanceTweets, ttType);
		logger.debug("Generated modified allstar tweet json using gson");
		
		gm.setStringProperty("body", massRelevanceTweets.isEmpty()? "[]" : json);	

		gm.setStringProperty("filename", path+feedName+ "/" + finalFeedName );
		gm.setStringProperty("name", feedName);
		gm.setStringProperty("absoluteDestinationFileNames", path+feedName+ "/" + finalFeedName );
		
		logger.info("sending "+path+feedName+ "/" + finalFeedName + " to LMS");
		logger.info("Done processing last " + massRelevanceTweets.size() + " tweets for "+ feedName);
	}

	protected void saveModifiedTweetsToDatabase(Gson gson, List<ModifiedTweet> tweetts) {
		List<TeamLast50Tweet> tweets4db = new ArrayList<TeamLast50Tweet>();
		
		try{ 
			for(ModifiedTweet tt: tweetts){
				TeamLast50Tweet teamTweet = new TeamLast50Tweet();
				teamTweet.setTeam(feedName);
				teamTweet.setTweetJson(gson.toJson(tt, ModifiedTweet.class));
				teamTweet.setCollectionTime(new Date());
				teamTweet.setCreatedAt(tt.getCreated_at());
				tweets4db.add(teamTweet);
			}
		
			if(!keepTweets)
				TeamTweetDAO.getInstance().deleteLast50Tweets(feedName);
			TeamTweetDAO.getInstance().saveLast50Tweets(tweets4db);
			
			logger.debug("Cleared and then saved "+tweets4db.size()+" items to the all-star last 50 table");
		}catch(Exception e){
			logger.error("Could not save last "+tweetts.size()+" tweets to database", e);
		}
	}

	protected List<Tweet> getTweetsFromMassRelevance(Gson gson, long lastID)
			throws Exception {
		java.lang.reflect.Type tvType = new TypeToken<java.util.ArrayList<Tweet>>() {}.getType();
		GetTeamTweets gtt = new GetTeamTweets();
		Reader reader = gtt.getStream(tweetURL, lastID, false);
		List<Tweet> tweets = gson.fromJson(reader, tvType);
		logger.debug("Retrieved "+tweets.size()+" new "+feedName+" tweet objects from mass relevance");
		return tweets;
	}

	protected List<ModifiedTweet> getModifiedTweetsFromDatabase(Gson gson, boolean ignoreCount) {
		List<ModifiedTweet> massRelevanceTweets = new ArrayList<ModifiedTweet>();
		
		List<TeamLast50Tweet> data = TeamTweetDAO.getInstance().getLast50Tweets(feedName, sortTweetsAsc);
		if(!ignoreCount){
			int size = data.size();
			int maxSize = size > count ? count : size;
			data = sortTweetsAsc ? data.subList(size - count, size) : data.subList(0, maxSize);
		}
		
		for(TeamLast50Tweet datum:data){
			try{
				massRelevanceTweets.add(gson.fromJson(datum.getTweetJson(), ModifiedTweet.class));
			}catch(Exception e){
				logger.warn("Failed to process a tweet["+datum.getId()+"]", e);
			}
		}
		logger.debug("Found " + massRelevanceTweets.size() + " tweets found in DB");
		
		return massRelevanceTweets;
	}
}
