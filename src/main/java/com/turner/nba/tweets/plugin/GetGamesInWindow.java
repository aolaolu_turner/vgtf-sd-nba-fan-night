package com.turner.nba.tweets.plugin;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.jms.Message;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.loki.AbstractPlugin;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.nba.tweets.jpa.TeamTweetDAO;
import com.turner.nba.tweets.util.HomeAwayTweetConfig;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;

/**
 * This plugin find the games within a defined window
 *
 */
public class GetGamesInWindow extends AbstractPlugin{
	public Logger logger = Logger.getLogger(this.getClass());
	
	private int teamGameWindow = 15;
	private String homeAwayFeedConfig = "";
	
	public static String SYNC_SUFFIX = ":SYNC";
	
	private static final String TEAM_GAME_WINDOW = "game_window_in_minutes";
	private static final String HOME_AWAY_FEED_CFG = "home_away_feed_cfg";
	
	public static SimpleDateFormat fd = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
	
	@Override
	public void initialize(Hashtable props) throws InitializationException {
		if(props.containsKey(TEAM_GAME_WINDOW))
			teamGameWindow = Integer.parseInt((String) props.get(TEAM_GAME_WINDOW));
		/** Commenting out this section as we will get the homeAwayGames from TNT_OT Indicator
		try{
			homeAwayFeedConfig = props.containsKey(HOME_AWAY_FEED_CFG) ? (String)props.get(HOME_AWAY_FEED_CFG) : "";
		}catch(Exception e){
			logger.warn("Could not initialize " + HOME_AWAY_FEED_CFG + "flag. The homeAwayFeeds will NOT be generated");
		}
		*/
	}
	
	@Override
	public Object process(Message m) throws PluginException {
		GenericMessage gm = null;
		try{
			if (m instanceof GenericMessage) {
				gm = (GenericMessage) m;
			}else{ 
				gm = GenericMessageFactory.createGenericMessage(m);
			}
			Date now = new Date(); //current system time
			if(gm.getStringProperty("now")!=null){
				//testing purpose
				now = this.fd.parse((String)gm.getStringProperty("now"));
			}
			logger.debug("Checking games between " + teamGameWindow + " of " + now);
			List<Game> games = TeamTweetDAO.getInstance().getGamesWithinWindow(teamGameWindow, now);
			logger.info("There are "+games.size()+" games currently taking place");
			
			try{
					//addHomeAwayGamesWithGson(games);
				//Created new mothod for adding homeAwayGames from TNT_OT
				addHomeAwayGamesTNTOT(games);
			}catch(Exception e){
				logger.warn("Process' attempt to add home and away games for social sync failed.", e);
			}
			
			//do not multiplex...this games list will be used later.
			gm.setObjectProperty("games", games);
		}catch (Exception e) {
			logger.error("ERROR", e);
			throw new PluginException(e);
		}
		return gm;
	}
	//this adds 'one' extra game if the TNT_OT_INT is set to true 
	public void addHomeAwayGamesTNTOT(List<Game> games) throws Exception {
			logger.debug("in addHomeAwayGames method");
	
			List<Game> homeAwayList = new ArrayList<Game>();
			//go through the games object and search for TNT_OT_IND
			
			
					logger.info("******************* Searching for TNT OT  ");
					for(Game game: games){
						try {
						  if (game.getTntOtInd().toString().equals("Y"))
						  {
							homeAwayList.add(getHomeAwayGame(game));
						  }
						}catch(Exception err)
						{
							logger.info("Game/games do not have TNT_OT_IND set to 'Y' ", err);
						}
						}
				//add any clones to the original list of games passed in
			logger.info("Created  " + homeAwayList.size() + " new game objects for home/away");
			games.addAll(homeAwayList);
	
	
}

	//this adds 'one' extra game iff there's a game matching one of the expected social sync ids in the passed in list 
		public void addHomeAwayGamesWithGson(List<Game> games) throws Exception {
				logger.debug("in addHomeAwayGames method");
				
				if(homeAwayFeedConfig == null || homeAwayFeedConfig.equals("") || games.isEmpty())
					return;
				
				logger.info("loooking for social sync's home-away games");
				//pull the contents of the config file
				URL url = new URL(homeAwayFeedConfig);
				BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
				
				//parse the contents and get the game ids
				Gson gson = new Gson();
				java.lang.reflect.Type tvType = new TypeToken<HomeAwayTweetConfig>() {}.getType();
				HomeAwayTweetConfig cfg = gson.fromJson(in, tvType);
								
				List<String> ids = cfg.getEnabledGameIDs();
				//ids.add("0021200969");		//FIXME remove this when not testing.
				
				List<Game> homeAwayList = new ArrayList<Game>();
					
				//go through the games object and create clones for any ids
				for(String id:ids){
						logger.info("****************************Searching for "+id);
						for(Game game: games){
							if(id.trim().equals(game.getGameId())){
								homeAwayList.add(getHomeAwayGame(game));
							}
						}
				}

				//add any clones to the original list of games passed in
				logger.info("Created " + homeAwayList.size() + " new game objects for home/away");
				games.addAll(homeAwayList);
		}
		
	//this adds 'one' extra game iff there's a game matching one of the expected social sync ids in the passed in list 
	public void addHomeAwayGamesWithRegex(List<Game> games) throws Exception {
			logger.info("in addHomeAwayGames method");
			
			if(homeAwayFeedConfig == null || homeAwayFeedConfig.equals(""))
				return;
			
			logger.info("loooking for social sync's home-away games");
			//pull the contents of the config file
			URL config = new URL(homeAwayFeedConfig);
			BufferedReader in = new BufferedReader(new InputStreamReader(config.openStream()));
			
			StringBuilder builder = new StringBuilder();
			String temp = "";
			while((temp = in.readLine()) != null)
				builder.append(temp);
			String contents = builder.toString() ;
			
			//parse the contents and get the game ids
			Pattern pattern = Pattern.compile(".+gameSync.+nobrand.+enabledGameIDsFix.+\\[([^\\]]+).+");
			Matcher matcher = pattern.matcher(contents);
			
			if(matcher.matches()){
				String match = matcher.group(1);
				logger.info("**********************ids: "+match);
				String[] ids_ = match.replaceAll("\"", "").split(",");
				
				//FIXME: this is only for testing. Revert ids_ back to ids for production
				List<String> ids = new ArrayList<String>();
				for(String id:ids_){
					ids.add(id);
				}
				ids.add("0021200969");
				
				List<Game> homeAwayList = new ArrayList<Game>();
				
				//go through the games object and create clones for any ids
				for(String id:ids){
					logger.info("****************************Searching for "+id);
					for(Game game: games){
						if(id.trim().equals(game.getGameId())){
							homeAwayList.add(getHomeAwayGame(game));
						}
					}
				}

				//add any clones to the original list of games passed in
				logger.info("Created " + homeAwayList.size() + " new game objects for home/away");
				games.addAll(homeAwayList);
			}else{
				logger.info("******************NO MATCH FOUND");
			}
	}
	
	//creates a new game instance with the relevant fields. The homeName/vstrName fields are called home/away resp
	public Game getHomeAwayGame(Game game){
		Game newGame = new Game();
		
		//we set the gameId to so that CountGameTweets generates output as social_volume_sync.json instead of social_volume.json
		newGame.setGameId (game.getGameId()+SYNC_SUFFIX);				
		newGame.setGameUrl (game.getGameUrl());
		newGame.setGameDate (game.getGameDate());
	    
	    newGame.setVisitorId (game.getVisitorId());
	    newGame.setVisitorName (game.getVisitorName());
	    newGame.setVisitorNickname ("away");
	    newGame.setVisitorUrlName (game.getVisitorUrlName());

	    newGame.setHomeId (game.getHomeId());
	    newGame.setHomeName (game.getHomeName());
	    newGame.setHomeNickname ("home");
	    newGame.setHomeUrlName (game.getHomeUrlName());
	    newGame.setHomeAbrv (game.getHomeAbrv());

		newGame.setGameEndDate(game.getGameEndDate());

		newGame.setVisitorGameDate(game.getVisitorGameDate());
		newGame.setHomeGameDate(game.getHomeGameDate());
		newGame.setArenaGameDate(game.getArenaGameDate());
		
		return newGame;
	}
}
