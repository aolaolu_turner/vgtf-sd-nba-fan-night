package com.turner.nba.tweets.jpa;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Transient;

import com.turner.nba.tweets.util.ModifiedTweet;

@Entity
public class TeamTweet {
	private Long id;
	private String team;
	private String tweetJson;
	private ModifiedTweet tweet;
	private Date collectionTime;
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getTeam() {
		return team;
	}
	public void setTeam(String team) {
		this.team = team;
	}
	
	public String getTweetJson() {
		return tweetJson;
	}
	public void setTweetJson(String tweet) {
		this.tweetJson = tweet;
	}
	
	@Transient
	public ModifiedTweet getTweet() {
		return tweet;
	}
	public void setTweet(ModifiedTweet tweet) {
		this.tweet = tweet;
	}
	public Date getCollectionTime() {
		return collectionTime;
	}
	public void setCollectionTime(Date collectionTime) {
		this.collectionTime = collectionTime;
	}


	
}
