package com.turner.nba.tweets.jpa;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.apache.log4j.Logger;

//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.nba.tweets.util.TimeCount;


public class TeamTweetDAO {
	Logger logger = Logger.getLogger(this.getClass());
	public static String NBAGAMES = "nbagames";
	public static String SLGAMES = "slgames";
	public static final String NBAFANNIGHT = "nba_fan_night";
	
	public static final int NO_TWEET_LIMIT = -1;
	
	private static TeamTweetDAO instance; 
	private TeamTweetDAO() {}
	
	public static TeamTweetDAO getInstance() {
		if(instance == null) 
			instance = new TeamTweetDAO();
		
		return instance;
	}
	
	public List<TeamLast50Tweet> getLast50Tweets(String name, boolean sortAsc){
		List<TeamLast50Tweet> list = new ArrayList<TeamLast50Tweet>();
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		
		try{
			String sql = "SELECT T FROM TeamLast50Tweet T where T.team = :name ORDER BY T.createdAt "+(sortAsc?"ASC":"DESC");
			Query q = em.createQuery(sql);
			q.setParameter("name", name);
			
			list = (List<TeamLast50Tweet>) q.getResultList();
			//logger.info("Found "+list.size());
		}catch(Exception e){
			logger.error("Could not retrieve all-star tweets for "+name+" from DB", e);
		}finally{
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return list;
	}
	
	public List<TeamTweet> getTweets(String name, int limit) {
		List<TeamTweet> list = new ArrayList<TeamTweet>();
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		
		try{
			Query q = null;
			
			if(limit != NO_TWEET_LIMIT){
				q = em.createQuery("SELECT T FROM TeamTweet T where T.team = :name ORDER BY T.id desc");
				q.setFirstResult(0);
				q.setMaxResults(limit);
			}else{
				q = em.createQuery("SELECT T FROM TeamTweet T where T.team = :name ORDER BY T.id");
			}
			
			q.setParameter("name", name);
			
			list = (List<TeamTweet>) q.getResultList();
			//logger.info("Found "+list.size());
		}catch(Exception e){
			logger.error("Could not retrieve tweets for "+name+" from DB", e);
		}finally{
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return list;
	}
	
	public List<TeamTweet> getTweets(String name){
		return getTweets(name, -1);
	}
	
	public boolean saveTweets(Collection<TeamTweet> tt){
		boolean foundErrors = false;
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		em.getTransaction().begin();
		
		try{
			for(TeamTweet t:tt)
					em.persist(t); 
		}catch(Exception e){
			foundErrors = true;
			logger.error("Could not save all tweets to db", e);
		}finally{
			if(foundErrors)
				em.getTransaction().rollback();
			else
				em.getTransaction().commit();
			
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return foundErrors;
	}
	
	public boolean saveLast50Tweets(Collection<TeamLast50Tweet> tt){
		boolean foundErrors = false;
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		em.getTransaction().begin();
		
		try{
			for(TeamLast50Tweet t:tt)
					em.persist(t); 
		}catch(Exception e){
			foundErrors = true;
			logger.error("Could not save all-star tweets to db", e);
		}finally{
			if(foundErrors)
				em.getTransaction().rollback();
			else
				em.getTransaction().commit();
			
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return foundErrors;
	}
	
	public boolean deleteTweets(String name){
		boolean foundErrors = false;
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		em.getTransaction().begin();
		
		try{
			Query q = em.createQuery("DELETE FROM TeamTweet T where T.team = :name");
			q.setParameter("name", name);
			q.executeUpdate();
		}catch(Exception e){
			foundErrors = true;
			logger.error("Could not delete all tweets to db", e);
		}finally{
			if(foundErrors)
				em.getTransaction().rollback();
			else
				em.getTransaction().commit();
			
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return foundErrors;
	}
	
	public boolean deleteTweets(List<String> names){
		boolean foundErrors = false;
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		em.getTransaction().begin();

		try{
			if(names.isEmpty()){	//there's no game going on... clean out the tweet table
				Query q = em.createQuery("DELETE FROM TeamTweet T");
				q.executeUpdate();
			}else{
				StringBuilder sb = new StringBuilder();
				for(String name:names){
					if(sb.length() > 0) sb.append(", ");
					sb.append(name);
				}

				Query q = em.createQuery("DELETE FROM TeamTweet T where T.team NOT IN ("+sb.toString()+")");
				logger.info("Deleting all tweets where team NOT in "+sb.toString());
				//q.setParameter("name", sb.toString());  jpa issue?
				q.executeUpdate();
			}
		}catch(Exception e){
			foundErrors = true;
			logger.error("Could not delete all tweets to db", e);
		}finally{
			if(foundErrors)
				em.getTransaction().rollback();
			else
				em.getTransaction().commit();
			
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return foundErrors;
	}
	
	public boolean deleteLast50Tweets(String team){
		boolean foundErrors = false;
		
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		em.getTransaction().begin();

		try{
			Query q = em.createQuery("DELETE FROM TeamLast50Tweet T where T.team = :team");
			q.setParameter("team", team);
			q.executeUpdate();
		}catch(Exception e){
			foundErrors = true;
			logger.error("Could not delete all-star tweets to db", e);
		}finally{
			if(foundErrors)
				em.getTransaction().rollback();
			else
				em.getTransaction().commit();
			
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
		
		return foundErrors;
	}
	
	public Game getGameById(String gameId){
		String name = gameId.startsWith("00")?NBAGAMES:SLGAMES;
		//String name = NBAGAMES;
		
		EntityManager em = PersistenceManager.getEntityManager(name);
		try{
			return (Game)em.createQuery("select g from Game g where gameId = :gameId").setParameter("gameId", gameId).getSingleResult();
		}catch(Exception e){
			throw new RuntimeException(e);
		}finally{
			PersistenceManager.closeEntityManager(name);
		}
	}
	
	public List<Game> getGamesWithinWindow(int intervalInMinutes, Date currentTime){
		List<Game> games = new ArrayList<Game>();
				
		try {
			games.addAll(getGamesWithinWindow(intervalInMinutes, currentTime, NBAGAMES));
		} catch (Exception e) {
			logger.warn("Maybe regular NBA league is not in season - will ignore regular season games: " + e.getMessage());
		}

		try {
			List<Game> sl = getGamesWithinWindow(intervalInMinutes, currentTime, SLGAMES);
			games.addAll(sl);
		} catch (Exception e) {
			logger.warn("Maybe summer league is not in season - will ignore summer league games: " + e.getMessage());
		}

		return games;
	}
	
	public List<Game> getGamesWithinWindow(int intervalInMinutes, Date currentTime, String persistenceUnitName){
		List<Game> games = new ArrayList<Game>();
		
		EntityManager em = PersistenceManager.getEntityManager(persistenceUnitName);
		
		try{
			Calendar cal = Calendar.getInstance();
			cal.setTime(currentTime);
			
			cal.add(Calendar.MINUTE, intervalInMinutes);
			Date date1 = cal.getTime();
			
			cal.add(Calendar.MINUTE, -2 * intervalInMinutes);
			Date date2 = cal.getTime();
			
			DateFormat df = new SimpleDateFormat("yyyyMMdd");
			cal.add(Calendar.HOUR, -5);		//account for none-eastern zones
			String calendarDate = df.format(cal.getTime());
			//FIXME this is a potential problem as the END_DATE comes from Sybase, and it is not easten time, it appears it is 1 hour ahead.
			//So we have a much "bigger" window. May not be issue right now, but worth taking a note.
			Query q = em.createQuery("SELECT G FROM Game G where :calendarDate = to_char(G.gameDate, 'YYYYMMDD') and :date1 >= G.gameDate and (G.gameEndDate is null or :date2 <= G.gameEndDate)");
			q.setParameter("calendarDate", calendarDate);
			//q.setParameter("calendarDate", "20120208");		//for testing only
			q.setParameter("date1", date1);
			q.setParameter("date2", date2);
			
			games = (List<Game>) q.getResultList();
		}catch(Exception e){
			logger.warn("Could not retrieve games within +/-"+intervalInMinutes+"minutes from DB: "+persistenceUnitName);
			logger.debug("Could not retrieve games within +/-"+intervalInMinutes+"minutes from DB: "+persistenceUnitName, e);
		}finally{
			PersistenceManager.closeEntityManager(persistenceUnitName);
		}
		
		return games;
	}


	
	public List<TimeCount> countTweetsByMinute(String teamName) {
		List results = new ArrayList();
		EntityManager em = PersistenceManager.getEntityManager(NBAFANNIGHT);
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try{
			Query q = em.createNativeQuery("select to_char(collectionTime,'YYYY-MM-DD HH24:MI') || ':00' as collectionTimeAtMinute, count(id) as tweetsCount from teamtweet where team=:teamName group by to_char(collectionTime,'YYYY-MM-DD HH24:MI')");
			q.setParameter("teamName", teamName);
			
			List<Object[]> rss =  q.getResultList();
			for(Object[] rs : rss){
				String collectionTimeAtMinute = rs[0].toString();
				Integer tweetsCount = Integer.parseInt(rs[1].toString());
				results.add(new TimeCount(dateFormat.parse(collectionTimeAtMinute), tweetsCount));
			}
		}catch(Exception e){
			logger.error("ERROR", e);
		}finally{
			PersistenceManager.closeEntityManager(NBAFANNIGHT);
		}
	
		return results;
	}

	
}
