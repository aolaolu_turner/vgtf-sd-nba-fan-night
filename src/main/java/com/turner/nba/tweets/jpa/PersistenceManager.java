package com.turner.nba.tweets.jpa;

import java.util.*;
import javax.persistence.*;

/**
 * This is the persistence manager from LOKI core plugin.
 * 
 * @author jyou
 *
 */
public class PersistenceManager {
	private static final Map<String, EntityManagerFactory> emfMap = new HashMap<String, EntityManagerFactory>();
	
	private static final Map<String, ThreadLocal<EntityManager>> threadEntityManagerMap = new HashMap<String, ThreadLocal<EntityManager>>();
	
	public static synchronized EntityManager getEntityManager(String persistenceContextName) {
		EntityManagerFactory emf = emfMap.get(persistenceContextName);
		if (emf == null) {
			emf = Persistence.createEntityManagerFactory(persistenceContextName);
			emfMap.put(persistenceContextName, emf);
		}
		ThreadLocal<EntityManager> tl = threadEntityManagerMap.get(persistenceContextName);
		if (tl == null) {
			tl = new ThreadLocal<EntityManager>();
			threadEntityManagerMap.put(persistenceContextName, tl);
		}
		EntityManager em = tl.get();
		if (em == null) {
			em = emf.createEntityManager();
			tl.set(em);
		}
		return em;
	}
	
	public static synchronized void closeEntityManager(String persistenceContextName) {
		ThreadLocal<EntityManager> tl = threadEntityManagerMap.get(persistenceContextName);
		if (tl != null) {
			EntityManager em = tl.get();
			if (em != null) {
				tl.set(null);
				em.close();
			}
		}
	}
}
