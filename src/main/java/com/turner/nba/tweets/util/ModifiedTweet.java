package com.turner.nba.tweets.util;

import java.text.SimpleDateFormat;
import java.util.List;

import org.apache.log4j.Logger;

import com.turner.nba.tweets.util.Tweet.RetweetedStatus;

public class ModifiedTweet {
	//private org.apache.log4j.Logger logger = Logger.getLogger(this.getClass());
	
	private String author;
	private String screen_name;
	private String profile_image_url;
	private String source;
	private String text;
	private Long id;
	private String network = "twitter";
	private String created_at;
	private String time_zone;
	private boolean retweeted;
	
	private RetweetedStatus retweeted_status;
	private TweetEntity entities;
	
	public ModifiedTweet(){}
	public ModifiedTweet(Tweet tweet){
		
		author = tweet.user.name;
		screen_name = tweet.user.screen_name;
		profile_image_url = tweet.user.profile_image_url;
		source = tweet.source;
		text = tweet.text;
		id = tweet.id;
		entities = tweet.entities;
		
		created_at = getFormattedDate(tweet.created_at);
		time_zone = created_at;
		
		retweeted = tweet.retweeted;
		//if(this.retweeted)
			retweeted_status = tweet.retweeted_status;
	}
	
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getScreen_name() {
		return screen_name;
	}
	public void setScreen_name(String screen_name) {
		this.screen_name = screen_name;
	}
	public String getProfile_image_url() {
		return profile_image_url;
	}
	public void setProfile_image_url(String profile_image_url) {
		this.profile_image_url = profile_image_url;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getNetwork() {
		return network;
	}
	public void setNetwork(String network) {
		this.network = network;
	}
	public TweetEntity getEntities() {
		return entities;
	}
	public void setEntities(TweetEntity entities) {
		this.entities = entities;
	}
	
	public String getCreated_at() {
		return created_at;
	}
	public void setCreated_at(String created_at) {
		this.created_at = created_at;
	}
	public String getTime_zone() {
		return time_zone;
	}
	public void setTime_zone(String time_zone) {
		this.time_zone = time_zone;
	}
	
	private static String getFormattedDate(String text){
		String value = "";
		SimpleDateFormat df = new SimpleDateFormat("EEE MMM dd HH:mm:ss yyyy");
		SimpleDateFormat fd = new SimpleDateFormat("yyyyMMdd HH:mm:ss");
		
		try{
			String[] s = text.split(" ");
			String t = "";
			for(int i = 0; i < s.length; i++){
				t += (i == 0 ? "" : " ");
				t += (i != 4 ? s[i] : "");
			}
	    
			value = fd.format(df.parse(t)) + " " + s[4];
		}catch(Exception e){
			Logger.getLogger("com.turner.nba.tweets.util.ModifiedTweet").warn("Could not initialize date e.g. 'created_at' field from '"+text+"'", e);
		}
		
		return value;
	}
	
	public boolean getRetweeted() {
		return retweeted;
	}
	public void setRetweeted(boolean retweeted) {
		this.retweeted = retweeted;
	}
	public RetweetedStatus getRetweeted_status() {
		return retweeted_status;
	}
	public void setRetweeted_status(RetweetedStatus retweeted_status) {
		this.retweeted_status = retweeted_status;
	}
	public String asString(){
		return "{\n" +
	            "\t\"author\": \"" + author +"\",\n" +
	            "\t\"screen_name\": \"" + screen_name +"\",\n" +
	            "\t\"profile_image_url\": \"" + profile_image_url +"\",\n" +
	            "\t\"time_zone\": \"" + created_at +"\",\n" +
	            "\t\"source\": \"" + source +"\",\n" +
	            "\t\"text\": \"" + text +"\",\n" +
	            "\t\"id\": \"" + id +"\",\n" +
	            "\t\"retweeted\": \"" + retweeted +"\"\n" +
	            "\t\"network\": \"twitter\"\n" +
	            "\t\"user_mentions\": \"" + (entities.getUser_mentions().isEmpty()?"":entities.getUser_mentions().get(0).screen_name) + "\"\n" +
	            "}";
	}
	
	public static class CreationTimeComparator implements java.util.Comparator<ModifiedTweet> {
		boolean ascending = true;
		
		public CreationTimeComparator(boolean asc){
			ascending = asc;
		}
		
		public int compare(ModifiedTweet o1, ModifiedTweet o2) {
			return ascending ? o1.created_at.compareTo(o2.created_at) : o2.created_at.compareTo(o1.created_at);
		}
	}
	
	public static class TweetIdComparator implements java.util.Comparator<ModifiedTweet> {
		boolean ascending = true;
		
		public TweetIdComparator(boolean asc){
			ascending = asc;
		}
		
		public int compare(ModifiedTweet o1, ModifiedTweet o2) {
			return ascending ? o1.id.compareTo(o2.id) : o2.id.compareTo(o1.id);
		}
	}
	
	public static void main(String[] args){
		System.out.println(getFormattedDate("Thu Oct 04 13:12:59 +0000 2012"));
	}
}
