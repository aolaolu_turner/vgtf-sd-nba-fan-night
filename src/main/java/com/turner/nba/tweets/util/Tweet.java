package com.turner.nba.tweets.util;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.InputStreamReader;
import java.net.URL;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;

public class Tweet {
	public static class TweetUser {
		public String name;
		public String screen_name;
		public String profile_image_url;

		TweetUser(String name, String screen_name, String profile_image_url){
			this.name = name;
			this.screen_name = screen_name;
			this.profile_image_url = profile_image_url;
		}
	}

	public static class RetweetedStatus {
		public String in_reply_to_user_id_str;
		public String in_reply_to_status_id_str;
		public TweetEntity entities;
		public String in_reply_to_screen_name;
		public Boolean favorited;
		public String text;
		public String source;
		//public String geo;
		public TweetUser user;
		public String created_at;
		public String contributors;
		public Boolean retweeted;
		public long id;
		public long retweet_count;
		public String id_str;
		public String in_reply_to_user_id;
		public String in_reply_to_status_id; 
		public Boolean truncated;
		//public String place;
		
		public RetweetedStatus(String in_reply_to_user_id_str,
				String in_reply_to_status_id_str, TweetEntity entities,
				String in_reply_to_screen_name, Boolean favorited, String text,
				String source, TweetUser user, String created_at,
				String contributors, Boolean retweeted, long id,
				long retweet_count, String id_str, String in_reply_to_user_id,
				String in_reply_to_status_id, Boolean truncated/*, String place*/) {
			super();
			this.in_reply_to_user_id_str = in_reply_to_user_id_str;
			this.in_reply_to_status_id_str = in_reply_to_status_id_str;
			this.entities = entities;
			this.in_reply_to_screen_name = in_reply_to_screen_name;
			this.favorited = favorited;
			this.text = text;
			this.source = source;
			//this.geo = geo;
			this.user = user;
			this.created_at = created_at;
			this.contributors = contributors;
			this.retweeted = retweeted;
			this.id = id;
			this.retweet_count = retweet_count;
			this.id_str = id_str;
			this.in_reply_to_user_id = in_reply_to_user_id;
			this.in_reply_to_status_id = in_reply_to_status_id;
			this.truncated = truncated;
			//this.place = place;
		}
	}

	public TweetUser user;
	public TweetEntity entities;
	public String text;
	public String source;
	public Long id;
	public String created_at;
	public boolean retweeted;
	public RetweetedStatus retweeted_status;

	Tweet(TweetUser user, TweetEntity entities, String text, String source, Long id, String created_at, boolean retweeted, RetweetedStatus status){
		this.user = user;
		this.entities = entities;
		this.text = text;
		this.source = source;
		this.created_at = created_at;
		this.id = id;
		this.retweeted = retweeted;
		if(this.retweeted)
			System.out.println(this.retweeted);
		this.retweeted_status = status;
	}

	public static void main(String[] args) throws Exception {
		String [] links = {
				"http://tweetriver.com/nbadproducts/bobcats.json",
				"http://tweetriver.com/nbadproducts/bucks.json",
				"http://tweetriver.com/nbadproducts/bulls.json",
				"http://tweetriver.com/nbadproducts/cavaliers.json",
				"http://tweetriver.com/nbadproducts/celtics.json",
				"http://tweetriver.com/nbadproducts/clippers.json",
				"http://tweetriver.com/nbadproducts/grizzlies.json",
				"http://tweetriver.com/nbadproducts/hawks.json",
				"http://tweetriver.com/nbadproducts/heat.json",
				"http://tweetriver.com/nbadproducts/hornets.json",
				"http://tweetriver.com/nbadproducts/jazz.json",
				"http://tweetriver.com/nbadproducts/kings.json",
				"http://tweetriver.com/nbadproducts/knicks.json",
				"http://tweetriver.com/nbadproducts/lakers.json",
				"http://tweetriver.com/nbadproducts/magic.json",
				"http://tweetriver.com/nbadproducts/mavericks.json",
				"http://tweetriver.com/nbadproducts/nets.json",
				"http://tweetriver.com/nbadproducts/nuggets.json",
				"http://tweetriver.com/nbadproducts/pacers.json",
				"http://tweetriver.com/nbadproducts/pistons.json",
				"http://tweetriver.com/nbadproducts/raptors.json",
				"http://tweetriver.com/nbadproducts/rockets.json",
				"http://tweetriver.com/nbadproducts/sixers.json",
				"http://tweetriver.com/nbadproducts/spurs.json",
				"http://tweetriver.com/nbadproducts/suns.json",
				"http://tweetriver.com/nbadproducts/timberwolves.json",
				"http://tweetriver.com/nbadproducts/thunder.json",
				"http://tweetriver.com/nbadproducts/trailblazers.json",
				"http://tweetriver.com/nbadproducts/warriors.json",
				"http://tweetriver.com/nbadproducts/wizards.json"
		};

		for(String link:links){
			Gson gson = new GsonBuilder().setPrettyPrinting().create();

			java.lang.reflect.Type tvType = new TypeToken<java.util.ArrayList<Tweet>>() {}.getType();
			URL url = new URL(link);
			BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
			List<Tweet> tweets = gson.fromJson(in, tvType);
			List<ModifiedTweet> tTweets = new ArrayList<ModifiedTweet>();

			for(Tweet tweet:tweets){
				ModifiedTweet tt = new ModifiedTweet(tweet);
				if(tweet.retweeted)
					System.out.println(tt.asString());
				tTweets.add(tt);
			}

			java.lang.reflect.Type ttType = new TypeToken<java.util.ArrayList<ModifiedTweet>>() {}.getType();
			String json = gson.toJson(tTweets, ttType);

			String filename = "data/" + link.split("/")[4];
			BufferedWriter out = new BufferedWriter(new FileWriter(filename));
			out.write(json);
			out.close();

			System.out.println("Done: "+link);
		}
	}

}
