package com.turner.nba.tweets.util;

import java.util.ArrayList;
import java.util.List;

public class HomeAwayTweetConfig {
	GameSync gameSync;
	
	public static class GameSync {
		public Nobrand nobrand;
		
		public GameSync(Nobrand nb){
			nobrand = nb;
		}
	}
	
	public static class Nobrand {
		public List<String> enabledGameIDsFix = new ArrayList<String>();
		
		public Nobrand(List<String> ids){
			enabledGameIDsFix = ids;
		}
	}
	
	public HomeAwayTweetConfig(GameSync gs){
		gameSync = gs;
	}
	
	public List<String> getEnabledGameIDs(){
		return gameSync.nobrand.enabledGameIDsFix;
	}
}
