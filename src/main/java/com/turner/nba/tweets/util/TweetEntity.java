package com.turner.nba.tweets.util;

import java.util.List;

public class TweetEntity {
	public static class TweetEntityHashtag {
		public String text;
		public List<Integer> indices;
		
		TweetEntityHashtag(String text, List<Integer> indices){
			this.text = text;
			this.indices = indices;
		}
	}
	
	public static class TweetEntityMedia {
		public String id;
		public String id_str;
		public List<Integer> indices;
		public String media_url;
		public String media_url_https;
		public String url;
		public String display_url;
		public String expanded_url;
		public String type;
		
		public TweetEntityMedia(String id, String id_str,
				List<Integer> indices, String media_url,
				String media_url_https, String url, String display_url,
				String expanded_url, String type) {
			super();
			this.id = id;
			this.id_str = id_str;
			this.indices = indices;
			this.media_url = media_url;
			this.media_url_https = media_url_https;
			this.url = url;
			this.display_url = display_url;
			this.expanded_url = expanded_url;
			this.type = type;
		}
	}
	
	public static class TweetEntityUrl {
		public String expanded_url;
		public String display_url;
		public String url;
		public List<Integer> indices;
		
		TweetEntityUrl(String eUrl, String dUrl, String url, List<Integer> indices){
			this.expanded_url = eUrl;
			this.display_url = dUrl;
			this.url = url;
			this.indices = indices;
		}
	}
	
	public static class TweetEntityUserMention {
		public String screen_name;
		public String name;
		public String id_string;
		public List<Integer> indices;
		
		TweetEntityUserMention(String screen_name, String name, String id_string, List<Integer> indices){
			this.screen_name = screen_name;
			this.name = name;
			this.id_string = id_string;
			this.indices = indices;
		}
	}
	
	private List<TweetEntityUrl> urls;
	private List<TweetEntityHashtag> hashtags;
	private List<TweetEntityUserMention> user_mentions;
	private List<TweetEntityMedia> media;
	
	public List<TweetEntityUserMention> getUser_mentions() {
		return user_mentions;
	}
	public void setUser_mentions(List<TweetEntityUserMention> user_mentions) {
		this.user_mentions = user_mentions;
	}
	public List<TweetEntityUrl> getUrls() {
		return urls;
	}
	public void setUrls(List<TweetEntityUrl> urls) {
		this.urls = urls;
	}
	public List<TweetEntityHashtag> getHashtags() {
		return hashtags;
	}
	public void setHashtags(List<TweetEntityHashtag> hashtags) {
		this.hashtags = hashtags;
	}
	public List<TweetEntityMedia> getMedia() {
		return media;
	}
	public void setMedia(List<TweetEntityMedia> media) {
		this.media = media;
	}
}
