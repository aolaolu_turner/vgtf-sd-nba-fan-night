package com.turner.nba.util;

import java.io.ByteArrayInputStream;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;

public abstract class Helper {
	public static org.apache.log4j.Logger logger = Logger.getLogger("Helper");
	
	public static Document convertStringToDocument(String xmlData) throws Exception {
		SAXBuilder builder = new SAXBuilder();
		
		xmlData = xmlData.replace('|', ' ');		// Broker is adding a "|" at the end of the XML file. We're removing it.
		logger.debug("xmlData.replace");
	
		//Document doc = builder.build(new File(xmlData));		//for testing only.
		logger.debug("pull out the content from feed.");
		Document doc = builder.build(new ByteArrayInputStream(xmlData.getBytes()));
		logger.debug("XML = " + xmlData);
		
		return doc;
	}

}
