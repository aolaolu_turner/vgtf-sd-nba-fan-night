package com.nba.loki.plugins;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.Hashtable;
import java.util.Properties;

import javax.jms.Message;

import twitter4j.internal.logging.Logger;

import com.turner.loki.AbstractPlugin;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;

/**
 * 
 * This plugin reads property files and can be used to switch on or off in the workflow.
 * To use it, first initialize it with property files names (seperated by comma) and default offAction (by default stopFlow)
 * <action name="checkSwitch" class="com.nba.loki.plugins.EnvironmentSwitch">
		<initialize>
			<param name="propertyFiles" value="switch.properties"/>
			<param name="defaultOffAction" value="stopFlow"/>
		</initialize>
	</action>
 * then, in the property file, 
 * do.something=on or off
 * 
 * In the workflow
 * <action name="...">
 * 		<success action="checkSwitch">
 * 			<param name="key" value="do.something"/>
 * 			<param name="onAction" value="theActionToDoSomething"/>
 * 		</success>
 * 
 */
public class PropertiesSwitch extends AbstractPlugin {
	Logger log = Logger.getLogger(this.getClass());
	Properties switchProperties = new Properties();
	String defaultOffAction="stopFlow";
	public static final String on="on";
	
	@Override
	public void initialize(Hashtable<String, String> props)
			throws InitializationException {
		super.initialize(props);
		//
		if(!props.containsKey("propertyFiles")){
			throw new InitializationException("A parameter named propertyFiles is required");
		}
		String propertyFiles = props.get("propertyFiles");
		String[] files = propertyFiles.split(",");
		for(String file:files){
			URL url = this.getClass().getClassLoader().getResource(file);
			if(url==null){
				throw new InitializationException("could not find a file named " + file+" in classpath");
			}
			BufferedReader r = null;
			try{
				Properties p = new Properties();
				r = new BufferedReader(new InputStreamReader(new FileInputStream(new File(url.getFile()))));
				p.load(r);
				this.switchProperties.putAll(p);
			}catch(Exception e){
				throw new InitializationException("could not laod a file named " + file+" from classpath");
			}finally{
				if(r!=null){
					try {
						r.close();
					} catch (IOException e) {
						log.error("Failed to close", e);
					}
				}
			}
		}
		if(props.containsKey("defaultOffAction")){
			this.defaultOffAction=props.get("defaultOffAction");
		}
		
	}

	
	@Override
	public Object process(Message message) throws PluginException {
		try{
			String key = message.getStringProperty("key");
			String onAction = message.getStringProperty("onAction");
			if(onAction==null){
				throw new PluginException("A parameter named onAction must be provided when using this plugin");
			}
			//check the key
			String onOff = (String)this.switchProperties.get(key);
			
			if(on.equalsIgnoreCase(onOff)){
				message.setStringProperty("nextAction", onAction);
			}else{
				//take it as off
				String offAction = message.getStringProperty("offAction");
				message.setStringProperty("nextAction", offAction==null?this.defaultOffAction:offAction);
			}
			return message;
		}catch(Exception e){
			throw new PluginException(e);
		}
	}

	
	
}
