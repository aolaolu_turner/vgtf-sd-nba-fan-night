<%@ page import="com.turner.loki.core.monitor.*,
				 com.turner.util.threadpool.*,
                 java.util.*,
                 org.apache.log4j.*,
                 java.text.*,
                 com.cnnsi.util.object.cache.*" %>
<%
Enumeration e;
e = request.getHeaderNames();
String userAgent = request.getHeader("user-agent");
String op = (String) request.getParameter("op");
Runtime rt = Runtime.getRuntime();
MonitorDataManager mdm = MonitorDataManager.getInstance();
CacheManager cm = null;
try
{
	cm = CacheManager.getInstance();
	Cache cache = cm.getCache("sessions");
	cache.add("TEST1",new CacheableString("TEST_ONE"));
	cache.add("TEST2",new CacheableString("TEST_TWO"));
	cache.add("TEST3",new CacheableString("TEST_THREE"));
	cache.add("TEST4",new CacheableString("TEST_FOUR"));
	cache.add("TEST5",new CacheableString("TEST_FIVE"));
}
catch(Exception err)
{
    cm = null;
}
if(op == null)
{
}
else if(op.equalsIgnoreCase("gc"))
{
        System.gc();
}
else if(op.contains(".ShowMessage.randomTime"))
{
    	int random = Integer.parseInt(op.substring(op.lastIndexOf('.') + 1,op.length()));
}
else if(op.equalsIgnoreCase("cache.dump"))
{
    	CacheManager.getInstance().dumpCache();
}
else if(op.equalsIgnoreCase("log.DEBUG"))
{
        Logger.getLogger(this.getClass()).getRootLogger().setLevel(Level.DEBUG);}
else if(op.equalsIgnoreCase("log.INFO"))
{
        Logger.getLogger(this.getClass()).getRootLogger().setLevel(Level.INFO);
}
else if(op.equalsIgnoreCase("log.WARN"))
{
        Logger.getLogger(this.getClass()).getRootLogger().setLevel(Level.WARN);
}
else if(op.equalsIgnoreCase("log.ERROR"))
{
        Logger.getLogger(this.getClass()).getRootLogger().setLevel(Level.ERROR);}
        NumberFormat twoDec = NumberFormat.getInstance();
        twoDec.setMaximumFractionDigits(2);
        twoDec.setMinimumFractionDigits(2);
        StringBuffer htmlBuffer = new StringBuffer();
        
        Hashtable allNodes = mdm.getAllNodesStatus();
        Enumeration en = allNodes.keys();
if(op != null && op.equalsIgnoreCase("xml"))
{
%><?xml version="1.0" encoding="ISO-8859-1"?><process name=""><%
        while(en.hasMoreElements())
        {
            
            String key = (String)en.nextElement();
            Status st = (Status)allNodes.get(key);
%><action name="<%=st.getDisplayName()%>"/><%
        }
%></process><%
}
else
{
    
        int count = 0;
        if(userAgent.indexOf("BlackBerry") == -1)
        {
        SimpleDateFormat sdft = new SimpleDateFormat("MM-dd-yyyy HH:mm:ss");
%>
<html>
<head>
  <title>LOKI Monitor Service</title>
<meta http-equiv="refresh" content="60">
</head>
<body class="bodyBackground" topmargin="0" marginheight="0" link="#87BBEE" alink="#87BBEE" vlink="#87BBEE">
<STYLE>
body {font-family:verdana}
td {font-family:verdana; font-size:9;}
input {font-family:verdana; font-size:9}
select {font-family:verdana; font-size:9}
title {font-family:verdana; font-size:20}
a {text-decoration:none}
</STYLE>
<img src="/images/loki.png" height="60"/>Basic Monitoring Tool
<%
        while(en.hasMoreElements())
        {
            
            String key = (String)en.nextElement();
            Status st = (Status)allNodes.get(key);
            if(op != null && op.equalsIgnoreCase("reset"))
            {
                st.reset();
            }
                if(0 == count++)
                {
%>
                <table border=1 cellpadding=0 cellspacing=0> 
                <tr>
                <td align=center colspan=13>Started: <%= new java.util.Date(st.getStartTime()) %> ** Memory Free:<%= rt.freeMemory()/1000000 %>M Total:<%= rt.totalMemory()/1000000 %>M Max:<%= rt.maxMemory()/1000000%>M</td>
                </tr>
                <tr>
                <td align=center>Name</td>
                <td align=center>Description</td>
                <td align=center>Total Msgs</td>
                <td align=center>Last Msg Time</td>
                <td align=center>Status</td>
                <td align=center>Normal Msgs</td>
                <td align=center>Last Msg Proc Time (ms)</td>
                <td align=center>Avg Proc Time (ms)</td>
                <td align=center>Error Msgs</td>
                <td align=center>Last Error</td>
                <td align=center>Last Error Time</td>
                <td align=center>Msgs/Sec</td>
                <td align=center>Msgs/Min</td>
                </tr>
<%
                }
                String color = "white";
                switch(st.getStatusLevel())
                {
                        case 0: color = "lightgreen";break;
                        case 1: color = "yellow";break;
                        case 2: color = "red";break;
                        case 3: color = "red";break;
                        default: color = "green";break;
                }
%>
            <tr bgcolor="<%= color %>">
            <td nowrap=1><%= st.getDisplayName() %></td>
            <td><%= st.getDescription() %></td>
            <td align=right><%= st.getTotalMessages() %></td>
<%
                if(st.getTotalMessages() > 0)
                {
%>
            <td><%= sdft.format(new java.util.Date(st.getLastMessageTime())) %></td>
<%
                }
                else
                {
%>
            <td>&nbsp;</td>
<%
                }
%>
            <td><%= st.getStatusDisplay() %></td>
            <td align=right><%= st.getTotalNormalMessages() %></td>
            <td align=right><%= st.getProcessingTime() %></td>
            <td align=right><%= st.getAverageProcessTime() %></td>
            <td align=left><%= st.getLastError() %>&nbsp;</td>
            <td align=right><%= st.getTotalErrorMessages() %></td>
<%            
                if(st.getTotalErrorMessages() > 0)
                {
%>
                <td><%= new java.util.Date(st.getLastErrorMessageTime()) %></td><%
                }
                else
                {
%>
                <td>&nbsp;</td>
<%
                }
%>
            <td align=right><%= twoDec.format(st.getMessageRateSec()) %></td>
            <td align=right><%= twoDec.format(st.getMessageRateMin()) %></td> 
            </tr>
<%
        }
%>
        </table>
Change log level from <%=Logger.getLogger(this.getClass()).getRootLogger().getLevel() %> to: <a href="/monitor/BasicMonitor.jsp?op=log.DEBUG">DEBUG</a> | <a href="/monitor/BasicMonitor.jsp?op=log.INFO">INFO</a>| <a href="/monitor/BasicMonitor.jsp?op=log.WARN">WARN</a>| <a href="/monitor/BasicMonitor.jsp?op=log.ERROR">ERROR</a><br/>
<a href="/monitor/BasicMonitor.jsp?op=cache.clear">Clear</a> all items in Memory Cache.<br/>
<%
Iterator it = cm.getKeys();
%><br/><br/>Caches currently in use<br/>---------------------------------------<br/><%
while(it.hasNext())
{
    String name = (String)it.next();
    Cache cache = (Cache)cm.getCache(name);
    %><b>Cache Name: <%=name%></b><br/><%
    Iterator cacheIt = cache.cacheMap.keySet().iterator();
    int counter = 0;
    while(cacheIt.hasNext())
    {
        if(counter == 0)
        {
        %>&nbsp;<%=cacheIt.next()%><%
        }
        else
        {
            %>,&nbsp;<%=cacheIt.next()%><% 
        }
        counter++;
    }
}
List tpl = ThreadPoolManager.getInstance().getPoolNames();
%><br/><br/>Thread Pools currently in use<br/>---------------------------------------<br/><%
Iterator itp = tpl.iterator();
while(itp.hasNext())
{
    String name = (String)itp.next();
    ThreadPool tp = ThreadPoolManager.getInstance().getPool(name);
    String poolSize = (String) request.getParameter("pool." + name + ".size");
    String maxPoolSize = (String) request.getParameter("pool." + name + ".maxSize");
    if(poolSize != null)
    {
        tp.setCorePoolSize(Integer.parseInt(poolSize));
    }
    if(maxPoolSize != null)
    {
        tp.setMaximumPoolSize(Integer.parseInt(maxPoolSize));
    }
    
    
    %><b>Threadpool Name: <%=name%></b><br/>
    Completed Tasks:<%=tp.getCompletedTaskCount()%><br/>
    Active Threads:<%=tp.getActiveCount()%><br/>
    Current Pool Size:<%=tp.getCurrentPoolSize()%><br/>
    Min Pool Size:<%=tp.getCorePoolSize()%><br/>
    Max Pool Size:<%=tp.getMaximumPoolSize()%><br/>
    Keep Alive Time:<%=tp.getKeepAliveTime()%><br/>
    Largest Pool Size:<%=tp.getLargestPoolSize()%><br/>
    <br/>
    <%
    
}
        }
        else
        {
%>
<html>
<head>
  <title>LOKI Monitor Service</title>
<meta http-equiv="refresh" content="60">

</head>

<body class="bodyBackground" topmargin="0" marginheight="0" link="#87BBEE" alink="#87BBEE" vlink="#87BBEE">
<STYLE>
body {font-family:system}
td {font-family:system; font-size:8;}
input {font-family:system; font-size:8}
select {font-family:system; font-size:8}
a {text-decoration:none}
</STYLE>
<%
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss");
        while(en.hasMoreElements())
        {
            
            String key = (String)en.nextElement();
            Status st = (Status)allNodes.get(key);
                if(0 == count++)
                {
%>
                <table border="1" cellpadding="0" cellspacing="0" columns="4">
                <tr> 
                <td align="center" colspan="4"><b>LOKI Blackberry Basic Monitor</b></td>
                </tr>
                <tr>
                <td align="center" colspan="4">Free:<%= rt.freeMemory()/1000000 %>M Total:<%= rt.totalMemory()/1000000 %>M Max:<%= rt.maxMemory()/1000000%>M</td>
                </tr>
                <tr>
                <td align="center" width="25%">Name</td>
                <td align="center" width="25%">Ttl Msg</td>
                <td align="center" width="25%">Lst Msg Tm</td>
                <td align="center" width="25%">Er Msg</td>
                </tr>
<%
                }
                String color = "white";
                switch(st.getStatusLevel())
                {
                        case 0: color = "lightgreen";break;
                        case 1: color = "yellow";break;
                        case 2: color = "red";break;
                        case 3: color = "red";break;
                        default: color = "green";break;
                }
                String displayName  = st.getDisplayName();
                if(displayName.length() >= 9)
                {
                                displayName = displayName.substring(0,9);
                }
%>
            <tr bgcolor="<%= color %>">
            <td nowrap="0" width="25%"><%=  displayName %></td>
            <td align="right" width="25%"><%= st.getTotalMessages() %></td>
<%
                if(st.getTotalMessages() > 0)
                {
%>
            <td width="25%"><%= sdf.format(new java.util.Date(st.getLastMessageTime())) %></td>
<%
                }
                else
                {
%>
            <td width="25%">&nbsp;</td>
<%
                }
%>
            <td align="right" width="25%"><%= st.getTotalErrorMessages() %></td> 
            </tr>
<%
        }
%>
        </table>
<%
        }
%>
        </body>
        </html>
<%
}
%>