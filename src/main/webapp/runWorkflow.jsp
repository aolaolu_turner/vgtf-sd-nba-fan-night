<%@ page import="com.turner.loki.*, java.util.*, javax.jms.Message,org.apache.log4j.Logger" %>

<%
XmlWorkflowEngine xmle = XmlWorkflowEngine.getInstance();
//Hashtable actions = XmlWorkflowEngine.actions;

// Get all of the workflow actions from the ActionManager
ActionManager am = ActionManager.getInstance();
TreeSet actions = new TreeSet(am.getActionNames());

Logger logger = Logger.getLogger(this.getClass());
String injectionAction = (String)request.getParameter("injectionAction");
Enumeration paramEnum = request.getParameterNames();
String messageNote = "";
if(request.getParameterMap().size() > 0)
{
        GenericMessage gm = new GenericMessage();
        while (paramEnum.hasMoreElements())
        {
                String key = (String)paramEnum.nextElement();
                if(key.indexOf("name.") > -1)
                {
                        String number = key.substring(key.indexOf(".") + 1,key.length());
                        String name = (String)request.getParameter(key);
                        String value = (String)request.getParameter("value." + number);
                        if(name.length() > 0)
                        {
                                gm.setStringProperty(name,value);
                        }
                }
        }
                gm.setStringProperty("nextAction" , injectionAction);
        gm.setStringProperty("currentAction" , "workflowInjector");
        gm.setStringProperty("encoding", "iso-8859-1");
        messageNote += "GenericMessage sent into workflow " + injectionAction + ":<br/>" + gm.toString();
        xmle.receive(gm);
}



%>
<html>

<head>
  <title>Workflow Runner Utility</title>
</head>
<body>
<font face='tahoma'><center><p><h1>@@@ENV@@@ ENVIRONMENT</h1></p></center>
<p/>
<pre><%=messageNote%></pre>
<form action="runWorkflow.jsp" method="get"/>
Inject Message at: <select name="injectionAction">
<%
//Enumeration en = actions.keys();
Iterator iter = actions.iterator();

String select = " ";
//while (en.hasMoreElements())
while (iter.hasNext())
{
//String name = (String)en.nextElement();
String name = (String)iter.next();

//NAME ALL ACTIVE INJECTABLE WORKFLOWS UNIFORMLY, AND THEY WILL BE LISTED
if (name.startsWith("run"))
{

  if (name.equalsIgnoreCase(injectionAction))
  {
        select = " SELECTED";
  }
  else
  {
        select = " ";
  }
%>
<option<%=select%> value="<%= name %>"><%= name %></option>
<%
  }
}
%>
</select><br/>
<br/>
with parameters:<br/>
Name: <input type="text" name="name.1"/> Value: <input type="text" name="value.1"/><br/>
Name: <input type="text" name="name.2"/> Value: <input type="text" name="value.2"/><br/>
Name: <input type="text" name="name.3"/> Value: <input type="text" name="value.3"/><br/>
Name: <input type="text" name="name.4"/> Value: <input type="text" name="value.4"/><br/>
Name: <input type="text" name="name.5"/> Value: <input type="text" name="value.5"/><br/>
<br/>
<input type="submit" value="Submit"/>&nbsp;&nbsp;<input type="reset" value="Reset"/>
</form>

</font>
</body>

</html>
<%

%>
