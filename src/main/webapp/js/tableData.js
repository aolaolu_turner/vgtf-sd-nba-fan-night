jQuery(document).ready(function(){
	jQuery("#list4").jqGrid({
		datatype: "local",
		height: 200,
	   	colNames:['Name','Description','OP','OPC','OPA','OPM', 'Total Msgs','Last Msg','Status','Normal Msgs','Proc Tm','Avg Proc Tm','Err Msgs','Last Err Tm','Msgs/Sec','Msgs/Min'],
	   	colModel:[
	   		{name:'id',index:'id', width:150, resizable:true, align:'left'},
	   		{name:'desc',index:'desc', width:90, resizable:true},
	   		{name:'poolStat',index:'poolStat', width:20,sortable:false},
	   		{name:'poolCurrent',index:'poolCurrent', width:20,sortable:false},
	   		{name:'poolActive',index:'poolActive', width:20,sortable:false},
	   		{name:'poolMax',index:'poolMax', width:20,sortable:false},
	   		{name:'totalmsg',index:'totalmsg', width:60, align:'right', sorttype:'int'},
	   		{name:'lastmsg',index:'lastmsg', width:110, align:'right', sorttype:'int'},
	   		{name:'status',index:'status', width:60, resizable:true},
	   		{name:'normalmsg',index:'normalmsg', width:60, align:'right', sorttype:'int'},
	   		{name:'proctm',index:'proctm', width:40, align:'right', sorttype:'int'},
	   		{name:'avgproctm',index:'avgproctm', width:90, align:'right', sorttype:'number'},
	   		{name:'errmsg',index:'errmsg', width:40, resizable:true, align:'right'},
	   		{name:'lsterrtm',index:'lsterrtm', width:90, align:'right', resizable:true},
	   		{name:'msgsec',index:'msgsec', width:90, align:'right', sorttype:'number', formatter:'number', formatoptions:{decimalPlaces:2,thousandsSeparator:''}},
	   		{name:'msgmin',index:'msgmin', width:90, align:'right', sorttype:'number', formatter:'number', formatoptions:{decimalPlaces:2,thousandsSeparator:''}}
	   	],
	   	multiselect: false,
	   	autowidth: true,
	   	caption: "<table border='0' cellpadding='0' cellspacing='0'><tr><td rowspan='4'><img src='/images/loki.png' height='50' style='align:left'/></td><td rowspan='4' nowrap='1'>Action Status Monitor</td><td rowspan='4' width='400px'></td><td><table border='0' cellpadding='0' cellspacing='0'><tr><td rowspan='4' style='align:left'><div id='memChart'/></td><td rowspan='4' style='align:left'>&nbsp;</td><td class='memLegend'>Memory</td></tr><tr><td class='memLegend' width='40' align='left'><div id='memDataMax'></div></td></tr><tr><td class='memLegend' align='left'><div id='memDataFree'></div></td></tr><tr><td class='memLegend' align='left'><div id='memDataUsed'></div></td></tr></table></td></tr></table>",
	   	sortname: 1,
	   	height: "auto", 
	   	sortname: 'id',
	    sortorder: 'asc',
	   	pager: '#pager12',
	   	onSelectRow: function(rowid) {
		alert(rowid);
		}	
	}); 
	
});