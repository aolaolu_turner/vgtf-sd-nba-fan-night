<%@ page import="com.turner.loki.*, java.util.*, com.turner.loki.*,javax.jms.Message,org.apache.log4j.Logger" %>

<%
ActionManager am = ActionManager.getInstance();
TreeSet actions = new TreeSet(am.getActionNames());

Logger logger = Logger.getLogger(this.getClass());
String injectionAction = (String)request.getParameter("injectionAction");
String runOnce[] = request.getParameterValues("runOnce");
if(runOnce == null)
{
	runOnce = new String[0];
}
Enumeration paramEnum = request.getParameterNames();
String messageNote = "";
if(request.getParameterMap().size() > 0)
{
	GenericMessage gm = new GenericMessage();
	while (paramEnum.hasMoreElements())
	{
		String key = (String)paramEnum.nextElement();
		if(key.indexOf("name.") > -1)
		{
			String number = key.substring(key.indexOf(".") + 1,key.length());
			String name = (String)request.getParameter(key);		
			String value = (String)request.getParameter("value." + number);
			if(name.length() > 0)
			{
				gm.setStringProperty(name,value);
			}
		}
	}
	try
	{
	for(int i = 0; i < runOnce.length; i++)
	{
		String actionName = runOnce[i];
		Action action = (Action)am.getAction(actionName);
		Object o = action.process(gm);
		if(o instanceof List)
		{
			List l = (List)o;
			if(l.size() > 0)
			{
				Object o2 = l.get(0);
				if(o2 instanceof List)
				{
					List l2 = (List)o2;
					if(l2.size() > 0)
					{
						Object o3 = l2.get(0);
						if(o3 instanceof GenericMessage)
						{
							gm = (GenericMessage)o3;
						}
						else
						{
							logger.error("Cant find a message.");
						}
					}
				}
				else if (o2 instanceof GenericMessage)
				{
					gm = (GenericMessage)o2;
				}
			}
		}
		else if(o instanceof GenericMessage)
		{
			gm = (GenericMessage)o;
		}
		
		//messageNote += "actionName:" + actionName + "</br>";
		//GenericMessage gtm = (GenericMessage)o;

	}
	}
		catch(Exception e)
		{
			logger.error("Got error",e);
		}
		gm.setStringProperty("nextAction" , injectionAction);
	gm.setStringProperty("currentAction" , "workflowInjector");
	messageNote += "GenericMessage sent into workflow " + injectionAction + ":<br/>" + gm.toString();
	XmlWorkflowEngine.getInstance().receive(gm);
}



%>
<html>

<head>
  <title>Workflow Runner Utility</title>
</head>
<body class="bodyBackground" topmargin="0" marginheight="0" link="#87BBEE" alink="#87BBEE" vlink="#87BBEE">
<STYLE>
body {font-family:verdana}
td {font-family:verdana; font-size:9;}
input {font-family:verdana; font-size:9}
select {font-family:verdana; font-size:9}
title {font-family:verdana; font-size:20}
a {text-decoration:none}
</STYLE>
<img src="/images/loki.png" height="60"/>Workflow Injection Tool<br/><br/>
<form action="/tools/runWorkflow.jsp" method="get"/>
Inject Message at: <select name="injectionAction">
<%
Iterator en = actions.iterator();
String select = " ";
while (en.hasNext())
{
String name = (String)en.next();
if (name.equalsIgnoreCase(injectionAction))
{
	select = " SELECTED";
}
else
{
	select = " ";
}
%>
<option<%=select%> value="<%= name %>"><%= name %></option>
<%
}
%>
</select><br/>
<br/>
with parameters:<br/>
Name: <input type="text" name="name.1"/> Value: <input type="text" name="value.1"/><br/>
Name: <input type="text" name="name.2"/> Value: <input type="text" name="value.2"/><br/>
Name: <input type="text" name="name.3"/> Value: <input type="text" name="value.3"/><br/>
Name: <input type="text" name="name.4"/> Value: <input type="text" name="value.4"/><br/>
Name: <input type="text" name="name.5"/> Value: <input type="text" name="value.5"/><br/>
<br/>
Append output from the following workflow nodes:<br/><select name = "runOnce" multiple size="5">
<%
en = actions.iterator();
while (en.hasNext())
{
String name = (String)en.next();
%>
<option<%=select%> value="<%= name %>"><%= name %></option>
<%
}
%>
</select><br/>
<br/>
<input type="submit" value="Run Workflow"/>&nbsp;&nbsp;<input type="reset" value="Reset"/>
</form>

</body>

</html>
<%

%>