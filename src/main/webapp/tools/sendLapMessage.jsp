<%@ page import="com.turner.loki.*, java.util.*, com.turner.loki.core.plugins.*,javax.jms.Message,org.apache.log4j.Logger" %>

<%
XmlWorkflowEngine xmle = XmlWorkflowEngine.getInstance();
System.out.println("WorkflowEngine:" + xmle);
Hashtable actions = XmlWorkflowEngine.actions;
Logger logger = Logger.getLogger(this.getClass());
String injectionAction = (String)request.getParameter("injectionAction");
String runOnce[] = request.getParameterValues("runOnce");
if(runOnce == null)
{
	runOnce = new String[0];
}
Enumeration paramEnum = request.getParameterNames();
String messageNote = "";
GenericMessage gm = new GenericMessage();
if(request.getParameterMap().size() > 0)
{
	
	while (paramEnum.hasMoreElements())
	{
		String key = (String)paramEnum.nextElement();
		if(key.indexOf("name.") > -1)
		{
			String number = key.substring(key.indexOf(".") + 1,key.length());
			String name = (String)request.getParameter(key);		
			String value = (String)request.getParameter("value." + number);
			if(name.length() > 0)
			{
				gm.setStringProperty(name,value);
			}
		}
	}
	gm.setStringProperty("loki_nextAction" , injectionAction);
	messageNote += "GenericMessage sent into workflow " + injectionAction + ":<br/>" + gm.toString();
	System.out.println("GenericMessage:" + gm);
	xmle.receive(gm);
}



%>
<html>

<head>
  <title>Workflow Runner Utility</title>
</head>
<body>
<pre><%=messageNote%></pre>
<form action="/tools/sendLapMessage.jsp" method="get"/>
<input type="hidden" name="injectionAction" value="multiplexFeedburner">
<br/>
<br/>
<input type="hidden" name="name.1" value="type"/><input type="hidden" name="value.1" value="T"/>
<input type="hidden" name="name.2" value="time"/>Time to Display: <input type="text" name="value.2" value="30"/><br/>
<input type="hidden" name="name.3" value="body"/>Message:<input type="text" name="value.3"/><br/>
<br/>
<br/>
<br/>
<input type="submit" value="Run Workflow"/>&nbsp;&nbsp;<input type="reset" value="Reset"/>
</form>

</body>

</html>
<%

%>