<%@ page import="com.cnnsi.loki.*, java.util.*, com.cnnsi.loki.workflow.*,javax.jms.Message,org.apache.log4j.Logger" %>
<%String function="cnnHideSubnav()";
%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
	<title>NASCAR.COM</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta http-equiv="Content-Language" content="en-us">
	<link rel="stylesheet" type="text/css" href="/css/2.0/global.css">
	<link rel="stylesheet" type="text/css" href="/css/2.0/comm.css">
	<link rel="shortcut icon" href="/favicon.ico">
	<script type="text/javascript" src="/js/2.0/global.js"></script>

	<script type="text/javascript" src="/js/2.0/comm.js"></script>
</head>
<body onload="cnnHandleCSIs();<%=function%>">
	<div id="cnnContainer">
		<script>cnnBuildNav();</script>
		    <div id="cnnFooter">
			<a href="/guides/customer_service/contact/">Help</a>|<a href="/guides/privacy/">Privacy Policy</a>|<a href="/guides/terms/">Terms of Use</a>|<a href="/guides/about/nascar/">About NASCAR</a>|<a href="/guides/about/">About NASCAR.COM</a>|<a href="/guides/jobs/">Jobs</a>|<a href="/guides/sponsors/">Official Sponsors</a>|<a href="/guides/advertising/">Advertising</a>

	        <p><img src="http://i.a.cnn.net/nascar/.element/img/2.0/global/footer/logo_external_site.gif" width="20" height="13" alt=""> All External sites will open in a new browser window. NASCAR.COM does not endorse external sites.</p>
	        <p><img src="http://i.a.cnn.net/nascar/.element/img/2.0/global/footer/logo_car.gif" width="40" height="13" alt=""> &copy; 2007 NASCAR | Turner Sports Interactive, Inc. All Rights Reserved.</p>
	    </div>
		<p class="cnnClear" style="height:0; margin:-20px 0 0; line-height:0;">
			<script>
var fc_track=screen.width+'x'+screen.height;
</script>
</body>
</html>