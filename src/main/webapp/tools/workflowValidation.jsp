<%@ page import="com.turner.loki.core.monitor.*,
                 com.turner.loki.*,
                 com.turner.loki.annotations.*,
                 com.turner.loki.core.ui.*,
                 java.util.*,
                 java.lang.*,
                 org.apache.commons.pool.*,
                 org.apache.log4j.*,
                 java.text.*" %>
<%
ActionHelper ah = new ActionHelper();
%>
<html>
<body class="bodyBackground" topmargin="0" marginheight="0" link="#87BBEE" alink="#87BBEE" vlink="#87BBEE">
<STYLE>
body {font-family:verdana}
td {font-family:verdana; font-size:9;}
input {font-family:verdana; font-size:9}
select {font-family:verdana; font-size:9}
title {font-family:verdana; font-size:20}
a {text-decoration:none}
</STYLE>
<img src="/images/loki.png" height="60"/>Workflow Validation Tool<br/><br/>
<table border="1" cellpadding="1" cellspacing="0" width="100%">
<tr><th>Name</th><th>Action</th><th>Class</th><th>Description</th><th>Dependencies</th><th>Success Actions</th><th>Failure Actions</th></tr>
<%
Iterator it = ah.getActionsNameList().iterator();

while(it.hasNext())
{
    String name = (String)it.next();
    boolean isValid = ah.validateAction(name);
    String color = "00FF00";
    if(!isValid)
    {
        color = "FF0000";
    }
    Hashtable stats = ah.getPoolStats(name);
    String actionDescription = "&nbsp;";
    String actionClassName = "&nbsp;";
    String classDependenciesName = "NONE";
    String fullClassName = "";
    PoolablePluginManager ppm = PoolablePluginManager.getInstance();
    ObjectPool objectPool = ppm.getObjectPool(name);
    if(objectPool != null)
    {
	    Object action = null;
	    try
	    {
	    	action = objectPool.borrowObject();
	    	if(action != null)
	    	{
				Description description = action.getClass().getAnnotation(Description.class);
				if(description != null)
				{
				    actionDescription = description.value();
				}
				Name className = action.getClass().getAnnotation(Name.class);
				fullClassName = action.getClass().getName();
				if(className != null)
				{
				    actionClassName = className.value();
				}
				ClassDependencies classDependencies = action.getClass().getAnnotation(ClassDependencies.class);
				if(classDependencies != null)
				{
				    classDependenciesName = classDependencies.value()[0];
				}
	    	}
	    }
	    catch(Exception e)
	    {
	        e.printStackTrace();
	    }
	    finally
	    {
	        if(action!= null)
	        objectPool.returnObject(action);
	    }
    }
	Iterator its = ah.getSuccessActionsList(name).iterator();
%>
<tr valign="top"><td><%=actionClassName %><td><font color="<%=color%>"><%=name %></font></td><td><%=fullClassName %></td><td><%=actionDescription %></td><td><%=classDependenciesName %>&nbsp;</td><td>
<%
	boolean hasAction = false;
	while(its.hasNext())
	{
	    hasAction = true;
	    String actionName = (String)its.next();
	    isValid = ah.validateResponseAction(actionName);
	    color = "00FF00";
	    if(!isValid)
	    {
	        color = "FF0000";
	    }
%><font color="<%=color%>"><%=actionName%></font><br/>
<%
	}
	if(!hasAction)
	{
%>
&nbsp;
<%
	}
%></td><td>
<%
	Iterator itf = ah.getFailureActionsList(name).iterator();
	hasAction = false;
	while(itf.hasNext())
	{
	    hasAction = true;
	    String actionName = (String)itf.next();
	    isValid = ah.validateResponseAction(actionName);
	    color = "00FF00";
	    if(!isValid)
	    {
	        color = "FF0000";
	    }
%><font color="<%=color%>"><%=actionName%></font><br/>
<%
	}
	if(!hasAction)
	{
%>
&nbsp;
<%
	}
%></td></tr>
<%
}
%>
</table>
<%=ah.getFlowModel() %>
</html>