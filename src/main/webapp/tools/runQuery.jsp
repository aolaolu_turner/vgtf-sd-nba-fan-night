<%@ page import="com.turner.loki.*, java.util.*, com.turner.loki.*,javax.jms.Message,org.apache.log4j.Logger, com.turner.nba.ws.dao.HelloServiceDAO" %>

<%
Logger logger = Logger.getLogger("com.turner.nba.runQuery.jsp");
HelloServiceDAO dao = new HelloServiceDAO();

String errorFlag = "\"STATUS\":\"ERROR\""; 
String successFlag = "\"STATUS\":\"SUCCESS\""; 

String records = "{}";

try {
	String sql = request.getParameter("query");
	String persistenceUnitName = request.getParameter("punit");
	String columnStr = request.getParameter("columns");
	
	logger.info("Query:\t"+sql);
	logger.info("Persistence Unit:\t"+persistenceUnitName);
	
	if(columnStr != null){
		String[] columns = columnStr.split(",");
		records = dao.getJsonResultListByNativeQuery(persistenceUnitName, sql, columns);
	}else{
		records = dao.getJsonResultListByNativeQuery(persistenceUnitName, sql);
	}
	
	String output = "{"+successFlag+", \"DATA\":"+records+"}";
	out.println(output);
} catch(Exception e){
	logger.error(e.getMessage(), e);
	
	out.println("{"+errorFlag+", \"ERROR\":\""+e.getMessage()+"\"}");
}
%>