<%@ page import="com.cnnsi.loki.*, java.util.*, com.cnnsi.loki.workflow.*,javax.jms.Message,org.apache.log4j.Logger" %>

<%
XmlWorkflowEngine xmle = XmlWorkflowEngine.getInstance();
Hashtable actions = XmlWorkflowEngine.actions;
Logger logger = Logger.getLogger(this.getClass());
String injectionAction = (String)request.getParameter("injectionAction");
Enumeration paramEnum = request.getParameterNames();
String messageNote = "";
if(request.getParameterMap().size() > 0)
{
	GenericMessage gm = new GenericMessage();
	while (paramEnum.hasMoreElements())
	{
		String key = (String)paramEnum.nextElement();
		if(key.indexOf("name.") > -1)
		{
			String number = key.substring(key.indexOf(".") + 1,key.length());
			String name = (String)request.getParameter(key);		
			String value = (String)request.getParameter("value." + number);
			if(name.length() > 0)
			{
				gm.setStringProperty(name,value);
			}
		}
	}
	try
	{
	
		Action action = (Action)actions.get("lastUpdatedDate");
		Object o = action.process(gm);
		if(o instanceof List)
		{
			List l = (List)o;
			if(l.size() > 0)
			{
				Object o2 = l.get(0);
				if(o2 instanceof List)
				{
					List l2 = (List)o2;
					if(l2.size() > 0)
					{
						Object o3 = l2.get(0);
						if(o3 instanceof GenericMessage)
						{
							gm = (GenericMessage)o3;
						}
						else
						{
							logger.error("Cant find a message.");
						}
					}
				}
				else if (o2 instanceof GenericMessage)
				{
					gm = (GenericMessage)o2;
				}
			}
		}
		else if(o instanceof GenericMessage)
		{
			gm = (GenericMessage)o;
		}
		
		//messageNote += "actionName:" + actionName + "</br>";
		//GenericMessage gtm = (GenericMessage)o;
	}
		catch(Exception e)
		{
			logger.error("Got error",e);
		}
		gm.setStringProperty("nextAction" , injectionAction);
	gm.setStringProperty("currentAction" , "workflowInjector");
	gm.setStringProperty("encoding", "iso-8859-1");
	messageNote += "GenericMessage sent into workflow " + injectionAction + ":<br/>" + gm.toString();
	xmle.receive(gm);
}



%>
<html>

<head>
  <title>Workflow Runner Utility</title>
</head>
<body>
<pre><%=messageNote%></pre>
<form action="/tools/worldcup/runWorkflow.jsp" method="get"/>
Inject Message at: <select name="injectionAction">
<%
Enumeration en = actions.keys();
String select = " ";
while (en.hasMoreElements())
{
String name = (String)en.nextElement();
if (name.equalsIgnoreCase("runSoccerDumpComplete")
    || name.equalsIgnoreCase("runSoccerStandings")
    || name.equalsIgnoreCase("runSoccerAllTeams")
    || name.equalsIgnoreCase("runSoccerAllPlayers")
    || name.equalsIgnoreCase("runSoccerAllStats")
    || name.equalsIgnoreCase("soccerGroupStandingsSchedule")
	|| name.equalsIgnoreCase("soccerAllTeams")
	|| name.equalsIgnoreCase("soccerAllTeamsRoster")
	|| name.equalsIgnoreCase("soccerAllTeamsSchedule")
	|| name.equalsIgnoreCase("soccerAllPlayersGoalie")
	|| name.equalsIgnoreCase("soccerAllPlayersNonGoalie")
	|| name.equalsIgnoreCase("soccerStandingsSchedule")
	|| name.equalsIgnoreCase("soccerStatsGatewayPlayerLeaders")
	|| name.equalsIgnoreCase("soccerPlayerNonGoalieStatLeaders")
	|| name.equalsIgnoreCase("soccerPlayerGoalieStatLeaders")
	|| name.equalsIgnoreCase("soccerTeamStatLeaders")
	|| name.equalsIgnoreCase("soccerTeam")
	|| name.equalsIgnoreCase("soccerTeamRoster")
	|| name.equalsIgnoreCase("soccerTeamSchedule")
	|| name.equalsIgnoreCase("soccerPlayerGoalie")
	|| name.equalsIgnoreCase("soccerPlayerNonGoalie")
	|| name.equalsIgnoreCase("soccerGroupStandingsSchedule"))
{
    
  if (name.equalsIgnoreCase(injectionAction))
  {
	select = " SELECTED";
  }
  else
  {
	select = " ";
  }
%>
<option<%=select%> value="<%= name %>"><%= name %></option>
<%
  }
}
%>
</select><br/>
<br/>
with parameters:<br/>
Name: <input type="text" name="name.1"/> Value: <input type="text" name="value.1"/><br/>
Name: <input type="text" name="name.2"/> Value: <input type="text" name="value.2"/><br/>
Name: <input type="text" name="name.3"/> Value: <input type="text" name="value.3"/><br/>
Name: <input type="text" name="name.4"/> Value: <input type="text" name="value.4"/><br/>
Name: <input type="text" name="name.5"/> Value: <input type="text" name="value.5"/><br/>
<br/>
<input type="submit" value="Submit"/>&nbsp;&nbsp;<input type="reset" value="Reset"/>
</form>

</body>

</html>
<%

%>