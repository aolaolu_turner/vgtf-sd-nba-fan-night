package com.turner.nba.tweets.plugin;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;

import javax.jms.JMSException;

import org.apache.log4j.Level;
import org.joda.time.DateTimeZone;
import org.json.simple.JSONObject;
import org.junit.Test;

import com.turner.loki.GenericMessage;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.nba.tweets.util.TimeCount;

public class CountTeamTweetsTest {

	@Test
	public void testCreateEmptyBuckets() throws InitializationException, ParseException{
		int publishIntervalInMinutes = 10;
		int publishMinutesBeforeGameStart = 15;
		CountGameTweets ctt = new CountGameTweets();
		//ctt.initialize(new Hashtable());
		ctt.logger.setLevel(Level.DEBUG);
		Date gameStartTime = CountGameTweets.dateFormat.parse("20130112 11:00:00");
		{
			Date stopTime = CountGameTweets.dateFormat.parse("20130112 10:59:00");
			List<TimeCount> buckets = ctt.createTweetCountBuckets(gameStartTime, publishMinutesBeforeGameStart, publishIntervalInMinutes, stopTime);
		}
		{
			Date stopTime = CountGameTweets.dateFormat.parse("20130112 11:00:00");
			List<TimeCount> buckets = ctt.createTweetCountBuckets(gameStartTime, publishMinutesBeforeGameStart, publishIntervalInMinutes, stopTime);
		}
		{
			Date stopTime = CountGameTweets.dateFormat.parse("20130112 11:01:00");
			List<TimeCount> buckets = ctt.createTweetCountBuckets(gameStartTime, publishMinutesBeforeGameStart, publishIntervalInMinutes, stopTime);
		}
		{
			Date stopTime = CountGameTweets.dateFormat.parse("20130112 11:31:00");
			List<TimeCount> buckets = ctt.createTweetCountBuckets(gameStartTime, publishMinutesBeforeGameStart, publishIntervalInMinutes, stopTime);
		}
		{
			Date stopTime = CountGameTweets.dateFormat.parse("20130112 12:31:00");
			List<TimeCount> buckets = ctt.createTweetCountBuckets(gameStartTime, publishMinutesBeforeGameStart, publishIntervalInMinutes, stopTime);
		}
	}
	
	@Test
	public void testFillBuckets() throws ParseException{
		int publishIntervalInMinutes = 10;
		int publishMinutesBeforeGameStart = 15;
		CountGameTweets ctt = new CountGameTweets();
		//ctt.initialize(new Hashtable());
		ctt.logger.setLevel(Level.DEBUG);
		Date gameStartTime = CountGameTweets.dateFormat.parse("20130112 11:00:00");
		{
			Date stopTime = CountGameTweets.dateFormat.parse("20130112 12:20:00");
			List<TimeCount> tweetCountsBuckets = ctt.createTweetCountBuckets(gameStartTime, publishMinutesBeforeGameStart, publishIntervalInMinutes, stopTime);
			//fake data
			List<TimeCount> tweetsCountByMinutes = new ArrayList<TimeCount>();
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 10:45:31"), 100));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 10:49:01"), 3));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 10:55:31"), 7));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 10:57:31"), 8));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 11:03:31"), 2));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 11:15:31"), 9));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 11:45:31"), 22));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 11:55:31"), 32));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 12:03:31"), 21));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 12:04:31"), 70));
			tweetsCountByMinutes.add(new TimeCount(CountGameTweets.dateFormat.parse("20130112 12:05:31"), 3));
			System.out.println("database:");
			for(TimeCount tc:tweetsCountByMinutes){
				System.out.println("\t"+CountGameTweets.dateFormat.format(tc.time) +"\t\t"+tc.count);
			}
			ctt.fillBucketsWithTweetCounts(tweetCountsBuckets, tweetsCountByMinutes);
			System.out.println("filled:");
			for(TimeCount tc:tweetCountsBuckets){
				System.out.println("\t"+CountGameTweets.dateFormat.format(tc.time) +"\t\t"+tc.count);
			}
		}
	}
	
	@Test
	public void testCountTweetsBetweenTimestamp() throws ParseException{
		int publishIntervalInMinutes = 10;
		int publishMinutesBeforeGameStart = 15;
		CountGameTweets ctt = new CountGameTweets();
		//ctt.initialize(new Hashtable());
		ctt.logger.setLevel(Level.DEBUG);
		String teamName = "lakers";
		Date gameStartTime = ctt.dateFormat.parse("20130113 21:30:00");
		Map<String, Object> res = ctt.countTweetsBetweenTimestamp(teamName, gameStartTime,publishMinutesBeforeGameStart, publishIntervalInMinutes, new Date());
		System.out.println(res);
		System.out.println("total " + res.get("total") + " at " + res.get("timestamp"));
		List<TimeCount> counts = (List<TimeCount>)res.get("buckets");
		for(TimeCount tc:counts){
			System.out.println(tc.time + " " + tc.count);
		}
	}
	
	@Test
	public void testCountAndJson()throws ParseException{
		
			int publishIntervalInMinutes = 1;
			int publishMinutesBeforeGameStart = 15;
			
			CountGameTweets ctt = new CountGameTweets();
			//ctt.initialize(new Hashtable());
			ctt.logger.setLevel(Level.DEBUG);
			Date stopTime = ctt.dateFormat.parse("20130114 23:50:00");
			String teamName = "lakers";
			Date gameStartTime = ctt.dateFormat.parse("20130113 21:30:00");
			Map<String, Object> res1 = ctt.countTweetsBetweenTimestamp(teamName, gameStartTime, publishMinutesBeforeGameStart, publishIntervalInMinutes, stopTime);
			System.out.println("team1 total " + res1.get("total") + " at " + res1.get("timestamp"));
			
			String teamName2 = "cavaliers";
			
			Map<String, Object> res2 = ctt.countTweetsBetweenTimestamp(teamName2, gameStartTime, publishMinutesBeforeGameStart,  publishIntervalInMinutes, stopTime);
			System.out.println("team 2 total " + res2.get("total") + " at " + res2.get("timestamp"));
			//
			//JSONObject jo = ctt.jsonizeGameTweetCounts(res1, res2);
			//System.out.println("json:"+jo.toString());
	}
	
	@Test
	public void testPlugin() throws InitializationException, JMSException, PluginException{
		Hashtable props = new Hashtable();
		props.put("publishIntervalInMinutes", "1");
		props.put("publishMinutesBeforeGameStart", "15");
		
		props.put("path","/nba/data/json/cms/noseason/");
		props.put("nickNameToSocialNameMap1","76ers|sixers");
		CountGameTweets ctt = new CountGameTweets();
		ctt.logger.setLevel(Level.DEBUG);
		ctt.initialize(props);
		GenericMessage gm = new GenericMessage();
		gm.setStringProperty("gameId","0021200544");
		//gm.setStringProperty("gameDate","20130113 21:30:00");
		//gm.setStringProperty("visitor","cavaliers");
		//gm.setStringProperty("home","lakers");
		gm.setStringProperty("now","20130113 23:40:00");
		//
		ctt.process(gm);
		System.out.println(gm);
	}
	
	@Test
	public void testUTCTime() throws ParseException{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date now = new Date();
		System.out.println(sdf.format(now) + " " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z").format(now));
		DateTimeZone timezone = DateTimeZone.forID("America/New_York");
		Date publishUtc = new Date(timezone.convertLocalToUTC(now.getTime(), false));
		System.out.println(sdf.format(publishUtc));
		//summer time?
		
		Date summer = sdf.parse("2012-08-01 20:00:00");
		System.out.println(sdf.format(summer) + " " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss Z").format(summer));
		Date publishUtc2 = new Date(timezone.convertLocalToUTC(summer.getTime(), false));
		System.out.println(sdf.format(publishUtc2));
		
	}
}
