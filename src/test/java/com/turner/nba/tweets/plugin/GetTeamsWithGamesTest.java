package com.turner.nba.tweets.plugin;

import java.util.Hashtable;
import java.util.List;

import javax.jms.JMSException;

import org.junit.Test;

//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.loki.GenericMessage;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;

public class GetTeamsWithGamesTest {

	@Test
	public void testGetTeamsMultiPlux() throws InitializationException, JMSException, PluginException{
		//first get the games
		GetGamesInWindow ggi = new GetGamesInWindow();
		{
			Hashtable props = new Hashtable();
			props.put("game_window_in_minutes", "15");
			ggi.initialize(props);
		}
		//
		GenericMessage gm = new GenericMessage();
		gm.setStringProperty("now", "20130115 20:00:00");
		ggi.process(gm);
		System.out.println(gm);
		List<Game> games = (List<Game>) gm.getObjectProperty("games");
		if(games!=null){
			for(Game g:games){
				System.out.println(g.getGameId() + " " + g.getGameDate() + " " + g.getGameEndDate() + " " + g.getVisitorNickname() + " " + g.getHomeNickname());
			}
		}
		
		GetTeamsWithGames gtw = new GetTeamsWithGames();
		{
			Hashtable props = new Hashtable();
			props.put("keepTweets", "true"); //do not delete REF data
			props.put("game_window_in_minutes","15");
			props.put("team_tweet_url","http://tweetriver.com/nbadproducts/{team}.json");
			props.put("nameMap1", "76ers|sixers");
			gtw.initialize(props);
		}
		//
		List<GenericMessage> gms = gtw.multiplex(gm);
		for(GenericMessage m:gms){
			System.out.println(m);
		}
	}
}
