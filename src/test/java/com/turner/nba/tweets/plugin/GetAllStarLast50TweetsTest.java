package com.turner.nba.tweets.plugin;

import static org.junit.Assert.*;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.turner.nba.tweets.jpa.TeamLast50Tweet;
import com.turner.nba.tweets.jpa.TeamTweetDAO;
import com.turner.nba.tweets.util.ModifiedTweet;

public class GetAllStarLast50TweetsTest {
	Gson gson = new GsonBuilder().setPrettyPrinting().create();
	GetAllStarLast50Tweets plugin;
	
	static String feedName = "summerleague";
	int count = 50;
	
	static private void loadSampleTweets(int count){
		List<TeamLast50Tweet> tweets4db = new ArrayList<TeamLast50Tweet>();
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss +0000");
		
		for(int i = 0;i < count;i++){
			TeamLast50Tweet teamTweet = new TeamLast50Tweet();
			teamTweet.setTeam(feedName);
			teamTweet.setTweetJson(sb.toString());
			teamTweet.setCollectionTime(new Date());
			teamTweet.setCreatedAt(df.format(new Date()));
			tweets4db.add(teamTweet);
		}
		
		TeamTweetDAO.getInstance().saveLast50Tweets(tweets4db);
	}
	
	@Before
	public void setup() throws Exception {
		plugin = new GetAllStarLast50Tweets();
		
		Hashtable<String, String> props = new Hashtable<String, String>();
		props.put(GetAllStarLast50Tweets.COUNT, ""+count);
		props.put(GetAllStarLast50Tweets.PATH, "/nba/data/nba/noseason/cms/nba/");
		props.put(GetAllStarLast50Tweets.START_DATE, "20130517 00:00:00");
		props.put(GetAllStarLast50Tweets.END_DATE, "20130617 00:00:00");
		props.put(GetAllStarLast50Tweets.TWEET_URL, "http://tweetriver.com/nbadproducts/summerleague.json");
		props.put(GetAllStarLast50Tweets.FEED_NAME, feedName);
		props.put(GetAllStarLast50Tweets.SORT_TWEETS, "desc");
		props.put(GetAllStarLast50Tweets.KEEP_TWEETS, "true");
		
		plugin.initialize(props);
	}
	@Test
	public void testSaveModifiedTweetsToDatabase() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetTweetsFromMassRelevance() {
		fail("Not yet implemented");
	}

	@Test
	public void testGetModifiedTweetsFromDatabase() {
		List<ModifiedTweet> tweets = plugin.getModifiedTweetsFromDatabase(gson, true);
		assertTrue("tweets should not be more than 50", tweets.size() > count);
	}
	
	@Test
	public void testGetModifiedTweetsFromDatabase2() {
		List<ModifiedTweet> tweets = plugin.getModifiedTweetsFromDatabase(gson, false);
		assertTrue("tweets should not be "+count, tweets.size() == count);
	}

	static StringBuilder sb = new StringBuilder();
	static{
		TestHelper.initLogger();
		
		//loadSampleTweets(50000);
		
		sb.append("{");
		sb.append("\"author\": \"Daniel Ritz .\",");
		sb.append("\"screen_name\": \"DanielRitz13\",");
		sb.append("\"profile_image_url\": \"http://a0.twimg.com/profile_images/3642312319/8d19c73e0e767221fa6112954bb4821c_normal.jpeg\",");
		sb.append("\"source\": \"\\u003ca href\\u003d\\\"http://twitter.com/download/iphone\\\" rel\u003d\\\"nofollow\\\"\u003eTwitter for iPhone\u003c/a\u003e\",");
		sb.append("\"text\": \"RT @InsideHoops: NBA #SummerLeague in Las Vegas is July 12-22, includes mini-tournament and championship: http://t.co/Py6aCHnoPT\",");
		sb.append("\"id\": 339799937741103104,");
		sb.append("\"network\": \"twitter\",");
		sb.append("\"created_at\": \"20130529 17:46:32 +0000\",");
		sb.append("\"time_zone\": \"20130529 17:46:32 +0000\",");
		sb.append("\"retweeted\": false,");
		sb.append("\"retweeted_status\": {");
		sb.append("\"entities\": {");
		sb.append("\"urls\": [");
		sb.append("{");
		sb.append("\"expanded_url\": \"http://www.insidehoops.com/vegas-summer-league.shtml\",");
		sb.append("\"display_url\": \"insidehoops.com/vegas-summer-lâ¦\",");
		sb.append("\"url\": \"http://t.co/Py6aCHnoPT\",");
		sb.append("\"indices\": [");
		sb.append("89,");
		sb.append("111");
		sb.append("]");
		sb.append("}");
		sb.append("],");
		sb.append("\"hashtags\": [");
		sb.append("{");
		sb.append("\"text\": \"SummerLeague\",");
		sb.append("\"indices\": [");
		sb.append("4,");
		sb.append("17");
		sb.append("]");
		sb.append("}");
		sb.append("],");
		sb.append("\"user_mentions\": []");
		sb.append("},");
		sb.append("\"favorited\": false,");
		sb.append("\"text\": \"NBA #SummerLeague in Las Vegas is July 12-22, includes mini-tournament and championship: http://t.co/Py6aCHnoPT\",");
		sb.append("\"source\": \"\u003ca href\u003d\\\"http://www.tweetdeck.com\\\" rel\u003d\\\"nofollow\\\"\u003eTweetDeck\u003c/a\u003e\",");
		sb.append("\"user\": {");
		sb.append("\"name\": \"InsideHoops.com\",");
		sb.append("\"screen_name\": \"InsideHoops\",");
		sb.append("\"profile_image_url\": \"http://a0.twimg.com/profile_images/2447957231/1d6bgfrp57y437ndqjcf_normal.jpeg\"");
		sb.append("},");
		sb.append("\"created_at\": \"Wed May 29 17:45:40 +0000 2013\",");
		sb.append("\"retweeted\": false,");
		sb.append("\"id\": 339799715963076608,");
		sb.append("\"retweet_count\": 1,");
		sb.append("\"id_str\": \"339799715963076608\",");
		sb.append("\"truncated\": false");
		sb.append("},");
		sb.append("\"entities\": {");
		sb.append("\"urls\": [");
		sb.append("{");
		sb.append("\"expanded_url\": \"http://www.insidehoops.com/vegas-summer-league.shtml\",");
		sb.append("\"display_url\": \"insidehoops.com/vegas-summer-lâ¦\",");
		sb.append("\"url\": \"http://t.co/Py6aCHnoPT\",");
		sb.append("\"indices\": [");
		sb.append("106,");
		sb.append("128");
		sb.append("]");
		sb.append("}");
		sb.append("],");
		sb.append("\"hashtags\": [");
		sb.append("{");
		sb.append("\"text\": \"SummerLeague\",");
		sb.append("\"indices\": [");
		sb.append("21,");
		sb.append("34");
		sb.append("]");
		sb.append("}");
		sb.append("],");
		sb.append("\"user_mentions\": [");
		sb.append("{");
		sb.append("\"screen_name\": \"InsideHoops\",");
		sb.append("\"name\": \"InsideHoops.com\",");
		sb.append("\"indices\": [");
		sb.append("3,");
		sb.append("15");
		sb.append("]");
		sb.append("}");
		sb.append("]");
		sb.append("}");
		sb.append("}");
	}
}
