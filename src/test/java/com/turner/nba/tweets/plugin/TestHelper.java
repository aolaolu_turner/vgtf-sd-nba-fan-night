package com.turner.nba.tweets.plugin;

import javax.jms.Message;

import org.apache.log4j.Appender;
import org.apache.log4j.ConsoleAppender;
import org.apache.log4j.Level;
import org.apache.log4j.Logger;
import org.apache.log4j.PatternLayout;

import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;

public class TestHelper {
	static void initLogger() {
		Logger rootLogger = Logger.getRootLogger();
	    rootLogger.setLevel(Level.DEBUG);
	    PatternLayout pattern = new PatternLayout("%-6r [%p] %c - %m%n");
	    Appender appender = new ConsoleAppender(pattern);
	    rootLogger.addAppender(appender);
	}
	
	static GenericMessage getGenericMessage(Message m) throws Exception{
		GenericMessage gm = null;
        if (m instanceof GenericMessage)
            gm = (GenericMessage) m;
        else
            gm = GenericMessageFactory.createGenericMessage(m);
        return gm;
	}
}
