package com.turner.nba.tweets.plugin;

import java.util.Hashtable;
import java.util.List;

import javax.jms.JMSException;

import org.junit.Test;

//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.loki.GenericMessage;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.loki.core.exceptions.PluginException;

public class GetGamesInWindowTest {

	@Test
	public void testGetGames1() throws InitializationException, PluginException, JMSException{
		GetGamesInWindow ggi = new GetGamesInWindow();
		Hashtable props = new Hashtable();
		props.put("game_window_in_minutes", "15");
		ggi.initialize(props);
		//
		GenericMessage gm = new GenericMessage();
		gm.setStringProperty("now", "20130114 20:00:00");
		ggi.process(gm);
		System.out.println(gm);
		List<Game> games = (List<Game>) gm.getObjectProperty("games");
		if(games!=null){
			for(Game g:games){
				System.out.println(g.getGameId() + " " + g.getGameDate() + " " + g.getGameEndDate());
			}
		}
	}
	@Test
	public void testGetGames2() throws InitializationException, PluginException, JMSException{
		GetGamesInWindow ggi = new GetGamesInWindow();
		Hashtable props = new Hashtable();
		props.put("game_window_in_minutes", "15");
		ggi.initialize(props);
		//
		GenericMessage gm = new GenericMessage();
		gm.setStringProperty("now", "20130114 00:00:00");
		ggi.process(gm);
		System.out.println(gm);
		List<Game> games = (List<Game>) gm.getObjectProperty("games");
		if(games!=null){
			for(Game g:games){
				System.out.println(g.getGameId() + " " + g.getGameDate() + " " + g.getGameEndDate());
			}
		}
	}
}
