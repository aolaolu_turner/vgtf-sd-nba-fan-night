package com.turner.nba.tweets.plugin;

import static org.junit.Assert.*;

import javax.jms.Message;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.turner.loki.GenericMessageFactory;
import com.turner.nba.fannight.util.MockTeams;

public class GetTeamTweetsTest extends GetTeamTweets {
	String nom = "bulls";
	String url = "http://dataint.nba.com/nba/noseason/cms/"+nom+"/twitter.json";
	
	@Before
	public void setUp() throws Exception {}

	@Test @Ignore
	public void testMultiplex() throws Exception {
		fail("Dont know how to do JPA just yet :(");
		
		//push a tweet for a specific team
		//message for the team
		//run the multiplex method and verify that the returned list of messages is not empty
	}

	@Test
	public void testGetParameters() throws Exception {
		
		String[] params = getParameters("<team name='"+nom+"' url='"+url+"' />");
		
		assertTrue("There should be 2 parameters returned", params.length == 2);
		assertTrue("Param 1 should be "+nom+" not "+params[0], params[0].equals(nom));
		assertTrue("Param 2 should be the url and not "+params[1], params[1].equals(url));
	}

}
