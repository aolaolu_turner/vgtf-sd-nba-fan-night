package com.turner.nba.tweets.plugin;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.Hashtable;
import java.util.List;

import javax.jms.Message;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;
import com.turner.nba.util.Helper;

import org.apache.log4j.Logger;
import org.jdom.Document;
import org.jdom.input.SAXBuilder;
import org.jdom.xpath.XPath;
import org.jdom.Element;

public class GetTeamsWithGamesIn15MinuteWindowTest extends GetTeamsWithGamesIn15MinuteWindow {
	Hashtable<String, String> props = new Hashtable<String, String>();
	
	@Before
	public void setUp() throws Exception {
		props.clear();
		props.put("nameMap1", "76ers|sixers");
		
		initialize(props);
	}

	@Test @Ignore
	public void testMultiplex() {
		fail("Don't know how to implement JPA in unit tests yet");
	}

	@Test
	public void testProcess() throws Exception {
		Message msg = GenericMessageFactory.createGenericMessage();
		ArrayList<GenericMessage> messages = new ArrayList<GenericMessage>();
		ArrayList<String> names = new ArrayList<String>();
		
		Date laDate = new Date();
		ArrayList<Game> games = new ArrayList<Game>();
		Game game = new Game();
		game.setVisitorNickname("bulls");
		game.setHomeNickname("hawks");	
		game.setGameDate(laDate);
		games.add(game);
		
		process(msg, messages, games, names);
		
		int nameCount = games.size() * 2;
		assertTrue("The message should not be empty", nameCount > 0);
		assertTrue("There should be "+nameCount+ " names", names.size() == nameCount);
		assertTrue("There should be "+nameCount+ " messsages", messages.size() == nameCount);
		
		for(Message m: messages){
			String bodyStr = m.getStringProperty("body");
			Document doc = Helper.convertStringToDocument(bodyStr);	
			XPath teamXP = XPath.newInstance("//team");
			Element teamElement = (Element)teamXP.selectSingleNode(doc);
			String name = (String) teamElement.getAttributeValue("name");
			String link = (String) teamElement.getAttributeValue("url");
			String date = (String) teamElement.getAttributeValue("gamedate");
			
			System.out.println("********************"+name+", "+link+", "+date+" instead of "+laDate);
			//return new String[]{ name, link, date };
		}
	}

	@Test
	public void testFixTeamName() throws Exception {
		String nom = "76ers";
		String name = fixTeamName(nom);
		assertTrue(nom+" should say sixers", name.equals("sixers"));
		
		props.clear();
		props.put("nameMap2", "bulls|cows");
		
		initialize(props);
		nom = "bulls";
		name = fixTeamName(nom);
		assertTrue("Bulls should say cows", name.equals("cows"));
		
		nom = "76ers";
		name = fixTeamName(nom);
		assertTrue(nom+" should say "+nom, name.equals(nom));
	}

}
