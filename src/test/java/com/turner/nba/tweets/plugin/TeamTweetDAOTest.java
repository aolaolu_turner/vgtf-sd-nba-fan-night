package com.turner.nba.tweets.plugin;

import java.util.List;

import org.junit.Test;

//import com.nba.loki.j5.beans.Game;
import com.nba.loki.beans.Game;
import com.turner.nba.tweets.jpa.TeamTweetDAO;

public class TeamTweetDAOTest {

	@Test
	public void testCountTweets(){
		List results = TeamTweetDAO.getInstance().countTweetsByMinute("hawks");
		System.out.println(results.size());
		for(Object o:results){
			System.out.println(o);
		}
	}
	
	@Test
	public void testGetGame(){
		Game g = TeamTweetDAO.getInstance().getGameById("0021200544");
		System.out.println(g.getGameId() + " " + g.getGameDate() + " " + g.getVisitorNickname() + " " + g.getHomeNickname());
	}
}
