package com.turner.nba.fannight.plugin;

import static org.junit.Assert.*;

import java.util.Hashtable;

import javax.jms.Message;

import org.junit.Before;
import org.junit.Test;

import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.PluginException;
import com.turner.nba.fannight.jpa.Poll;
import com.turner.nba.fannight.jpa.PollValue;
import com.turner.nba.fannight.jpa.Week;

public class SendVoteToCogixTest {
	SendVoteToCogix plugin;
	CogixConnection cogix = null;
	Week week;
	
	String urlString = "http://poll.turner.com/ViewsFlash/servlet/viewsflash";
	
	Message msg;
	
	@Before
	public void setUp() throws Exception {
		cogix = CogixConnection.getInstance();
		plugin = new SendVoteToCogix();
		
		Hashtable<String, String> props = new Hashtable<String, String>();
		props.put("cogix_url", urlString);
		
		plugin.initialize(props);
		
		msg = GenericMessageFactory.createGenericMessage();
		msg.setStringProperty("body", "<poll id='TESTAREA!fn2012_week1' vote='DETvsDEN' />");
		
		week = new Week();
		week.setId(1L);
		week.setWeekYear(2012);
	}

	@Test
	public void testProcess() throws Exception {
		Poll p1 = cogix.downloadLatestPoll(week);
		
		String text = "DETvsDEN";
		Message m = (Message) plugin.process(msg);
		
		try{
			  Thread.currentThread().sleep(20000);//sleep for 5seconds
		}catch(InterruptedException ie){}
		
		Poll p2 = cogix.downloadLatestPoll(week);
		
		for(PollValue v1:p1.getValues()){
			for(PollValue v2:p2.getValues()){
				if(v1.getId().equals(v2.getId())){
					int c1 = Integer.parseInt(v1.getCount());
					int c2 = Integer.parseInt(v2.getCount());
					
					if(v1.getId().equals(text)){
						assertTrue("Count of "+text+" should increase by one: ["+c1+", "+c2+"]", c1 == c2-1);
					}else
						assertTrue("Count of "+v1.getId()+" should not change", c1 == c2);
				}
			}
		}
	}

}
