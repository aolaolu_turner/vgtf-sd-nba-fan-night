package com.turner.nba.fannight.plugin;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXParseException;

import com.turner.nba.fannight.jpa.Poll;
import com.turner.nba.fannight.jpa.PollValue;
import com.turner.nba.fannight.jpa.Week;

public class CogixConnectorTest {
	CogixConnection cogix = null;
	private Week week;
	private String urlString = "http://poll.nba.com/ViewsFlash/servlet/viewsflash?cmd=getresults&pollid=TESTAREA!fn2012_week1&spotname=TESTAREA&style=XMLResults";
	
	@Before
	public void setUp() throws Exception {
		cogix = CogixConnection.getInstance();
		
		week = new Week();
		week.setId(1L);
		week.setWeekYear(2012);
	}

	@Test
	public void test() {
			URL url = null;
			URLConnection urlc = null; 
			try{
				url = new URL("http://soap.ne");
			}catch(MalformedURLException mue){
				System.err.println(mue); 
			}
			try{
				urlc = url.openConnection(); 
			}catch(IOException ioe){
				System.err.println(ioe); 
			}
			
			Map headerfields = urlc.getHeaderFields();
			Set headers = headerfields.entrySet(); 
			for(Iterator i = headers.iterator(); i.hasNext();){ 
				Map.Entry map = (Map.Entry)i.next();
				System.out.println(map.getKey() + " : " + map.getValue());
			}
	}

	@Test
	public void testGetPollUrl(){
		String url = cogix.getPollUrl(2012, 0L, urlString);
		assertTrue("Url should be: "+urlString, urlString.equals(url));
		
		String u = cogix.getPollUrl(2012, 4L, urlString);
		assertTrue("Url should be: "+urlString, urlString.equals(url));
		
		String string = urlString.replaceAll("2012", "2045").replaceAll("week1", "week3000");
		String uu = cogix.getPollUrl(2045, 3000L, null);
		assertTrue("Url should be: "+string, uu.equals(string));
	}
	
	@Test(expected=SAXParseException.class)
	public void testGetLatestPoll() throws Exception {
		Poll poll = cogix.downloadLatestPoll(week, urlString);
		assertTrue("Poll should not be null: ", poll != null);
		assertTrue("Poll values should not be empty", poll.getValues().isEmpty() == false);
		//assertTrue("Poll should have 3 values and not "+poll.getValues().size(), poll.getValues().size() == 3);
		
		Poll p2 = cogix.downloadLatestPoll(week);
		assertTrue("Poll should not be null: ", p2 != null);
		assertTrue("Poll values should not be empty", p2.getValues().isEmpty() == false);
		assertTrue("Poll ID should be the same because url passed didn't changed. Found "+poll.getId(), poll.getId().equals(p2.getId()));
		
		week.setId(3000L);
		Poll p3 = cogix.downloadLatestPoll(week, urlString);
		assertTrue("Poll should not be null: ", p3 != null);
		assertTrue("Poll ID should be the same because url passed didn't changed. Found "+poll.getId(), poll.getId().equals(p3.getId()));
		
		poll = cogix.downloadLatestPoll(week);	//this should fail
		assertTrue("Poll should not be null: ", poll != null);
		assertTrue("Poll values should be empty", poll.getValues().isEmpty());
	}

	@Test
	public void testSendVoteToCogix() {
		try{
			week.setId(0L);
			
			Poll p1 = cogix.downloadLatestPoll(week);
			String text = "DENvsMIN";
			
			//cogix.sendVote("http://poll.turner.com/ViewsFlash/servlet/viewsflash", p1.getId(), text);
			
			try{
				  Thread.currentThread().sleep(20000);//sleep for 5seconds
			}catch(InterruptedException ie){}
			
			Poll p2 = cogix.downloadLatestPoll(week);
			
			for(PollValue v1:p1.getValues()){
				for(PollValue v2:p2.getValues()){
					if(v1.getId().equals(v2.getId())){
						int c1 = Integer.parseInt(v1.getCount());
						int c2 = Integer.parseInt(v2.getCount());
						
						if(v1.getId().equals(text)){
							assertTrue("Count of "+text+" should increase by one: ["+c1+", "+c2+"]", c1 == c2-1);
						}else
							assertTrue("Count of "+v1.getId()+" should not change", c1 == c2);
					}
				}
			}
				
		}catch(Exception e){
			assertTrue("sending vote to cogix failed: "+e.getMessage(), true == false);
		}
	}

}
