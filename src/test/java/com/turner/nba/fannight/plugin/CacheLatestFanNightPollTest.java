package com.turner.nba.fannight.plugin;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.turner.nba.fannight.jpa.FanNightDAO;
import com.turner.nba.fannight.jpa.Poll;
import com.turner.nba.fannight.jpa.PollValue;
import com.turner.nba.fannight.jpa.Week;
import com.turner.nba.fannight.plugin.CacheLatestFanNightPoll;

public class CacheLatestFanNightPollTest {
	CacheLatestFanNightPoll plugin = null;
	Poll poll = null;
	Week week = null;
	FanNightDAO dao = null;
	
	@Before
	public void setup() throws Exception {
		week = new Week();
		week.setId(1L);
		week.setWeekYear(2012);

		plugin = new CacheLatestFanNightPoll();
		if(poll == null){
			poll = new Poll();
			poll.setId(System.currentTimeMillis()+"");
			poll.setLastCogixPollDate(new Date());
			
			List<PollValue> values = new ArrayList<PollValue>();
			for(int i = 0;i < 3;i++){
				PollValue pv = new PollValue();
				pv.setId("TESTAREA!fn2012_week1");
				values.add(pv);
			}
			
			values.get(0).setValue("ORLvsCHI");
			values.get(1).setValue("TORvsOKC");
			values.get(2).setValue("DETvsDEN");
			
			poll.setValues(values);
		}
	}
	
	@Test @Ignore
	public void testGetLatestSamplePoll() {
		fail("Don't know how to test using JPA yet");
		//Poll poll = plugin.getLatestSampleCogixPoll();
		//assertTrue("Poll should not be null: ", poll != null);
	}
}
