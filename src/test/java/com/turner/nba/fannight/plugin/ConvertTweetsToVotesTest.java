package com.turner.nba.fannight.plugin;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.Random;

import javax.jms.Message;

import net.unto.twitter.Api;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.turner.nba.fannight.jpa.FanNightDAO;
import com.turner.nba.fannight.jpa.Poll;
import com.turner.nba.fannight.jpa.Team;
import com.turner.nba.fannight.jpa.Week;
import com.turner.nba.fannight.plugin.CacheLatestFanNightPoll;
import com.turner.nba.fannight.plugin.ConvertTweetsToVotes;
import com.turner.nba.fannight.util.MockTeams;
import com.turner.nba.fannight.util.TwitterVote;
import com.turner.loki.GenericMessage;
import com.turner.loki.GenericMessageFactory;
import com.turner.loki.core.exceptions.InitializationException;

public class ConvertTweetsToVotesTest {
	private ConvertTweetsToVotes plugin = null;
	private Hashtable<String, String> props = new Hashtable<String, String>();
	private Week week;
	private CacheLatestFanNightPoll cache = null;
	
	private CogixConnection cogix = null;
	
	@Before
	public void setUp() throws Exception{
		props.put("tweetriver_url", "http://tweetriver.com/NBATV/fan-night-voting.json");
		props.put("cogix_url", "http://poll.turner.com/ViewsFlash/servlet/viewsflash");
		
		cogix = CogixConnection.getInstance();
		
		week = new Week();
		week.setId(1L);
		week.setWeekYear(2012);
		
		plugin = new ConvertTweetsToVotes();
		plugin.initialize(props);
		
		cache = new CacheLatestFanNightPoll();
	}

	@Test (expected=InitializationException.class)
	public void testInitialization() throws Exception {
		String url = "http://tweetriver.com/NBATV/fan-night-voting.json";
		String key = "tweetriver_url";
		
		props = new Hashtable<String, String>();
		props.put(key,  url);
		
		ConvertTweetsToVotes engine = new ConvertTweetsToVotes();
		engine.initialize(props);
		assertTrue("massRelevanceUrl not properly set", engine.getMassRelevanceUrl().equals(url));
		
		engine = new ConvertTweetsToVotes();
		assertTrue("massRelevanceUrl default value is wrong", engine.getMassRelevanceUrl().equals(url));
		
		props.remove(key);
		engine.initialize(props);
		assertTrue("massRelevanceUrl default value is wrong", engine.getMassRelevanceUrl().equals(url));
		
		props.put(key,  "");
		engine.initialize(props);
	}
	
	@Test
	public void testProcessTweets() throws Exception {
		Message msg = GenericMessageFactory.createGenericMessage();
		//sample poll contains ORLvsCHI, TORvsOKC, DETvsDEN
		Poll poll = FanNightDAO.getInstance().getSampleCogixPoll();
		
		List<TwitterVote> votes = new ArrayList<TwitterVote>();
		String[] texts = {"orlando", "magic", "heat", "oklahoma city", "thunder", "oklahoma"};
		for(int i = 1; i <= texts.length; i++){
			TwitterVote vote = new TwitterVote();
			vote.setId(i+"");
			vote.setText(texts[i-1]);
			votes.add(vote);
		}
		
		List<GenericMessage> messages = plugin.processTweets(msg, poll, votes, MockTeams.teams);
		assertTrue("returned messages should 4 messages from 6 votes", messages.size() == 4);
	}
	
	@Test @Ignore
	public void testGetTweets() throws Exception {
		fail("Don't know how to push stuff to twitter using twitter4j yet");
	}
	
	@Test @Ignore
	public void testMultiplex() throws Exception {
		fail("Don't know how to test JPA stuff just yet :(");
	}

	@Test
	public void testConvertTweetToPollId() throws Exception {
		Poll poll = cogix.downloadLatestPoll(week);
		
		String text = plugin.convertTweetToPollValueID(poll, MockTeams.teams, "toronto");
		assertTrue("TORvsOKC should be found", text.equals("TORvsOKC"));
		
		text = plugin.convertTweetToPollValueID(poll, MockTeams.teams, "magic");
		assertTrue("ORLvsCHI should be found", text.equals("ORLvsCHI"));
		
		text = plugin.convertTweetToPollValueID(poll, MockTeams.teams, "oklahoma");
		assertTrue("nothing should be found", text == null);
		
		week.setId(0L);
		poll = cogix.downloadLatestPoll(week);
		text = plugin.convertTweetToPollValueID(poll, MockTeams.teams, "golden state");
		assertTrue("LACvsGSW should be found not "+text, text.equals("LACvsGSW"));
	}

	@Test
	public void testGetMatchingTeams() throws Exception{
		Team te4 = plugin.getMatchingTeam(MockTeams.teams, "orlando miami");
		assertTrue(te4.getCity().equals("orlando"));
		
		for(Team team: MockTeams.teams){		//pick each team...
			String[] terms = { team.getCity(), team.getName(), team.getNickname(), team.getAbbreviation() };

			for(String text: terms){
				if(text != null){
					Team voted = plugin.getMatchingTeam(MockTeams.teams, text);

					assertTrue("We should always find team: "+text.toUpperCase(), voted != null);

					for(Team tt: MockTeams.teams){
						if(!tt.getAbbreviation().equals(team.getAbbreviation())){
							assertTrue(team.getAbbreviation() + "/" + text.toUpperCase() + " expected. Got " + voted.getAbbreviation(), voted.getAbbreviation().equals(team.getAbbreviation()) || voted.getCity().equals(voted.getCity()));
						}else{
							assertTrue(team.getCity() + " should be the expected team. Got " + voted.getAbbreviation(), voted.getCity().equals(team.getCity()));
						}
					}
				}
			}
			
			String alphabet = "abcdefghijklmnopqrstuvwxyz ";
			Random random = new Random(System.nanoTime());
			
			for(String t: terms){
				if(t != null){
					String text = alphabet.charAt(random.nextInt(alphabet.length())) + t + alphabet.charAt(random.nextInt(alphabet.length()));
					
					Team voted = plugin.getMatchingTeam(MockTeams.teams, text);

					assertTrue("We should always find team: "+text.toUpperCase(), voted != null);

					for(Team tt: MockTeams.teams){
						if(!tt.getAbbreviation().equals(team.getAbbreviation())){
							assertTrue(team.getAbbreviation() + "/" + text.toUpperCase() + " expected. Got " + voted.getAbbreviation(), voted.getAbbreviation().equals(team.getAbbreviation()) || voted.getCity().equals(voted.getCity()));
						}else{
							assertTrue(team.getCity() + " should be the expected team. Got " + voted.getAbbreviation(), voted.getCity().equals(team.getCity()));
						}
					}
				}
			}
		}
		 
		//run through all the other teams and make sure it won't match
		//make sure it matches it's default team
	}
}
