package com.turner.nba.fannight.util;

import java.util.ArrayList;
import java.util.List;
import com.turner.nba.fannight.jpa.Team;

public class MockTeams {
	public static List<Team> teams = null;
	
	private static String[] teamInfo = {
			"Boston, Celtics, BOS, Celts",
			"Brooklyn, Nets, BKN",
			"New York, Knicks, NYK, Knickerbockers",
			"Philadelphia, 76ers, PHI, Sixers",
			"Toronto, Raptors, TOR, Raps",
			"Chicago, Bulls, CHI",
			"Cleveland, Cavaliers, CLE, Cavs",
			"Detroit, Pistons, DET",
			"Indiana, Pacers, IND",
			"Milwaukee, Bucks, MIL",
			"Atlanta, Hawks, ATL",
			"Charlotte, Bobcats, CHA",
			"Miami, Heat, MIA",
			"Orlando, Magic, ORL",
			"Washington, Wizards, WAS",
			"Dallas, Mavericks, DAL, Mavs",
			"Houston, Rockets, HOU",
			"Memphis, Grizzlies, MEM, Grizz",
			"New Orleans, Hornets, NOH",
			"San Antonio, Spurs, SAS",
			"Denver, Nuggets, DEN, Nuggs",
			"Minnesota, Timberwolves, MIN, Twolves, T-Wolves, Wolves",
			"Portland, Trailblazers, POR, Blazers",
			"Oklahoma City, Thunder, OKC",
			"Utah, Jazz, UTA",
			"Golden State, Warriors, GSW",
			"Los Angeles, Clippers, LAC",
			"Los Angeles, Lakers, LAL",
			"Phoenix, Suns, PHX"	
	};
	
	static {
		teams = new ArrayList<Team>();
		
		for(String info:teamInfo){
			Team team = new Team();
			String[] splits = info.split(", ");
			
			team.setCity(splits[0]);
			team.setName(splits[1]);
			team.setAbbreviation(splits[2]);
			
			if(splits.length >= 4)
				team.setNickname(splits[3]);
			
			teams.add(team);
		}
	}
}
