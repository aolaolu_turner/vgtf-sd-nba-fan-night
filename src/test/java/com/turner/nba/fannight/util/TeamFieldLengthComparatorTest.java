package com.turner.nba.fannight.util;

import static org.junit.Assert.*;

import java.util.Collections;

import org.junit.Before;
import org.junit.Test;

import com.turner.nba.fannight.jpa.Team;

public class TeamFieldLengthComparatorTest {
	TeamFieldLengthComparator comparator;
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSorting() { 
		for(String category : Team.SEARCH_ORDER){
			comparator = new TeamFieldLengthComparator(category);

			Collections.sort(MockTeams.teams, comparator);

			for(Team team : MockTeams.teams){
				boolean found = false;
				String text = category.equals(Team.CITY) ? team.getCity() : category.equals(Team.NAME) ? team.getName() : category.equals(Team.NICKNAME) ? team.getNickname() : category.equals(Team.ABBREVIATION) ? team.getAbbreviation() : null;

				for(Team t : MockTeams.teams){
					if(team.getAbbreviation().equals(t.getAbbreviation())){
						found = true;
						continue;
					}

					String field = category.equals(Team.CITY) ? t.getCity() : category.equals(Team.NAME) ? t.getName() : category.equals(Team.NICKNAME) ? t.getNickname() : category.equals(Team.ABBREVIATION) ? t.getAbbreviation() : null;

					if(text != null && field != null)
						assertTrue(field+"/"+text+" and text has "+(found?"NOT ":"")+"been found", (found && field.length() <= text.length()) || field.length() >= text.length());
				}
			}
		}
	}
	
	@Test
	public void testCompare() {
		 
	}

}
