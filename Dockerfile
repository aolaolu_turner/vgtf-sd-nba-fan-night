# NAME: ssd-nba-fan-night-${env-name}
# VERSION: 1.0.${build-number}

FROM registry.services.dmtio.net/sd-java6-image:0.0.2
#FROM sd-java6-image:0.0.1

ENV DEBIAN_FRONTEND noninteractive
ENV HEALTHCHECK /app/healthcheck
ENV PORT 30000
ENV NAME ssd-nba-fan-night

ADD . /app/

EXPOSE 30000

CMD ["/app/bin/publisher restart"]